﻿using System.Web;
using System.Web.Optimization;

namespace PKTasks.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.1.1.min.js"));

            //// jQuery Validation
            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //"~/Scripts/jquery.validate.js",
            //"~/Scripts/jquery.validate.unobtrusive.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/animate.css",
                      "~/Content/style.css"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include(
                      "~/Scripts/plugins/metisMenu/metisMenu.min.js",
                      "~/Scripts/plugins/pace/pace.min.js",
                      "~/Scripts/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(
                      "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            // toastr notification 
            bundles.Add(new ScriptBundle("~/plugins/toastr").Include(
                      "~/Scripts/plugins/toastr/toastr.min.js"));

            // toastr notification styles
            bundles.Add(new StyleBundle("~/plugins/toastrStyles").Include(
                      "~/Content/plugins/toastr/toastr.min.css"));


            // unobtrusive ajax
            bundles.Add(new ScriptBundle("~/bundles/unobtractiveajax").Include(
                        "~/Scripts/plugins/unobtrusiveajax/jquery.unobtrusive-ajax.js"));

            // bootstrap-dialog Styles - Script
            bundles.Add(new StyleBundle("~/plugins/bootstrapdialogStyles").Include(
                      "~/Content/plugins/bootstrapdialog/bootstrap-dialog.css"));
            bundles.Add(new ScriptBundle("~/plugins/bootstrapdialog").Include(
                      "~/Scripts/plugins/bootstrapdialog/bootstrap-dialog.js"));

            // Font Awesome icons
            bundles.Add(new StyleBundle("~/font-awesome/css").Include(
                      "~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            // datepicker Styles - Script
            bundles.Add(new StyleBundle("~/plugins/datePickerStyles").Include(
                      "~/Content/plugins/datepicker/bootstrap-datepicker3.css"));
            bundles.Add(new ScriptBundle("~/plugins/datepicker").Include(
                      "~/Scripts/plugins/datepicker/bootstrap-datepicker.min.js",
                      "~/Scripts/plugins/datepicker/bootstrap-datepicker.el.min.js"));


            // Proteas scripts + styles
            bundles.Add(new ScriptBundle("~/bundles/proteas").Include(
                      "~/Scripts/proteas.js"));


            bundles.Add(new StyleBundle("~/Content/proteasStyles").Include(
         "~/Content/proteas.css"));

            // datatables
            bundles.Add(new StyleBundle("~/plugins/datatablesStyles").Include(
                      "~/Content/datatables/css/dataTables.fontAwesome.css"));
            bundles.Add(new ScriptBundle("~/plugins/datatables").Include(
                      "~/Scripts/datatables/jquery.dataTables.min.js",
                      "~/Scripts/datatables/dataTables.bootstrap.min.js",
                      "~/Scripts/datatables/dataTables.buttons.min.js"));
        }
    }
}
