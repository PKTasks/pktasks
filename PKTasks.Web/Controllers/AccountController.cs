﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using PKTasks.Web.Models;
using TasksBL;
using TasksDB;
using System.Web.Security;

namespace PKTasks.Web.Controllers
{  
    public class AccountController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        #region Login / Logout

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(AccountViewModels.LoginVM model, string returnUrl)
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.GetHumanRecourceByCredentials(model.Username, model.Password);
            if (human !=null)
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);
                Globals.UserId = human.Id;
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return View();
            }



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Login");
        }
        #endregion
    }
}