﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TasksBL;
using TasksDB;

namespace PKTasks.Web.Controllers
{
    public class AdjustmentsController : Controller
    {
        // GET: Adjustments
        public ActionResult Index()
        {
            return View();
        }


        #region Sectors
        public ActionResult SectorsView()
        {
            SectorService srvsc = new SectorService();
            List<Sector> sectors = new List<Sector>();
            sectors = srvsc.GetAllSectors();
            Models.SectorsVM SectorsVM = new Models.SectorsVM(sectors);
            return View(SectorsVM);
        }


        public ActionResult SectorsAddView()
        {
            Models.SectorVM sectorVM = new Models.SectorVM();
            return View(sectorVM);
        }


        public ActionResult SectorsAdd(Models.SectorVM sectorVM)
        {
            SectorService srvsc = new SectorService();
            Sector sectorTemp = new Sector();
            sectorTemp.Description = sectorVM.Description;
            srvsc.Add(sectorTemp);
            return RedirectToAction("SectorsView", "Adjustments");
        }


        public ActionResult SectorsEditView(int sectorId)
        {
            SectorService srvsc = new SectorService();
            Sector sector = srvsc.ReturnOneSector(sectorId);
            Models.SectorVM Sector = new Models.SectorVM(sector);
            return View(Sector);
        }

        public ActionResult SectorsEdit(Models.SectorVM sectorVM)
        {
            SectorService srvsc = new SectorService();
            Sector sector = srvsc.ReturnOneSector(sectorVM.Id);
            sector.Description = sectorVM.Description;
            srvsc.Edit(sector);
            return RedirectToAction("SectorsView","Adjustments");
        }
        #endregion


        #region TaskTags
        public ActionResult TagsView()
        {
            TaskTagsService srvtt = new TaskTagsService();
            List<TaskTags> tags = new List<TaskTags>();
            tags = srvtt.GetAllTags();
            Models.TaskTagsVM tagsVM = new Models.TaskTagsVM(tags);
            return View(tagsVM);
        }

        public ActionResult TaskTagsAddView()
        {
            Models.TaskTagVM tagVM = new Models.TaskTagVM();
            return View(tagVM);
        }

        public ActionResult TaskTagsAdd(Models.TaskTagVM tagVM)
        {
            TaskTagsService srvtt = new TaskTagsService();
            TaskTags taskTemp = new TaskTags();
            taskTemp.Name = tagVM.Name;
            srvtt.Add(taskTemp);
            return RedirectToAction("TagsView", "Adjustments");
        }

        public ActionResult TaskTagsEditView(byte tagId)
        {
            TaskTagsService srvtt = new TaskTagsService();
            TaskTags tag = srvtt.ReturnOneTag(tagId);
            Models.TaskTagVM tagVM = new Models.TaskTagVM(tag);
            return View(tagVM);
        }


        public ActionResult TaskTagsEdit(Models.TaskTagVM tagVM)
        {
            TaskTagsService srvtt = new TaskTagsService();
            TaskTags tag = srvtt.ReturnOneTag(tagVM.Id);
            tag.Name = tagVM.Name;
            srvtt.Edit(tag);
            return RedirectToAction("TagsView", "Adjustments");
        }
        #endregion

    }
}