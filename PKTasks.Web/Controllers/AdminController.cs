﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TasksBL;
using TasksDB;

namespace PKTasks.Web.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }



        #region projects
        public ActionResult Projects()
        {
            ProjectService srvpr = new ProjectService();
            List<Project> projects = new List<Project>();
            projects = srvpr.GetAllProjects();

            Models.ProjectsVM projectsvm = new Models.ProjectsVM(projects);
            return View(projectsvm);
        }


        public ActionResult ProjectsEdit(int ProjectId)
        {

            ProjectService srvpr = new ProjectService();
            Project project = new Project();
            project = srvpr.ReturnOneProject(ProjectId);


            bool taskSaved = false;
            if (TempData["TaskSaved"] != null)
            {
                taskSaved = (bool)TempData["TaskSaved"];
            }
            ViewBag.TaskSaved = taskSaved;

            Models.ProjectVM projectvm = new Models.ProjectVM(project);
            return View(projectvm);
        }


        public ActionResult ProjectsAdd()
        {
            Models.ProjectVM projectvm = new Models.ProjectVM();
            return View(projectvm);
        }


        public ActionResult ProjectEdit(Models.ProjectVM projectVM)
        {
            ProjectService srvpr = new ProjectService();
            Project project = new Project();

            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(projectVM.statusId);

            project = srvpr.ReturnOneProject(projectVM.Id);
            project.Id = projectVM.Id;

            project.Name = projectVM.Name;
            project.StatusId = status.Id;
            project.Description = projectVM.Description;
            project.EstimatedStartDate = projectVM.EstimatedStartDate;
            project.EstimatedEndDate = projectVM.EstimatedEndDate;
            project.StartDate = projectVM.StartDate;
            project.EndDate = projectVM.EndDate;
            project.Deadline = projectVM.Deadline;
            project.EstimatedDuration = projectVM.EstimatedDuration;
            project.EstimatedDurationType = 1;
            srvpr.Edit(project);

            TempData["TaskSaved"] = true;

            return RedirectToAction("ProjectsEdit", new { ProjectId = projectVM.Id });
        }


        public ActionResult ProjectAdd(Models.ProjectVM projectVM)
        {
            ProjectService srvpr = new ProjectService();
            Project project = new Project();

            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(projectVM.statusId);

            project.Name = projectVM.Name;
            project.StatusId = status.Id;
            project.Description = projectVM.Description;
            project.EstimatedStartDate = projectVM.EstimatedStartDate;
            project.EstimatedEndDate = projectVM.EstimatedEndDate;
            project.StartDate = projectVM.StartDate;
            project.EndDate = projectVM.EndDate;
            project.Deadline = projectVM.Deadline;
            project.EstimatedDuration = projectVM.EstimatedDuration;
            project.EstimatedDurationType = 1;
            srvpr.Add(project);
            return RedirectToAction("Projects");
        }


        //public ActionResult ProjectViewFilter(int statusId = 0)
        //{
        //    ProjectService srvpr = new ProjectService();
        //    List<Project> projects = new List<Project>();

        //    //StatusService srvst = new StatusService();
        //    if (statusId != 0)
        //    {
        //        projects = srvpr.GetAllProjects(x => x.IsDeleted == false, x => x.StatusId == statusId);
        //    }
        //    else
        //    {
        //        projects = srvpr.GetAllProjects(x => x.IsDeleted == false);
        //    }
        //}


        public ActionResult ProjectsTableView(int statusId = 0)
        {
            ProjectService srvpr = new ProjectService();
            List<Project> projects = new List<Project>();

            //StatusService srvst = new StatusService();
            if (statusId != 0)
            {
                projects = srvpr.GetAllProjects(x => x.IsDeleted == false, x => x.StatusId == statusId);
            }
            else
            {
                projects = srvpr.GetAllProjects(x => x.IsDeleted == false);
            }

            Models.ProjectsVM projectsVM = new Models.ProjectsVM(projects);
            return View(projectsVM);
        }
        

        public ActionResult ProjectRemove(int projectId)
        {

            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(projectId);
            project.IsDeleted = true;
            srvpr.Edit(project);
            SubProjectService srvsbpr = new SubProjectService();
            srvsbpr.Project_SubprojectsRemove(projectId);
            return Json(true);
        }


        #endregion

        #region subprojects
        public ActionResult SubProjects(int ProjectId)
        {
            SubProjectService srvsbpr = new SubProjectService();
            List<SubProject> subprojects = new List<SubProject>();
            subprojects = srvsbpr.GetProjectSubProjects(ProjectId);

            Models.Project_SubProjectsVM subprojectsvm = new Models.Project_SubProjectsVM(subprojects, ProjectId);
            return View(subprojectsvm);

        }


        public ActionResult SubProjectsAdd(int ProjectId)
        {
            Models.SubProjectVM subprojectvm = new Models.SubProjectVM(ProjectId);
            return View(subprojectvm);
        }


        public ActionResult SubProjectsEdit(int subprojectId)
        {

            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = new SubProject();
            subproject = srvsbpr.ReturnOneSubProject(subprojectId);


            bool taskSaved = false;
            if (TempData["TaskSaved"] != null)
            {
                taskSaved = (bool)TempData["TaskSaved"];
            }
            ViewBag.TaskSaved = taskSaved;

            Models.SubProjectVM subprojectvm = new Models.SubProjectVM(subproject);
            return View(subprojectvm);
        }



        public ActionResult SubProjectEdit(Models.SubProjectVM subprojectVM)
        {
            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = srvsbpr.ReturnOneSubProject(subprojectVM.Id);
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(subprojectVM.statusId);
            subproject.Id = subprojectVM.Id;
            subproject.ProjectId = subprojectVM.ProjectId;
            subproject.Name = subprojectVM.Name;
            subproject.StatusId = status.Id;
            subproject.Description = subprojectVM.Description;
            subproject.EstimatedStartDate = subprojectVM.EstimatedStartDate;
            subproject.EstimatedEndDate = subprojectVM.EstimatedEndDate;
            subproject.StartDate = subprojectVM.StartDate;
            subproject.EndDate = subprojectVM.EndDate;
            subproject.Deadline = subprojectVM.Deadline;
            subproject.EstimatedDuration = subprojectVM.EstimatedDuration;
            subproject.EstimatedDurationType = 1;
            srvsbpr.Edit(subproject);
            
            TempData["TaskSaved"] = true;

            return RedirectToAction("SubProjectsEdit", new { subprojectId = subprojectVM.Id });
        }


        public ActionResult SubProjectAdd(Models.SubProjectVM subprojectVM)
        {
            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = new SubProject();
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(subprojectVM.statusId);

            subproject.ProjectId = subprojectVM.ProjectId;
            subproject.Name = subprojectVM.Name;
            subproject.StatusId = status.Id;
            subproject.Description = subprojectVM.Description;
            subproject.EstimatedStartDate = subprojectVM.EstimatedStartDate;
            subproject.EstimatedEndDate = subprojectVM.EstimatedEndDate;
            subproject.StartDate = subprojectVM.StartDate;
            subproject.EndDate = subprojectVM.EndDate;
            subproject.Deadline = subprojectVM.Deadline;
            subproject.EstimatedDuration = subprojectVM.EstimatedDuration;
            subproject.EstimatedDurationType = 1;
            srvsbpr.Add(subproject);
            return RedirectToAction("SubProjects", new { ProjectId = subproject.ProjectId });
        }

        public ActionResult SubProjectsTableView(int projectId, int statusId=0)
        {
            SubProjectService srvsbpr = new SubProjectService();
            List<SubProject> subprojects = new List<SubProject>();

            if (statusId != 0)
            {
                subprojects = srvsbpr.GetProjectSubProjects(projectId, x => x.IsDeleted == false, x => x.StatusId == statusId);
            }
            else
            {
                subprojects = srvsbpr.GetProjectSubProjects(projectId,x => x.IsDeleted == false);
            }


            Models.Project_SubProjectsVM subprojectsVM = new Models.Project_SubProjectsVM(subprojects, projectId);
            return View(subprojectsVM);
        }


        public ActionResult SubProjectRemove(int subprojectId)
        {

            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = srvsbpr.ReturnOneSubProject(subprojectId);
            subproject.IsDeleted = true;
            srvsbpr.Edit(subproject);
            TaskService srvtk = new TaskService();
            srvtk.Subproject_TasksRemove(subprojectId);
            return Json(true);
        }
        #endregion

        #region tasks
        public ActionResult Tasks(int SubProjectId)
        {
            TaskService srvtk = new TaskService();
            List<Task> tasks = new List<Task>();
            tasks = srvtk.GetSubProjectTasks(SubProjectId);

            Models.SubProjects_TasksVM tasksvm = new Models.SubProjects_TasksVM(tasks, SubProjectId);
            return View(tasksvm);
        }


        public ActionResult TasksAdd(int SubProjectId)
        {

            Models.TaskVM taskvm = new Models.TaskVM(SubProjectId);
            return View(taskvm);
        }


        public ActionResult TasksEdit(int taskId)
        {

            TaskService srvtk = new TaskService();
            Task task = new Task();
            task = srvtk.ReturnOneTask(taskId);

            bool taskSaved = false;
            if (TempData["TaskSaved"] != null)
            {
                taskSaved = (bool)TempData["TaskSaved"];
            }
            ViewBag.TaskSaved = taskSaved;

            Models.TaskVM taskvm = new Models.TaskVM(task);
            return View(taskvm);
        }

        //public ActionResult TasksRemove(int taskId)
        //{
        //    TaskService srvtk = new TaskService();
        //    Task task = new Task();
        //    task = srvtk.ReturnOneTask(taskId);

        //}

        public ActionResult TaskEdit(Models.TaskVM taskVM)
        {
            TaskService srvtk = new TaskService();
            Task task = srvtk.ReturnOneTask(taskVM.Id);
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(taskVM.StatusId);
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(taskVM.HumanResourcesId);////////////////////////
            task.Id = taskVM.Id;
            //task.SubProjectId = taskVM.SubProjectId;
            task.Name = taskVM.Name;
            task.StatusId = status.Id;
            task.Description = taskVM.Description;
            task.EstimatedStartDate = taskVM.EstimatedStartDate;
            task.EstimatedEndDate = taskVM.EstimatedEndDate;
            task.StartDate = taskVM.StartDate;
            task.EndDate = taskVM.EndDate;
            task.Deadline = taskVM.Deadline;
            task.EstimatedDuration = taskVM.EstimatedDuration;
            task.EstimatedDurationType = taskVM.EstimatedDurationType;
            task.HumanResourcesId = human.Id;
            task.TaskTagsId = taskVM.TagId;
            srvtk.Edit(task);

            TempData["TaskSaved"] = true;

            return RedirectToAction("TasksEdit", new { taskId = task.Id });
        }

        public ActionResult TaskAdd(Models.TaskVM taskVM)
        {
            TaskService srvtk = new TaskService();
            Task task = new Task();
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(taskVM.StatusId);
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(taskVM.HumanResourcesId);////////////////////////

            task.SubProjectId = taskVM.SubProjectId;
            task.Name = taskVM.Name;
            task.StatusId = status.Id;
            task.Description = taskVM.Description;
            task.EstimatedStartDate = taskVM.EstimatedStartDate;
            task.EstimatedEndDate = taskVM.EstimatedEndDate;
            task.StartDate = taskVM.StartDate;
            task.EndDate = taskVM.EndDate;
            task.Deadline = taskVM.Deadline;
            task.EstimatedDuration = taskVM.EstimatedDuration;
            task.EstimatedDurationType = taskVM.EstimatedDurationType;
            task.HumanResourcesId = human.Id;
            if (taskVM.TagId != 0)
            {
                task.TaskTagsId = taskVM.TagId;
            }
            else
            {
                task.TaskTagsId = null;
            }
            srvtk.Add(task);
            return RedirectToAction("Tasks", new { SubProjectId = task.SubProjectId });
        }

        public ActionResult NoteEditView(int noteId)
        {
            NotesService srvnt = new NotesService();
            Note note = srvnt.ReturnOneNotebyId(noteId);
            Models.NoteVM noteVM = new Models.NoteVM(note);
            return View(noteVM);
        }

        public ActionResult TaskNoteEdit(Models.NoteVM noteVM)
        {
            NotesService srvnt = new NotesService();
            Note note = srvnt.ReturnOneNotebyId(noteVM.Id);
            note.HumanResourceId = noteVM.HumanResourceId;
            note.Id = noteVM.Id;
            note.Message = noteVM.Message;
            note.PostDate = noteVM.PostDate;
            note.EntryId = noteVM.EntryId;
            note.EntryTypeId = (byte)noteVM.EntryType;
            srvnt.Edit(note);
            return Json(true);
        }


        public ActionResult TasksTableView(int subprojectId, int statusId=0)
        {
            TaskService srvtk = new TaskService();
            List<Task> tasks = new List<Task>();

            if (statusId != 0)
            {
                tasks = srvtk.GetSubProjectTasks(subprojectId, x => x.IsDeleted == false, x => x.StatusId == statusId);
            }
            else
            {
                tasks = srvtk.GetSubProjectTasks(subprojectId, x => x.IsDeleted == false);
            }

            Models.SubProjects_TasksVM tasksVM = new Models.SubProjects_TasksVM(tasks,subprojectId);
            return View(tasksVM);
        }


        public ActionResult TaskRemove(int taskId)
        {

            TaskService srvtk = new TaskService();
            Task task = srvtk.ReturnOneTask(taskId);
            task.IsDeleted = true;
            srvtk.Edit(task);

            return Json(true);
        }
        #endregion

    }
}