﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TasksBL;
using TasksDB;

namespace PKTasks.Web.Controllers
{
    public class AdminHumanresourceController : Controller
    {
        // GET: AdminHumanresource
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HumanResource()
        {
            HumanResourceService srvhr = new HumanResourceService();
            List<HumanResource> humans = new List<HumanResource>();
            humans = srvhr.GetAllHumanResources();

            Models.HumanResourcesVM humansvm = new Models.HumanResourcesVM(humans);
            return View(humansvm);
        }


        public ActionResult HumanResourceAddView()
        {
            Models.HumanResourceVM humanresourcevm = new Models.HumanResourceVM();
            return View(humanresourcevm);
        }


        public ActionResult HumanResourceEditView(int HumanResourceId)
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(HumanResourceId);
            Models.HumanResourceVM humanresourcevm = new Models.HumanResourceVM(human);
            return View(humanresourcevm);

        }


        public ActionResult HumanResourceAdd(Models.HumanResourceVM humanresourceVM)
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = new TasksDB.HumanResource();
            human.Name = humanresourceVM.Name;
            human.Username = humanresourceVM.Username;
            human.Password = humanresourceVM.Password;
            human.Role = (byte)humanresourceVM.Role;
            human.SectorId = humanresourceVM.SectorId;
            srvhr.Add(human);
            return RedirectToAction("HumanResource");
        }


        public ActionResult HumanResourceEdit(Models.HumanResourceVM humanresourceVM)
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = new TasksDB.HumanResource();
            human.Id = humanresourceVM.Id;
            human.Name = humanresourceVM.Name;
            human.Username = humanresourceVM.Username;
            human.Password = humanresourceVM.Password;
            human.Role = humanresourceVM.RoleId;
            human.SectorId = humanresourceVM.SectorId;
            srvhr.Edit(human);
            return RedirectToAction("HumanResource");
        }


        public ActionResult HumanResourceCostAddView(int hrId)
        {
            Models.HumanResourceCostVM humanresourcecostvm = new Models.HumanResourceCostVM(hrId);
            return View(humanresourcecostvm);
        }


        public JsonResult HumanResourceCostAdd(Models.HumanResourceCostVM humanresourcecostVM)
        {
            HumanResourceCostsService srvhrct = new HumanResourceCostsService();
            HumanResourceCost cost = new TasksDB.HumanResourceCost();
            cost.Cost = humanresourcecostVM.Cost;
            cost.FromDate = humanresourcecostVM.FromDate;
            cost.HumanResourceId = humanresourcecostVM.HumanResourceId;
            srvhrct.Add(cost);
            return Json(true);
        }
    }
}