﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TasksBL;
using TasksDB;

namespace PKTasks.Web.Controllers
{
    public class NotesController : Controller
    {
        // GET: Notes
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult NoteAddView(int humanId, int entryId, Enumerations.PKTasksEntryType entryType)
        {
            Models.NoteVM noteVM = new Models.NoteVM(humanId, entryId, entryType);
            return View(noteVM);
        }


        public ActionResult NoteAdd(Models.NoteVM noteVM)
        {
            NotesService srvnt = new NotesService();
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(Globals.UserId);
            Note note = new Note();
            note.EntryId = noteVM.EntryId;
            note.EntryTypeId = (byte)noteVM.EntryType;
            note.HumanResourceId = human.Id;
            note.Message = noteVM.Message;
            note.PostDate = DateTime.Now;
            srvnt.Add(note);
            return Json(true);
        }


        public ActionResult EntryNotesView(int EntryId, byte EntryTypeId)
        {
            NotesService srvnt = new NotesService();
            List<Note> notes = srvnt.GetAllNotes(EntryId, EntryTypeId);

            Models.NotesVM notesVM = new Models.NotesVM(notes);
            return View(notesVM);
        }


        public ActionResult FullNoteView(int noteId)
        {
            NotesService srvnt = new NotesService();
            Note note = srvnt.ReturnOneNotebyId(noteId);
            Models.NoteVM noteVM = new Models.NoteVM(note);
            return View(noteVM);
        }

    }
}