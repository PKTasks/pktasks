﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using TasksBL;
using TasksDB;

namespace PKTasks.Web.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult MyAccount()
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(Globals.UserId);

            SectorService srvst = new SectorService();
            Sector sector = new Sector();
            sector = srvst.ReturnOneSector(human.SectorId);

            Models.HumanResourceVM user = new Models.HumanResourceVM(human);

            return View(user);
        }

        public ActionResult MyTasks()
        {
            TaskService srvts = new TaskService();
            List<Task> tasks = new List<Task>();
            tasks = srvts.GetHumanResourceTasks(Globals.UserId, x=>x.IsDeleted==false);

            Models.HumanResourceTasksVM tasksvm = new Models.HumanResourceTasksVM(tasks);
            return View(tasksvm);
        }

        public ActionResult MyTask(int taskId)
        {
            TaskService srvts = new TaskService();
            Task task = srvts.ReturnOneTask(taskId);
            Models.TaskVM taskVM = new Models.TaskVM(task);
            return View(taskVM);
        }


        public ActionResult TaskStatusChanged(int Id, byte StatusId)
        {
            TaskService srvts = new TaskService();
            Task task = srvts.ReturnOneTask(Id);
            task.StatusId = StatusId;
            srvts.Edit(task);
            return RedirectToAction("MyTask", new { taskId= Id });
        }

        
        public ActionResult WorkingDurationAddView(int taskId)
        {
            Models.WorkingDurationVM workingdurationVM = new Models.WorkingDurationVM(taskId);
            return View(workingdurationVM);
        }



        public ActionResult WorkingDurationAdd(Models.WorkingDurationVM workingdurationVM)
        {
            TasksWorkingDurationsService srvwd = new TasksWorkingDurationsService();
            TaskWorkingDuration duration = new TaskWorkingDuration();
            duration.Duration = workingdurationVM.Duration;
            duration.HumanResourceCostsId = workingdurationVM.HumanResourceCostId;
            duration.HumanResourceId = workingdurationVM.HumanResourceId;
            duration.TaskId = workingdurationVM.TaskId;
            duration.WorkingDate = workingdurationVM.WorkingDate;
            srvwd.Add(duration);
            return RedirectToAction("MyTask", new { taskId = duration.TaskId });
        }


        public ActionResult WorkingDurationEditView(int WorkingDurationId)
        {
            TasksWorkingDurationsService srvwd = new TasksWorkingDurationsService();
            TaskWorkingDuration duration = srvwd.ReturnOneWorkingDuration(WorkingDurationId);
            Models.WorkingDurationVM workingdurationVM = new Models.WorkingDurationVM(duration);
            return View(workingdurationVM);
        }



        public ActionResult WorkingDurationEdit(Models.WorkingDurationVM workingdurationVM)
        {
            TasksWorkingDurationsService srvwd = new TasksWorkingDurationsService();
            TaskWorkingDuration duration = new TaskWorkingDuration();
            duration = srvwd.ReturnOneWorkingDuration(workingdurationVM.Id);
            duration.Duration = workingdurationVM.Duration;
            duration.HumanResourceCostsId = workingdurationVM.HumanResourceCostId;
            duration.HumanResourceId = workingdurationVM.HumanResourceId;
            duration.TaskId = workingdurationVM.TaskId;
            duration.WorkingDate = workingdurationVM.WorkingDate;
            srvwd.Edit(duration);
            return RedirectToAction("WorkingDurationEditView", new { WorkingDurationId = duration.Id });
        }


        public ActionResult Projects()
        {
            ProjectService srvpr = new ProjectService();
            List<Project> projects = srvpr.GetAllProjects(x => x.IsDeleted == false);
            Models.ProjectsVM ProjectsVM = new Models.ProjectsVM(projects);
            return View(ProjectsVM);
        }


        public ActionResult SubProjects(int projectId)
        {
            SubProjectService srvsbpr = new SubProjectService();
            List<SubProject> subprojects = srvsbpr.GetProjectSubProjects(projectId, x => x.IsDeleted == false);
            Models.Project_SubProjectsVM SubProjectsVM = new Models.Project_SubProjectsVM(subprojects, projectId);
            return View(SubProjectsVM);
        }

        public ActionResult _TaskAdd(int subprojectId)
        {
            Models.TaskVM taskvm = new Models.TaskVM(subprojectId);
            return View(taskvm);
        }

        public ActionResult TaskAdd(Models.TaskVM taskVM)
        {
            TaskService srvtk = new TaskService();
            Task task = new Task();
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(taskVM.StatusId);
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(taskVM.HumanResourcesId);////////////////////////

            task.SubProjectId = taskVM.SubProjectId;
            task.Name = taskVM.Name;
            task.StatusId = status.Id;
            task.Description = taskVM.Description;
            task.EstimatedStartDate = taskVM.EstimatedStartDate;
            task.EstimatedEndDate = taskVM.EstimatedEndDate;
            task.StartDate = taskVM.StartDate;
            task.EndDate = taskVM.EndDate;
            task.Deadline = taskVM.Deadline;
            task.EstimatedDuration = taskVM.EstimatedDuration;
            task.EstimatedDurationType = taskVM.EstimatedDurationType;
            task.HumanResourcesId = human.Id;
            if (taskVM.TagId != 0)
            {
                task.TaskTagsId = taskVM.TagId;
            }
            else
            {
                task.TaskTagsId = null;
            }
            srvtk.Add(task);
            return Json(true);
        }


        public ActionResult LoadProjectData(byte? statusId)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();


            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;

            
            ProjectService srvpr = new ProjectService();
            

            Expression<Func<Project, bool>> query = x => x.IsDeleted == false;
            if (statusId.HasValue && statusId > 0)
            {
                
                query = query.AndAlso(x => x.StatusId == statusId);
            }

            List<Project> Projects = srvpr.GetAllProjects(filter1: query);
            var v = (from a in Projects select new { a.Id, a.Name, Status = a.Status.Name });

            //if(!(string.IsNullOrEmpty(sortColumn) & string.IsNullOrEmpty(sortColumnDir)))
            //{
            //    v = v.OrderBy(sortColumn + " " + sortColumnDir);
            //}

            totalRecords = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            var t = Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
            return t;

        }
    }
}