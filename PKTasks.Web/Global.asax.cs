﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using TasksBL;
using TasksDB;

namespace PKTasks.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //var exception = Server.GetLastError();
            //throw exception;
        }
        
        protected void Session_Start(object sender, EventArgs e)
        {
#if DEBUG
            string username = User.Identity.Name;
            HumanResourceService service = new HumanResourceService();
            HumanResource human = service.GetHumanResource(username);
            if (human != null)
            {
                Globals.UserId = human.Id;
            }
#else
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
#endif
        }
    }
}
