﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using TasksBL;
using TasksDB;

namespace PKTasks.Web
{
    public static class Globals
    {
        public const int DefddlValue = 0;

        private const string SESSION_USER_ID = "UserId";
        public const Enumerations.PKTasksUserRole ADMIN = Enumerations.PKTasksUserRole.Διαχειριστής;
        public const Enumerations.PKTasksUserRole USER = Enumerations.PKTasksUserRole.Χρήστης;

        /// <summary>
        /// Το αναγνωριστικό του συνδεδεμένου (loggedin) χρήστη
        /// </summary>
        public static int UserId
        {
            get
            {
                int userId = -1;
                if (HttpContext.Current.Session[SESSION_USER_ID] != null)
                {
                    int.TryParse(HttpContext.Current.Session[SESSION_USER_ID].ToString(), out userId);
                }
                return userId;
            }
            set
            {
                HttpContext.Current.Session[SESSION_USER_ID] = value;
            }
        }

        public static Enumerations.PKTasksUserRole GetUserRole()
        {
            HumanResourceService srvhr = new HumanResourceService();
            Enumerations.PKTasksUserRole UserRole = srvhr.GetHumanResourceRole(UserId);
            return UserRole;
        }


        public static string GetEnumDescription<TEnum>(TEnum enumObj)
        {
            FieldInfo fi = enumObj.GetType().GetField(enumObj.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumObj.ToString();
            }
        }

        /// <summary>
        /// Συνενώνει δύο expressions με λογικό AND
        /// </summary>
        /// <typeparam name="T">Ό τύπος των οντοτήτων του expression</typeparam>
        /// <param name="expr1">Το πρώτο expression</param>
        /// <param name="expr2">Το δεύτερο expression</param>
        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var parameter = Expression.Parameter(typeof(T));

            var leftVisitor = new ReplaceExpressionVisitor(expr1.Parameters[0], parameter);
            var left = leftVisitor.Visit(expr1.Body);

            var rightVisitor = new ReplaceExpressionVisitor(expr2.Parameters[0], parameter);
            var right = rightVisitor.Visit(expr2.Body);

            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(left, right), parameter);
        }

        private class ReplaceExpressionVisitor : ExpressionVisitor
        {
            private readonly Expression _oldValue;
            private readonly Expression _newValue;

            public ReplaceExpressionVisitor(Expression oldValue, Expression newValue)
            {
                _oldValue = oldValue;
                _newValue = newValue;
            }

            public override Expression Visit(Expression node)
            {
                if (node == _oldValue)
                    return _newValue;
                return base.Visit(node);
            }
        }
    }


}