﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PKTasks.Web.Models
{
    public class AccountViewModels
    {

        public class LoginVM
        {
            public LoginVM()
            {

            }

            public LoginVM(string username, string password)
            {
                this.Username = username;
                this.Password = password;
            }

            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}