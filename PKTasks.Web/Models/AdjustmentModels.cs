﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Web.Mvc;
using TasksBL;
using TasksDB;
using System.Linq;

namespace PKTasks.Web.Models
{
    public class SectorVM
    {
        public SectorVM()
        {
            
        }

        public SectorVM(Sector sector)
        {
            this.Id = sector.Id;
            this.Description = sector.Description;
        }

        public int Id { get; set; }
        public string Description { get; set; }
    }


    public class SectorsVM
    {
        public SectorsVM(List<Sector> sectors)
        {
            Sectors = new List<SectorVM>();
            foreach (var sector in sectors)
            {
                SectorVM SectorTemp = new SectorVM(sector);
                Sectors.Add(SectorTemp);
            }
        }

        public List<SectorVM> Sectors { get; set; }
    }



    public class TaskTagVM
    {
        public TaskTagVM()
        {

        }

        public TaskTagVM(TaskTags tag)
        {
            this.Id = tag.Id;
            this.Name = tag.Name;
        }

        public byte Id { get; set; }
        public string Name { get; set; }
    }


    public class TaskTagsVM
    { 
        public TaskTagsVM(List<TaskTags> tags)
        {
            TaskTags = new List<TaskTagVM>();
            foreach (var tag in tags)
            {
                TaskTagVM TaskTemp = new TaskTagVM(tag);
                TaskTags.Add(TaskTemp);
            }
        }
        public List<TaskTagVM> TaskTags { get; set; }
    }
}