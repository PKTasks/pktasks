﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Web.Mvc;
using TasksBL;
using TasksDB;
using System.Linq;

namespace PKTasks.Web.Models
{

    #region HumanResourceCostVM
    public class HumanResourceCostVM
    {
        public HumanResourceCostVM()
        {

        }
        public HumanResourceCostVM(int humanresourceId)
        {
            this.HumanResourceId = humanresourceId;
        }

        public HumanResourceCostVM(HumanResourceCost cost)
        {
            this.Id = cost.Id;
            this.Cost = cost.Cost;
            this.FromDate = cost.FromDate;
        }
        public int Id { get; set; }
        public int HumanResourceId { get; set; }
        [Display(Name ="Κόστος/Ώρα")]
        public decimal Cost { get; set; }
        [Display(Name = "Ημερομηνία Ισχύος")]
        [UIHint("DateTimeToDate")]
        public DateTime FromDate { get; set; }
    }

    public class HumanResourceCostsVM
    {
        public HumanResourceCostsVM(List<HumanResourceCost> humsnresourcecosts)
        {
            lstHumanResourceCostsVM = new List<HumanResourceCostVM>();

            foreach (var cost in humsnresourcecosts)
            {
                HumanResourceCostVM costTemp = new HumanResourceCostVM(cost);
                lstHumanResourceCostsVM.Add(costTemp);
            }
        }

        public List<HumanResourceCostVM> lstHumanResourceCostsVM { get; set; }
    }
    #endregion



    #region HumanResourceVM
    public class HumanResourceVM
    {

        public HumanResourceVM()
        {
            SectorService srvsc = new SectorService();
            AllSectors = new List<SelectListItem>();
            AllRoles = new List<SelectListItem>();
            List<Sector> lsectors = srvsc.GetAllSectors();

            foreach (var st in lsectors)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Description;
                item.Value = st.Id.ToString();
                this.AllSectors.Add(item);
            }
            foreach(Enumerations.PKTasksUserRole role in Enum.GetValues(typeof(Enumerations.PKTasksUserRole)).Cast<Enumerations.PKTasksUserRole>())
            {
                SelectListItem item = new SelectListItem();
                item.Text = role.ToString();
                item.Value = ((int)role).ToString();
                this.AllRoles.Add(item);
            }
        }

        public HumanResourceVM(HumanResource human)
        {
            SectorService srvsc = new SectorService();
            Sector sector = srvsc.ReturnOneSector(human.SectorId);

            this.Id = human.Id;
            this.LastLogin = human.LastLogin;
            this.Name = human.Name;
            this.Password = human.Password;
            this.Role = (Enumerations.PKTasksUserRole)human.Role;
            this.RoleId = human.Role;
            this.Username = human.Username;
            this.Sector = sector.Description;

            AllSectors = new List<SelectListItem>();

            List<Sector> lsectors = srvsc.GetAllSectors();

            foreach (var st in lsectors)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Description;
                item.Value = st.Id.ToString();
                this.AllSectors.Add(item);
            }

            AllRoles = new List<SelectListItem>();
            foreach (Enumerations.PKTasksUserRole role in Enum.GetValues(typeof(Enumerations.PKTasksUserRole)).Cast<Enumerations.PKTasksUserRole>())
            {
                SelectListItem item = new SelectListItem();
                item.Text = role.ToString();
                item.Value = ((int)role).ToString();
                this.AllRoles.Add(item);
            }
            HumanResourceCostsService srvhrc = new HumanResourceCostsService();
            HumanResourceCost cost = srvhrc.ReturnOneHumanResourceCost(DateTime.Now, human.Id);
            if (cost != null)
            {
                this.Cost = cost.Cost;
                this.FromDate = cost.FromDate;
            }
            List<HumanResourceCost> costs = srvhrc.GetAllHumanResourceCosts(human.Id);
                Costs = new HumanResourceCostsVM(costs);
        }


        [Display(Name = "Αναγνωριστικό")]
        public int Id { get; set; }
        [Display(Name = "Όνομα")]
        public string Name { get; set; }
        [Display(Name = "UserName")]
        public string Username { get; set; }
        [Display(Name = "Κωδικός Πρόσβασης")]
        public string Password { get; set; }
        public Nullable<System.DateTime> LastLogin { get; set; }
        [Display(Name = "Ρόλος")]
        public Enumerations.PKTasksUserRole Role { get; set; }
        public byte RoleId { get; set; }
        [Display(Name = "Ιδιότητα")]
        public string Sector { get; set; }
        public int SectorId { get; set; }
        public List<SelectListItem> AllSectors { get; set; }
        public List<SelectListItem> AllRoles { get; set; }
        public decimal Cost { get; set; }
        [UIHint ("DateTimeToDate")]
        public DateTime FromDate { get; set; }

        public HumanResourceCostsVM Costs { get; set; }
    }

    public class HumanResourcesVM
    {
        public HumanResourcesVM(List<HumanResource> humans)
        {
            lstHumansVM = new List<HumanResourceVM>();

            foreach (var human in humans)
            {
                HumanResourceVM HumanTemp = new HumanResourceVM(human);
                lstHumansVM.Add(HumanTemp);
            }

        }
        public List<HumanResourceVM> lstHumansVM { get; set; }
    }
    #endregion



    public class HumanResourceTasksVM
    {
        public HumanResourceTasksVM(List<Task> tasks)
        {
            this.Tasks = tasks;

        }
        public List<Task> Tasks { get; set; }


    }


    #region ProjectVM
    public class ProjectVM
    {
        public ProjectVM()
        {
            StatusService srvst = new StatusService();
            AllStatus = new List<SelectListItem>();
            List<Status> lstatus = srvst.GetSubProjectStatus();
            this.statusId = lstatus[0].Id;

            foreach (var st in lstatus)
            {
                SelectListItem item1 = new SelectListItem();
                item1.Text = st.Name;
                item1.Value = st.Id.ToString();
                this.AllStatus.Add(item1);
            }
        }
        public ProjectVM(Project project)
        {
            AllStatus = new List<SelectListItem>();
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(project.StatusId);
            List<Status> lstatus = srvst.GetProjectStatus();

            this.Id = project.Id;
            this.Status = status.Name;
            this.Name = project.Name;
            this.Description = project.Description;
            this.EstimatedStartDate = project.EstimatedStartDate;
            this.EstimatedEndDate = project.EstimatedEndDate;
            this.StartDate = project.StartDate;
            this.EndDate = project.EndDate;
            
            this.statusId = status.Id;


            foreach (var st in lstatus)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Name;
                item.Value = st.Id.ToString();
                this.AllStatus.Add(item);
            }
            this.Deadline = project.Deadline;
            this.EstimatedDuration = project.EstimatedDuration;
            this.EstimatedDurationType = project.EstimatedDurationType;
        }

        public int Id { get; set; }
        [Display(Name = "Όνομα")]
        public string Name { get; set; }
        [Display(Name = "Περιγραφή")]
        public string Description { get; set; }

        [Display(Name = "Εκτιμώμενη Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public DateTime? EstimatedStartDate { get; set; }


        [Display(Name = "Εκτιμώμενη Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EstimatedEndDate { get; set; }

        [Display(Name = "Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> StartDate { get; set; }

        [Display(Name = "Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Κατάσταση")]
        public string Status { get; set; }
        public int statusId { get; set; }

        [Display(Name = "Οριακή Προθεσμία")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> Deadline { get; set; }
        [Display(Name = "Εκτιμώμενη Διάρκεια (Ημέρες)")]
        public Nullable<int> EstimatedDuration { get; set; }
        [Display(Name = "Τύπος Εκτιμώμενης Διάρκειας")]
        public Nullable<byte> EstimatedDurationType { get; set; }
        public List<SelectListItem> AllStatus { get; set; }
    }


    public class ProjectsVM
    {
        public ProjectsVM(List<Project> projects)
        {

            Projects = new List<ProjectVM>();

            StatusId = 0;
            foreach (var project in projects)
            {
                ProjectVM ProjectTemp = new ProjectVM(project);

                SelectListItem item1 = new SelectListItem();
                item1.Text = "Όλα";
                item1.Value = 0.ToString();
                ProjectTemp.AllStatus.Add(item1);

                Projects.Add(ProjectTemp);

            }

        }
        public List<ProjectVM> Projects { get; set; }
        public int StatusId { get; set; }

    }
    #endregion


    #region subprojectVM
    public class SubProjectVM
    {
        public SubProjectVM()
        {

        }
        public SubProjectVM(int ProjectId)
        {
            StatusService srvst = new StatusService();
            AllStatus = new List<SelectListItem>();
            List<Status> lstatus = srvst.GetSubProjectStatus();
            this.statusId = lstatus[0].Id;
            this.ProjectId = ProjectId;
            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(ProjectId);
            this.ProjectName = project.Name;

            foreach (var st in lstatus)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Name;
                item.Value = st.Id.ToString();
                this.AllStatus.Add(item);
            }
        }

        public SubProjectVM(SubProject subproject)
        {

            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(subproject.StatusId);

            AllStatus = new List<SelectListItem>();
            List<Status> lstatus = srvst.GetSubProjectStatus();


            this.Id = subproject.Id;
            this.ProjectId = subproject.ProjectId;
            this.Name = subproject.Name;
            this.Description = subproject.Description;
            this.EstimatedStartDate = subproject.EstimatedStartDate;
            this.EstimatedEndDate = subproject.EstimatedEndDate;
            this.StartDate = subproject.StartDate;
            this.EndDate = subproject.EndDate;
            this.Status = status.Name;
            this.statusId = status.Id;

            foreach (var st in lstatus)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Name;
                item.Value = st.Id.ToString();
                this.AllStatus.Add(item);
            }
            this.Deadline = subproject.Deadline;
            this.EstimatedDuration = subproject.EstimatedDuration;
            this.EstimatedDurationType = subproject.EstimatedDurationType;

            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(subproject.ProjectId);
            this.ProjectName = project.Name;
        }

        public int Id { get; set; }
        public int ProjectId { get; set; }
        [Display(Name = "Όνομα")]
        public string Name { get; set; }
        [Display(Name = "Περιγραφή")]
        public string Description { get; set; }

        [Display(Name = "Εκτιμώμενη Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EstimatedStartDate { get; set; }

        [Display(Name = "Εκτιμώμενη Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EstimatedEndDate { get; set; }

        [Display(Name = "Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> StartDate { get; set; }

        [Display(Name = "Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Κατάσταση")]
        public string Status { get; set; }
        public int statusId { get; set; }

        [Display(Name = "Οριακή Προθεσμία")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> Deadline { get; set; }
        [Display(Name = "Εκτιμώμενη Διάρκεια (Ημέρες)")]
        public Nullable<int> EstimatedDuration { get; set; }
        [Display(Name = "Τύπος Εκτιμώμενης Διάρκειας")]
        public Nullable<byte> EstimatedDurationType { get; set; }
        public string ProjectName { get; set; }
        public List<SelectListItem> AllStatus { get; set; }

        //public List<SelectListItem>
    }


    public class Project_SubProjectsVM
    {
        public Project_SubProjectsVM(List<SubProject> subprojects, int ProjectId)
        {
            SubProjects = new List<SubProjectVM>();
            ProjectVM = new ProjectVM();
            foreach (var subproject in subprojects)
            {
                SubProjectVM SubProjectTemp = new SubProjectVM(subproject);

                SelectListItem item1 = new SelectListItem();
                item1.Text = "Όλα";
                item1.Value = 0.ToString();
                SubProjectTemp.AllStatus.Add(item1);

                SubProjects.Add(SubProjectTemp);

            }
            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(ProjectId);
            ProjectVM.Id = project.Id;
            ProjectVM.Name = project.Name;
        }
        public List<SubProjectVM> SubProjects { get; set; }
        public ProjectVM ProjectVM { get; set; }

        public int StatusId { get; set; }
    }
    #endregion 



    #region TaskVM
    public class TaskVM
    {
        public TaskVM()
        {
        }


        public TaskVM(int SubProjectId) {
            StatusService srvst = new StatusService();
            AllStatus = new List<SelectListItem>();
            List<Status> lstatus = srvst.GetTasksStatus();
            this.StatusId = lstatus[0].Id;
            this.SubProjectId = SubProjectId;
            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = srvsbpr.ReturnOneSubProject(SubProjectId);
            this.SubProjectName = subproject.Name;
            this.ProjectId = subproject.ProjectId;

            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(subproject.ProjectId);
            this.ProjectName = project.Name;


            foreach (var st in lstatus)
            {
                SelectListItem item = new SelectListItem();
                item.Text = st.Name;
                item.Value = st.Id.ToString();
                this.AllStatus.Add(item);
            }

            HumanResourceService srvhr = new HumanResourceService();
            
            List<HumanResource> lhuman = srvhr.GetAllHumanResources();
            AllHumanResources = new List<SelectListItem>();
            this.HumanResourcesId = lhuman[0].Id;
            foreach (var hr in lhuman)
            {
                SelectListItem item = new SelectListItem();
                item.Text = hr.Name;
                item.Value = hr.Id.ToString();
                this.AllHumanResources.Add(item);
            }

            AllTags = new List<SelectListItem>();
            TaskTagsService srvtg = new TaskTagsService();
            List<TaskTags> ltags = srvtg.GetAllTags();
            //this.TagId = ltags[0].Id;
            SelectListItem itemnew = new SelectListItem();
            this.TagId = Globals.DefddlValue;
            itemnew.Text = "";
            itemnew.Value = Globals.DefddlValue.ToString();
            this.AllTags.Add(itemnew);
            foreach (var tag in ltags)
            {
                SelectListItem item = new SelectListItem();
                item.Text = tag.Name;
                item.Value = tag.Id.ToString();
                this.AllTags.Add(item);
            }


            this.AllEstimatedDurationTypes = new List<SelectListItem>();
            foreach (Enumerations.PKTasksDurationType duration in Enum.GetValues(typeof(Enumerations.PKTasksDurationType)).Cast<Enumerations.PKTasksDurationType>())
            {
                SelectListItem Item = new SelectListItem();
                Item.Text = Globals.GetEnumDescription(duration);
                Item.Value = ((int)duration).ToString();
                this.AllEstimatedDurationTypes.Add(Item);
            }


        }


        public TaskVM(Task task)
        {
            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(task.StatusId);
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(task.HumanResourcesId);

            AllStatus = new List<SelectListItem>();
            List<Status> lstatus = srvst.GetTasksStatus();
            List<HumanResource> lhuman = srvhr.GetAllHumanResources();
            AllHumanResources = new List<SelectListItem>();

            this.Id = task.Id;
            this.SubProjectId = task.SubProjectId;
            this.Name = task.Name;
            this.Description = task.Description;
            this.EstimatedStartDate = task.EstimatedStartDate;
            this.EstimatedEndDate = task.EstimatedEndDate;
            this.StartDate = task.StartDate;
            this.EndDate = task.EndDate;
            this.Status = status.Name;
            this.StatusId = status.Id;



            foreach (var st in lstatus)
            {
                SelectListItem itemnew = new SelectListItem();
                itemnew.Text = st.Name;
                itemnew.Value = st.Id.ToString();
                this.AllStatus.Add(itemnew);
            }

            this.HumanResourcesId = human.Id;
            foreach (var hr in lhuman)
            {
                SelectListItem itemnew = new SelectListItem();
                itemnew.Text = hr.Name;
                itemnew.Value = hr.Id.ToString();
                this.AllHumanResources.Add(itemnew);
            }
            this.HumanResources = human.Name;
            this.Deadline = task.Deadline;
            this.EstimatedDuration = task.EstimatedDuration;
            this.EstimatedDurationType = task.EstimatedDurationType;
            if (this.EstimatedDurationType != null)
            {
                this.EstimatedDurationTypeName = ((Enumerations.PKTasksDurationType)task.EstimatedDurationType).ToString();
            }

            this.AllEstimatedDurationTypes = new List<SelectListItem>();
            foreach (Enumerations.PKTasksDurationType duration in Enum.GetValues(typeof(Enumerations.PKTasksDurationType)).Cast<Enumerations.PKTasksDurationType>())
            {
                SelectListItem Item = new SelectListItem();
                Item.Text = Globals.GetEnumDescription(duration);
                Item.Value = ((int)duration).ToString();
                this.AllEstimatedDurationTypes.Add(Item);
            }

            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = srvsbpr.ReturnOneSubProject(task.SubProjectId);
            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(subproject.ProjectId);

            this.ProjectId = subproject.ProjectId;
            this.ProjectName = project.Name;
            this.SubProjectName = subproject.Name;

            AllTags = new List<SelectListItem>();
            TaskTagsService srvtg = new TaskTagsService();
            List<TaskTags> ltags = srvtg.GetAllTags();
            
            
            if (task.TaskTagsId.HasValue)
             {
                
                this.TagId = task.TaskTagsId.Value;
                TaskTags tag = srvtg.ReturnOneTag(task.TaskTagsId);
                this.TaskTag = tag.Name;
            }
            else
            {
                this.TagId = Globals.DefddlValue;
            }
            
            SelectListItem item = new SelectListItem();
            item.Text = "";
            item.Value = Globals.DefddlValue.ToString();
            this.AllTags.Add(item);
            foreach (var tag in ltags)
            {
                SelectListItem itemnew = new SelectListItem();
                itemnew.Text = tag.Name;
                itemnew.Value = tag.Id.ToString();
                this.AllTags.Add(itemnew);
            }

            List<Note> notes = new List<Note>();
            NotesService srvnt = new NotesService();


            notes = srvnt.GetAllNotes(task.Id, (byte)Enumerations.PKTasksEntryType.Task);

            Notes = new NotesVM(notes);

            TasksWorkingDurationsService srvtwd = new TasksWorkingDurationsService();
            List<TaskWorkingDuration> durations = new List<TaskWorkingDuration>();
            durations = srvtwd.GetAllWorkingDurations(task.Id);
            WorkingDurations = new WorkingDurationsVM(durations);
        }

        public int Id { get; set; }
        public int SubProjectId { get; set; }
        [Display(Name = "Όνομα")]
        public string Name { get; set; }
        [Display(Name = "Περιγραφή")]
        public string Description { get; set; }

        [Display(Name = "Εκτιμώμενη Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EstimatedStartDate { get; set; }

        [Display(Name = "Εκτιμώμενη Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EstimatedEndDate { get; set; }

        [Display(Name = "Ημερομηνία Έναρξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> StartDate { get; set; }

        [Display(Name = "Ημερομηνία Λήξης")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Display(Name = "Κατάσταση")]
        public string Status { get; set; }
        public byte StatusId { get; set; }
        [Display(Name = "Όνομα Υπαλλήλου προς Ανάθεση")]
        public string HumanResources { get; set; }
        public int HumanResourcesId { get; set; }

        [Display(Name = "Οριακή Προθεσμία")]
        [UIHint("NullableDatePicker")]
        public Nullable<System.DateTime> Deadline { get; set; }
        [Display(Name = "Εκτιμώμενη Διάρκεια")]
        public Nullable<int> EstimatedDuration { get; set; }
        [Display(Name = "Τύπος Εκτιμώμενης Διάρκειας")]
        public Nullable<byte> EstimatedDurationType { get; set; }
        public string EstimatedDurationTypeName { get; set; }

        public List<SelectListItem> AllEstimatedDurationTypes { get; set; }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string SubProjectName { get; set; }
        public List<SelectListItem> AllStatus { get; set; }
        public List<SelectListItem> AllHumanResources { get; set; }
        public byte? TagId { get; set; }
        public List<SelectListItem> AllTags { get; set; }

        [Display(Name = "Ετικέτα")]
        public string TaskTag { get; set; }

        public NotesVM Notes { get; set; }

        public WorkingDurationsVM WorkingDurations { get; set; }
    }




    public class SubProjects_TasksVM
    {
        public SubProjects_TasksVM(List<Task> tasks,int SubProjectId)
        {
            Tasks = new List<TaskVM>();
            Subproject = new SubProjectVM();
            foreach (var task in tasks)
            {
                TaskVM TaskTemp = new TaskVM(task);

                SelectListItem item1 = new SelectListItem();
                item1.Text = "Όλα";
                item1.Value = 0.ToString();
                TaskTemp.AllStatus.Add(item1);

                Tasks.Add(TaskTemp);
            }

            SubProjectService srvsbpr = new SubProjectService();
            SubProject subproject = srvsbpr.ReturnOneSubProject(SubProjectId);
            Subproject.Id = subproject.Id;
            Subproject.Name = subproject.Name;
            ProjectService srvpr = new ProjectService();
            Project project = srvpr.ReturnOneProject(subproject.ProjectId);
            Subproject.ProjectId = subproject.ProjectId;
            Subproject.ProjectName = project.Name;

        }
        public List<TaskVM> Tasks { get; set; }

        public SubProjectVM Subproject { get; set; }

        public int StatusId { get; set; }
    }
    #endregion


    #region NotesVM
    public class NoteVM
    {
        public NoteVM()
        {

        }

        public NoteVM(int humanresourceId, int EntryId, Enumerations.PKTasksEntryType entrytypeId)
        {
            this.EntryId = EntryId;
            this.HumanResourceId = humanresourceId;
            this.EntryType = entrytypeId;
        }
        public NoteVM(Note note)
        {
            this.Id = note.Id;
            this.EntryId = note.EntryId;
            this.PostDate = note.PostDate;
            this.Message = note.Message;
            this.HumanResourceId = note.HumanResourceId;
            this.EntryType = (Enumerations.PKTasksEntryType)note.EntryTypeId;

            HumanResourceService srvhr = new HumanResourceService();
            HumanResource humanresource = srvhr.ReturnOneHumanResource(this.HumanResourceId);
            this.HumanResourceName = humanresource.Name;
        }

        public int Id { get; set; }
        [Display(Name = "Ημερομηνία")]
        [DataType(DataType.DateTime),DisplayFormat(DataFormatString ="{0:dd/MM/yyyy HH:mm}")]
        public DateTime PostDate { get; set; }
        [Display(Name = "Σημείωση")]
        public string Message { get; set; }
        //public string SubMessage { get; set; }
        public int HumanResourceId { get; set; }
        [Display(Name = "Συγγραφέας")]
        public string HumanResourceName { get; set; }
        public int EntryId { get; set; }
        public Enumerations.PKTasksEntryType EntryType { get; set; }
    }

    public class NotesVM
    {
        public NotesVM(List<Note> notes)
        {
            Notes = new List<NoteVM>();

            foreach (var note in notes)
            {
                NoteVM NoteTemp = new NoteVM(note);
                Notes.Add(NoteTemp);
            }
        }

        public List<NoteVM> Notes { get; set; }
    }
    #endregion


    #region Working Duration
    public class WorkingDurationVM
    {
        public WorkingDurationVM()
        {

        }

        public WorkingDurationVM(int TaskId)
        {
            TaskService srvtk = new TaskService();
            Task task = srvtk.ReturnOneTask(TaskId);
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(task.HumanResourcesId);
            this.TaskId = TaskId;
            this.HumanResourceId = task.HumanResourcesId;
            this.HumanResourceCostId = human.HumanResourceCosts.Last().Id;
            this.TaskName = task.Name;
            this.WorkingDate = DateTime.Now.Date;
        }

        public WorkingDurationVM(TaskWorkingDuration workingduration)
        {
            this.Id = workingduration.Id;
            if (workingduration.HumanResourceCostsId.HasValue)
            {
                this.HumanResourceCostId = workingduration.HumanResourceCostsId;
            }
            this.HumanResourceId = workingduration.HumanResourceId;
            this.TaskId = workingduration.TaskId;
            this.WorkingDate = workingduration.WorkingDate;
            this.Duration = workingduration.Duration;
        }

        public int Id { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }

        //[UIHint("DayDate")]
        [UIHint("NullableDatePicker")]
        public DateTime WorkingDate { get; set; }
        public int? HumanResourceCostId { get; set; }
        public int HumanResourceId { get; set; }
        public int Duration { get; set; }
    }


    public class WorkingDurationsVM
    {
        public WorkingDurationsVM(List<TaskWorkingDuration> durations)
        {
            Durations = new List<WorkingDurationVM>();

            foreach (var duration in durations)
            {
                WorkingDurationVM DurationTemp = new WorkingDurationVM(duration);
                Durations.Add(DurationTemp);
            }
        }

        public List<WorkingDurationVM> Durations { get; set; }
    }
    #endregion




}


