﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PKTasks.Web.Models
{
    public class ViewModelBase
    {
        [Display(Name = "Ημ. Δημιουργίας")]
        [UIHint("DateTimeOffset-DateTime")]
        public virtual DateTimeOffset Created { get; set; }

        [Display(Name = "Ημ. Τροποποίησης")]
        [UIHint("DateTimeOffset-DateTime")]
        public virtual DateTimeOffset Modified { get; set; }
    }
}