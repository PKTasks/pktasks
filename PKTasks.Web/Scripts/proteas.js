﻿function showMainModal()
{
    $('#mainmodal').modal('show');
}

function showMainModalAction(actionUrl)
{
    $('#mainmodal .modal-body').load(actionUrl);
    $('#mainmodal').modal('show');
}

function hideMainModal()
{
    $('#mainmodal').modal('hide');
}

var ProteasJS =
{
    DefaultDate : new Date(0),
    MessageTypes: {
        Success: 'Success',
        Warning: 'Warning',
        Danger: 'Danger',
        Info: 'info'
    },
    formatDate : function(date)
    {
        if (date.getTime() == ProteasJS.DefaultDate.getTime())
        {
            return "-";
        }
        else
        {
            return ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
        }
    },
    formatBool : function(data)
    {
        return (data === true ? "Ναι" : "Όχι");
    },
    showToastr : function(message)
    {
        if (message.Body === '') return;

        switch (message.MessageTypeText) {
            case "Success":
                toastr.success(message.Body, message.Header);
                break;

            case "Warning":
                toastr.warning(message.Body, message.Header);
                break;

            case "Danger":
                toastr.error(message.Body, message.Header);
                break;

            default:
                toastr.info(message.Body, message.Header);
                break;
        }
    },
    showToastrMessage : function(header, body, messageType)
    {
        switch(messageType)
        {
            case ProteasJS.MessageTypes.Success:
                toastr.success(body, header);
                break;

            case ProteasJS.MessageTypes.Warning:
                toastr.warning(body, header);
                break;

            case ProteasJS.MessageTypes.Danger:
                toastr.error(body, header);
                break;

            default:
                toastr.info(message.Body, message.Header);
                break;
        }
    },
    showModal : function(actionUrl, onHide, onShow)
    {
        $('#mainmodal .modal-body').load(actionUrl);
        $('#mainmodal').modal('show');

        var callback = function () {            
            $('#mainmodal').off('hide.bs.modal', callback);
            onHide();
        };

        var callbackShow = function () {
            $('#mainmodal').off('shown.bs.modal', callback);
            onShow();
        };

        $('#mainmodal').on('hide.bs.modal', callback);
        $('#mainmodal').on('shown.bs.modal', callbackShow);
    },

    /**
     *  Εμφανίζει ένα modal window
        {
            actionUrl   : string            [το url που θα επιστρέψει το περιεχόμενο του modal]
            onHide      : function          [καλείται όταν ξεκινά η απόκρυψη του modal window]
            onShow      : function          [καλείται όταν ξεκινά η εμφάνιση του modal window]
            size        : "lg" / "sm"       [ορίζει το μέγεθος του modal lg = μεγάλο, sm = μικρό]
            title       : string            [ο τίτλος του παραθύρου]
            onLoaded    : function          [καλείται όταν ολοκληρωθεί η (ajax) φόρτωση του modal
        }
     */
    showModalWindow : function(data)
    {
        var mainModal = $('#mainmodal');
        var mainModalDialog = $('#mainmodal .modal-dialog');
        var mainModalBody = $('#mainmodal .modal-body');

        mainModalBody.empty();
        mainModalBody.load(data.actionUrl, data.onLoaded);
        mainModal.modal('show');

        if (data.onHide)
        {
            var callback = function () {
                mainModal.off('hide.bs.modal', callback);
                data.onHide();
            };
            mainModal.on('hide.bs.modal', callback);
        }

        if (data.onShow)
        {
            var callbackShow = function () {
                mainModal.off('shown.bs.modal', callbackShow);
                data.onShow();
            };

            mainModal.on('shown.bs.modal', callbackShow);
        }

        if (data.size)
        {            
            mainModalDialog.addClass('modal-' + data.size);
        }

        if (data.title)
        {
            $('#mainmodal .modal-title').text(data.title);
        }
    },
    hideModalWindow:function()
    {
        $('#mainmodal').modal('hide');
    },
    clearModalWindow: function()
    {
        var mainModalBody = $('#mainmodal .modal-body');
        mainModalBody.empty();
    },

    showConfirm: function(title, messageHtml, onYes, onNo)
    {
        var html = '<div class="modal inmodal" tabindex="-1" role="dialog" aria-hidden="true">' +
                        '<div class="modal-dialog">' +
                            '<div class="modal-content animated bounceInDown">' +
                                '<div class="modal-header">' +
                                    '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                                    '<i class="fa fa-question-circle modal-icon"></i>' +
                                    '<h6 class="modal-title">' + title + '</h6>' +
                                '</div>' +
                                '<div class="modal-body"><h4>' + messageHtml + '</h4></div>' +
                                '<div class="modal-footer">' +
                                    '<button type="button" class="btn btn-primary confirm-yes">Ναι</button>' +
                                    '<button type="button" class="btn btn-white confirm-no" data-dismiss="modal">Όχι</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

        var container = $("<div></div>");
        $(document.body).append(container);
        container.html(html);
        container.find('.modal').modal('show');

        container.find('.confirm-yes').click(function () {
            if ((onYes !== undefined) && (typeof onYes == "function"))
            {
                onYes();
                container.find('.modal').modal('hide');
            }
        });

        container.find('.confirm-no').click(function () {
            if ((onNo !== undefined) && (typeof onNo == "function")) {
                onNo();
                container.find('.modal').modal('hide');
            }
        });
    },
    parseDate: function (dateStr)
    {
        var year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;

        var parts = dateStr.split(" ");
        for (var i = 0; i < parts.length; i++)
        {
            if (parts[i].indexOf('/') > -1)
            {
                var dateParts = parts[i].split("/");
                year = parseInt(dateParts[2], 10);
                month = parseInt(dateParts[1], 10) - 1;
                day = parseInt(dateParts[0], 10);
            }
            else if (parts[i].indexOf(':') > -1)
            {
                var timeParts = parts[i].split(":");
                hour = parseInt(timeParts[0], 10);
                minute = parseInt(timeParts[1], 10);
                second = parseInt(timeParts[2], 10);
            }
        }        

        var dt = new Date(year, month, day, hour, minute, second);
        return dt.getTime();
    },
    compareDates: function (strDate1, strDate2)
    {
        var x = ProteasJS.parseDate(strDate1.trim());
        var y = ProteasJS.parseDate(strDate2.trim());
        var r = ((x < y) ? -1 : ((x > y) ? 1 : 0));
        return r;
    },
    dataTablesCompareDates:function()
    {
        $.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b)
        {
            return ProteasJS.compareDates(a, b);
        };

        $.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b)
        {
            return -ProteasJS.compareDates(a, b);
        };
    }
};

jQuery.extend({
    showAutoHideMessage: function (message) {
        // message is an object of type {Header:'xxx', Body:'xxx'}
        $('.modal-content').html('<div class="callout callout-success"><h4><i class="fa fa-check"></i> ' + message.Header + '</h4><p>' + message.Body + '</p></div>');
        $('#modal-container').modal('show');
        setTimeout(function () { $('#modal-container').modal('hide'); }, 3000);
    },
    postJSON : function (url, data, callback, error) {
        return jQuery.ajax({
            'type': 'POST',
            'url': url,
            'contentType': 'application/json',
            'data': JSON.stringify(data),
            'dataType': 'json',
            'success': callback,
            'error': function (jqXHR) {
                res = null;
                alert("Σφάλμα : {0}\nΕισερχόμενο μήνυμα : {1}".format(jqXHR, jqXHR.responseText));
            }
        });
    }
});



// DateOffsetGreaterThan Client Validation
$.validator.addMethod("dateoffsetgreaterthan", function (value, element, params) {
    // parsing ημερομηνιών για αποφυγή προβλημάτων locale του browser (dd/MM/yyyy ή ΜΜ/dd/yyyy)
    var checkingSplit = value.split('/');
    var checkingValue = new Date(checkingSplit[2], checkingSplit[1] - 1, checkingSplit[0]);

    var comparingSplit = $(params).val().split('/');
    var comparingValue = new Date(comparingSplit[2], comparingSplit[1] - 1, comparingSplit[0]);

    return checkingValue > comparingValue;
});

$.validator.unobtrusive.adapters.add("dateoffsetgreaterthan", ["otherproperty"], function (options) {
    options.rules["dateoffsetgreaterthan"] = "#" + options.params.otherproperty;
    options.messages["dateoffsetgreaterthan"] = options.message;
});


// IntegerGreaterThan Client Validation
$.validator.addMethod("integergreaterthan", function (value, element, params)
{
    var checkingValue = parseInt(value);
    var comparingValue = parseInt($(params).val());

    return checkingValue > comparingValue;
});

$.validator.unobtrusive.adapters.add("integergreaterthan", ["otherproperty"], function (options)
{
    options.rules["integergreaterthan"] = "#" + options.params.otherproperty;
    options.messages["integergreaterthan"] = options.message;
});


jQuery.extend(jQuery.validator.methods, {    
    number: function (value, element) {
        // regular expression to allow comma as decimal separator
        return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
    }
});

// global error handling
$('body').ajaxError(function (evt, jqXHR) {
    var error = $.parseJSON(jqXHR.responseText);
    toastr.error(error.errorMessage, 'Σφάλμα');
});
