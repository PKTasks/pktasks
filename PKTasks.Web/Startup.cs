﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PKTasks.Web.Startup))]
namespace PKTasks.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
