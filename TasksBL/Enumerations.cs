﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksBL
{
    public class Enumerations
    {
        public enum PKTasksState
        {
            Added,
            Edited
        }

        public enum PKTasksDurationType
        {
            [Display(Name = "Ημέρες")]
            [Description("Ημέρες")]
            Day = 1,
            [Display(Name = "Ώρες")]
            [Description("Ώρες")]
            Hour = 2 
        }

        public enum PKTasksUserRole
        {
            Unknown = 0,
            Διαχειριστής = 1,
            Χρήστης = 2
        }

        public enum PKTasksEntryType
        {
            Task = 1,
            SubProject = 2,
            Project = 3

        }
    }
}
