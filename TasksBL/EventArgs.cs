﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class HumanResourcesEventArgs : EventArgs
    {
        public TasksDB.HumanResource HumanResource { get; set; }
    }

    public class SEctorsEventArgs : EventArgs
    {
        public TasksDB.Sector Sector { get; set; }
    }

    public class ProjectsEventArgs : EventArgs
    {
        public TasksDB.Project Project { get; set; }
    }

    public class SubProjectsEventArgs : EventArgs
    {
        public TasksDB.SubProject SubProject { get; set; }
    }

    public class TasksEventArgs : EventArgs
    {
        public TasksDB.Task Task { get; set; }
    }

    public class TaskWorkingDurationsArgs : EventArgs
    {
        public TaskWorkingDuration WorkingDuration { get; set; }
    }

    public class HumanResourceCostArgs : EventArgs
    {
        public HumanResourceCost humanresourcecost { get; set; }
    }

    public class NoteArgs : EventArgs
    {
        public Note Note { get; set; }
    }
}
