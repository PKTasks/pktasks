﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksBL;
using TasksDB;

namespace TasksBL
{
    public class HumanResourceCostsService
    {
        public event EventHandler<HumanResourceCostArgs> OnHumanResourceCostAdded;
        public event EventHandler<HumanResourceCostArgs> OnHumanResourceCostEdited;
        public event EventHandler<HumanResourceCostArgs> OnHumanResourceCostRemove;

        public void Add(TasksDB.HumanResourceCost hrc)
        {
            DBAccess DB = new DBAccess();
            DB.AddHumanResourceCost(hrc);

            if (OnHumanResourceCostAdded != null)
            {
                HumanResourceCostArgs args = new HumanResourceCostArgs();
                args.humanresourcecost = hrc;
                OnHumanResourceCostAdded.Invoke(this, args);
                //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + "OnCustomerAdded");
            }

        }


        public void Edit(TasksDB.HumanResourceCost hrc)
        {
            DBAccess DB = new DBAccess();
            DB.EditHumanResourceCost(hrc);
            if (OnHumanResourceCostEdited != null)
            {
                HumanResourceCostArgs args = new HumanResourceCostArgs();
                args.humanresourcecost = hrc;
                OnHumanResourceCostEdited.Invoke(this, args);
            }

        }


        public void Remove(TasksDB.HumanResourceCost hrc)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveHumanResourceCost(hrc.Id);
            if (OnHumanResourceCostRemove != null)
            {
                HumanResourceCostArgs args = new HumanResourceCostArgs();
                args.humanresourcecost = hrc;
                OnHumanResourceCostRemove.Invoke(this, args);
            }
        }

        public List<HumanResourceCost> GetAllHumanResourceCosts(int HumanResourceId)
        {
            DBAccess DB = new DBAccess();
            List<HumanResourceCost> costs = new List<HumanResourceCost>();
            costs = DB.GetAllHumanResourceCosts(HumanResourceId);
            return costs;
        }

        public HumanResourceCost ReturnOneHumanResourceCostbyId(int humanresourceId)
        {
            DBAccess DB = new DBAccess();
            HumanResourceCost cost = new HumanResourceCost();
            cost = DB.ReturnOneHumanResourceCostbyId(humanresourceId);
            return cost;
        }

        public HumanResourceCost ReturnOneHumanResourceCost(DateTime fromdate, int humanresourceId)
        {
            DBAccess DB = new DBAccess();
            HumanResourceCost cost = new HumanResourceCost();
            cost = DB.ReturnOneHumanResourceCost(fromdate, humanresourceId);
            return cost;
        }
    }
}
