﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class HumanResourceService
    {
        public event EventHandler<HumanResourcesEventArgs> OnHumanResourceAdded;
        public event EventHandler<HumanResourcesEventArgs> OnHumanResourceEdit;
        public event EventHandler<HumanResourcesEventArgs> OnHumanResourceRemove;


        public void Add(TasksDB.HumanResource hr)
        {
            DBAccess DB = new DBAccess();
            DB.AddHumanResource(hr);

            if (OnHumanResourceAdded != null)
            {
                HumanResourcesEventArgs args = new HumanResourcesEventArgs();
                args.HumanResource = hr;
                OnHumanResourceAdded.Invoke(this, args);
                //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + "OnCustomerAdded");
            }
            
        }


        public void Edit(TasksDB.HumanResource human)
        {
            DBAccess DB = new DBAccess();
            DB.EditHumanResource(human);
            if (OnHumanResourceEdit != null)
            {
                HumanResourcesEventArgs args = new HumanResourcesEventArgs();
                args.HumanResource = human;
                OnHumanResourceEdit.Invoke(this, args);
            }

        }


        public void Remove(TasksDB.HumanResource human)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveHumanResource(human.Id);
            if (OnHumanResourceRemove != null)
            {
                HumanResourcesEventArgs args = new HumanResourcesEventArgs();
                args.HumanResource = human;
                OnHumanResourceRemove.Invoke(this, args);
            }
        }


        public List<HumanResource> GetAllHumanResources()
        {

            DBAccess DB = new DBAccess();
            List<HumanResource> HumanResources = new List<HumanResource>();
            HumanResources = DB.GetAllHumanResources();
            return HumanResources;
        }


        public HumanResource GetHumanResource(String username)
        {
            DBAccess DB = new DBAccess();
            HumanResource human = new HumanResource();
            human = DB.GetHumanResourceByUsername(username);
            return human;
        }

        public HumanResource ReturnOneHumanResource(int hrId)
        {
            DBAccess DB = new DBAccess();
            HumanResource human = new HumanResource();
            human = DB.ReturnOneHumanResource(hrId);
            return human;
        }


        public HumanResource ReturnOneHumanResource(string name)
        {
            DBAccess DB = new DBAccess();
            HumanResource human = new HumanResource();
            human = DB.ReturnOneHumanResource(name);
            return human;
        }

        public bool ValidateHumanRecourceByCredentials(string username, string password)
        {
            HumanResource human = new HumanResource();
            human = GetHumanResource(username);
            if (human != null)
            {
                if (username == human.Username && password == human.Password)
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            else
            {
                return false;
            }
        }

        

        public HumanResource GetHumanRecourceByCredentials(string username, string password)
        {
            HumanResource human = new HumanResource();
            DBAccess DB = new DBAccess();
            human = DB.ReturnOneHumanResource(username, password);
            return human;
        }

        public Enumerations.PKTasksUserRole GetHumanResourceRole(int userId)
        {
            DBAccess DB = new DBAccess();
            return (Enumerations.PKTasksUserRole)DB.GetHumanResourceRole(userId);
        }
    }
}
