﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class NotesService
    {
        public event EventHandler<NoteArgs> OnNoteAdded;
        public event EventHandler<NoteArgs> OnNoteEdited;
        public event EventHandler<NoteArgs> OnNoteRemove;

        public void Add(TasksDB.Note note)
        {
            DBAccess DB = new DBAccess();
            DB.AddNote(note);

            if (OnNoteAdded != null)
            {
                NoteArgs args = new NoteArgs();
                args.Note = note;
                OnNoteAdded.Invoke(this, args);
                //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + "OnCustomerAdded");
            }

        }


        public void Edit(TasksDB.Note note)
        {
            DBAccess DB = new DBAccess();
            DB.EditNote(note);
            if (OnNoteEdited != null)
            {
                NoteArgs args = new NoteArgs();
                args.Note = note;
                OnNoteEdited.Invoke(this, args);
            }

        }


        public void Remove(TasksDB.Note note)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveNote(note.Id);
            if (OnNoteRemove != null)
            {
                NoteArgs args = new NoteArgs();
                args.Note = note;
                OnNoteRemove.Invoke(this, args);
            }
        }

        //public List<Note> GetAllNotes(int taskId, byte Type, int humanId)
        //{
        //    DBAccess DB = new DBAccess();
        //    List<Note> notes = new List<Note>();
        //    HumanResourceService srvhr = new HumanResourceService();
        //    notes = DB.GetAllNotesForHumanResource(taskId, Type, srvhr.ReturnOneHumanResource(humanId));
        //    return notes;
        //}

        public List<Note> GetAllNotes(int taskId, byte Type)
        {
            DBAccess DB = new DBAccess();
            List<Note> notes = new List<Note>();
            notes = DB.GetAllNotesForAdmin(taskId, Type);
            return notes;
        }

        public Note ReturnOneNote(Note note)
        {
            DBAccess DB = new DBAccess();
            Note Note = new Note();
            Note = DB.ReturnOneNote(note.PostDate, note.EntryId);
            return Note;
        }

        public Note ReturnOneNotebyId(int noteId)
        {
            DBAccess DB = new DBAccess();
            Note Note = new Note();
            Note = DB.ReturnOneNotebyId(noteId);
            return Note;
        }
    }
}
