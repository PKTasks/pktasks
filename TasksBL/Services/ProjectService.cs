﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class ProjectService
    {
        public static event EventHandler<ProjectsEventArgs> OnProjectAdded;
        public event EventHandler<ProjectsEventArgs> OnProjectEdit;
        public event EventHandler<ProjectsEventArgs> OnProjectRemove;

        public void Add(TasksDB.Project Project)
        {
            DBAccess DB = new DBAccess();
            DB.AddProject(Project);
            if (OnProjectAdded != null)
            {
                ProjectsEventArgs args = new ProjectsEventArgs();
                args.Project = Project;
                OnProjectAdded.Invoke(this, args);
                
            }
        }

        public void Edit(TasksDB.Project Project)
        {
            DBAccess DB = new DBAccess();
            DB.EditProject(Project);
            if (OnProjectEdit != null)
            {
                ProjectsEventArgs args = new ProjectsEventArgs();
                args.Project = Project;
                OnProjectEdit.Invoke(this, args);
            }
        }

        public void Remove(TasksDB.Project Project)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveProject(Project.Id);
            if (OnProjectRemove != null)
            {
                ProjectsEventArgs args = new ProjectsEventArgs();
                args.Project = Project;
                OnProjectRemove.Invoke(this, args);
            }
        }


        public List<Project> GetAllProjects(Expression<Func<Project, bool>> IsDeletedfilter = null, Expression<Func<Project, bool>> filter1 = null)
        {
            DBAccess DB = new DBAccess();
            List<Project> Projects = new List<Project>();
            Projects = DB.GetAllProjects(IsDeletedfilter, filter1);
            return Projects;
        }

        public Project ReturnOneProject(int projectId)
        {
            DBAccess DB = new DBAccess();
            Project project = new Project();
            project = DB.ReturnOneProject(projectId);
            return project;
        }
    }
}
