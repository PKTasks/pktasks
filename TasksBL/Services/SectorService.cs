﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class SectorService
    {
        public event EventHandler<SEctorsEventArgs> OnSectorAdded;
        public event EventHandler<SEctorsEventArgs> OnSectorEdit;
        public event EventHandler<SEctorsEventArgs> OnSectorRemove;

        public void Add(TasksDB.Sector Sector)
        {
            DBAccess DB = new DBAccess();
            DB.AddSector(Sector);
            if (OnSectorAdded != null)
            {
                SEctorsEventArgs args = new SEctorsEventArgs();
                args.Sector = Sector;
                OnSectorAdded.Invoke(this, args);
            }
        }

        public void Edit(TasksDB.Sector Sector)
        {
            DBAccess DB = new DBAccess();
            DB.EditSector(Sector);
            if (OnSectorEdit != null)
            {
                SEctorsEventArgs args = new SEctorsEventArgs();
                args.Sector = Sector;
                OnSectorEdit.Invoke(this, args);
            }
        }

        public void Remove(TasksDB.Sector Sector)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveSector(Sector.Id);
            if (OnSectorRemove != null)
            {
                SEctorsEventArgs args = new SEctorsEventArgs();
                args.Sector = Sector;
                OnSectorRemove.Invoke(this, args);
            }
        }

        public List<Sector> GetAllSectors()
        {
            DBAccess DB = new DBAccess();
            List<Sector> Sectors = new List<Sector>();
            Sectors = DB.GetAllSectors();
            return Sectors;
        }

        public Sector ReturnOneSector(int id)
        {
            DBAccess DB = new DBAccess();
            Sector sector = DB.ReturnOneSector(id);
            return sector;
        }
    }
}
