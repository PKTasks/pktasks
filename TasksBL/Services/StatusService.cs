﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class StatusService
    {
        public List<Status> GetProjectStatus()
        {
            DBAccess DB = new DBAccess();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.GetProjectStatus();
            return Status;
        }

        public List<Status> GetSubProjectStatus()
        {
            DBAccess DB = new DBAccess();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.GetSubProjectStatus();
            return Status;
        }

        public List<Status> GetTasksStatus()
        {
            DBAccess DB = new DBAccess();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.GetTasksStatus();
            return Status;
        }

        public Status ReturnOneStatus(int statusId)
        {
            DBAccess DB = new DBAccess();
            Status status = new Status();
            status = DB.ReturnOneStatus(statusId);
            return status;
        }

        public Status ReturnOneStatus(string name)
        {
            DBAccess DB = new DBAccess();
            Status status = new Status();
            status = DB.ReturnOneStatus(name);
            return status;
        }


    }
}
