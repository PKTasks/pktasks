﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TasksBL;
using TasksDB;

namespace TasksBL
{
    public class SubProjectService
    {

        public static event EventHandler<SubProjectsEventArgs> OnSubProjectAdded;
        public event EventHandler<SubProjectsEventArgs> OnSubProjectEdit;
        public event EventHandler<SubProjectsEventArgs> OnSubProjectRemove;

        public void Add(TasksDB.SubProject SubProject)
        {
            DBAccess DB = new DBAccess();
            DB.AddSubProject(SubProject);
            if (OnSubProjectAdded != null)
            {
                SubProjectsEventArgs args = new SubProjectsEventArgs();
                args.SubProject = SubProject;
                OnSubProjectAdded.Invoke(this, args);

            }
        }

        public void Edit(TasksDB.SubProject SubProject)
        {
            DBAccess DB = new DBAccess();
            DB.EditSubProject(SubProject);
            if (OnSubProjectEdit != null)
            {
                SubProjectsEventArgs args = new SubProjectsEventArgs();
                args.SubProject = SubProject;
                OnSubProjectEdit.Invoke(this, args);
            }
        }

        public void Remove(TasksDB.SubProject SubProject)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveSubProject(SubProject.Id);
            if (OnSubProjectRemove != null)
            {
                SubProjectsEventArgs args = new SubProjectsEventArgs();
                args.SubProject = SubProject;
                OnSubProjectRemove.Invoke(this, args);
            }
        }


        public List<SubProject> GetAllSubProjects(Expression<Func<SubProject, bool>> filter = null)
        {
            DBAccess DB = new DBAccess();
            List<SubProject> SubProjects = new List<SubProject>();
            SubProjects = DB.GetAllSubProjects(filter);
            return SubProjects;
        }


        public List<SubProject> GetProjectSubProjects(int ProjectId, Expression<Func<SubProject, bool>> IsDeletedfilter = null, Expression<Func<SubProject, bool>> filter1 = null)
        {
            DBAccess DB = new DBAccess();
            List<SubProject> SubProjects = new List<SubProject>();
            SubProjects = DB.GetProjectSubProjects(ProjectId, IsDeletedfilter,filter1);
            return SubProjects;
        }

        public SubProject ReturnOneSubProject(int subprojectId)
        {
            DBAccess DB = new DBAccess();
            SubProject subproject = new SubProject();
            subproject = DB.ReturnOneSubProject(subprojectId);
            return subproject;
        }

        //    public SubProject ReturnProjectsOneSubProject(int projectId)
        //    {
        //        DBAccess DB = new DBAccess();
        //        SubProject subproject = new SubProject();
        //        subproject = DB.ReturnProjectsOneSubProject(projectId);
        //        return subproject;
        //    }


        public void Project_SubprojectsRemove(int projectId)
        {
            SubProjectService srvsbpr = new SubProjectService();
            List<SubProject> subprojects = srvsbpr.GetProjectSubProjects(projectId, x => x.IsDeleted == false);
            TaskService srvtk = new TaskService();
            foreach (var subproject in subprojects)
            {
                subproject.IsDeleted = true;
                srvsbpr.Edit(subproject);
                List<TasksDB.Task> tasks = srvtk.GetSubProjectTasks(subproject.Id, x => x.IsDeleted == false);
                foreach (var task in tasks)
                {
                    task.IsDeleted = true;
                    srvtk.Edit(task);
                }
            }
        }


        //public void GetSubprojectSectors(int subprojectId)
        //{
        //    DBAccess DB = new DBAccess();
        //    SubProject subproject = new SubProject();
            
        //    SubProjects = DB.GetSubprojectSectors(subprojectId);
        //    return SubProjects;
        //}
    }
}
