﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
//using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class TaskService
    {
        public event EventHandler<TasksEventArgs> OnTaskAdded;
        public event EventHandler<TasksEventArgs> OnTaskEdit;
        public event EventHandler<TasksEventArgs> OnTaskRemove;

        public void Add(TasksDB.Task Task)
        {
            DBAccess DB = new DBAccess();
            DB.AddTask(Task);
            if (OnTaskAdded != null)
            {
                TasksEventArgs args = new TasksEventArgs();
                args.Task = Task;
                OnTaskAdded.Invoke(this, args);

            }
        }

        public void Edit(TasksDB.Task Task)
        {
            DBAccess DB = new DBAccess();
            DB.EditTask(Task);
            if (OnTaskEdit != null)
            {
                TasksEventArgs args = new TasksEventArgs();
                args.Task = Task;
                OnTaskEdit.Invoke(this, args);
            }
        }

        public void Remove(TasksDB.Task Task)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveTask(Task.Id);
            if (OnTaskRemove != null)
            {
                TasksEventArgs args = new TasksEventArgs();
                args.Task = Task;
                OnTaskRemove.Invoke(this, args);
            }
        }


        public List<Task> GetSubProjectTasks(int SubProjectId, Expression<Func<Task, bool>> IsDeletedfilter = null, Expression<Func<Task, bool>> filter = null)
        {
            DBAccess DB = new DBAccess();
            List<Task> Tasks = new List<Task>();
            Tasks = DB.GetSubProjectTasks(SubProjectId, IsDeletedfilter,filter);
            return Tasks;
        }


        public List<Task> GetHumanResourceTasks(int humanId, Expression<Func<Task, bool>> filter = null)
        {
            DBAccess DB = new DBAccess();
            List<Task> Tasks = new List<Task>();
            Tasks = DB.GetHumanResourceTasks(humanId, filter);
            return Tasks;
        }


        public Task ReturnOneTask(int id)
        {
            DBAccess DB = new DBAccess();
            Task task = DB.ReturnOneTask(id);
            return task;
        }

        //public Task ReturnSubProjectsOneTask(int subprojectid, int Id)
        //{
        //    DBAccess DB = new DBAccess();
        //    Task task = DB.ReturnSubProjectsOneTask(subprojectid, Id);
        //    return task;
        //}


        public void Subproject_TasksRemove(int subprojectId)
        {
            TaskService srvtk = new TaskService();
            List<TasksDB.Task> tasks = srvtk.GetSubProjectTasks(subprojectId, x => x.IsDeleted == false);
            foreach (var task in tasks)
            {
                task.IsDeleted = true;
                srvtk.Edit(task);
            }

        }
    }
}
