﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksBL;
using TasksDB;

namespace TasksBL
{
    public class TaskTagsService
    {

        //public event EventHandler<HumanResourcesEventArgs> OnHumanResourceAdded;
        //public event EventHandler<HumanResourcesEventArgs> OnHumanResourceEdit;
        //public event EventHandler<HumanResourcesEventArgs> OnHumanResourceRemove;


        public void Add(TasksDB.TaskTags tag)
        {
            DBAccess DB = new DBAccess();
            DB.AddTaskTag(tag);



        }


        public void Edit(TasksDB.TaskTags tag)
        {
            DBAccess DB = new DBAccess();
            DB.EditTaskTag(tag);


        }


        public void Remove(TasksDB.TaskTags tag)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveTaskTag(tag.Id);

        }


        public List<TaskTags> GetAllTags()
        {

            DBAccess DB = new DBAccess();
            List<TaskTags> Tags = new List<TaskTags>();
            Tags = DB.GetAllTags();
            return Tags;
        }


        public TaskTags ReturnOneTag(byte? tagId)
        {
            DBAccess DB = new DBAccess();
            TaskTags tag = new TaskTags();
            tag = DB.ReturnOneTag(tagId);
            return tag;
        }


        public TaskTags ReturnOneTag(string name)
        {
            DBAccess DB = new DBAccess();
            TaskTags tag = new TaskTags();
            tag = DB.ReturnOneTag(name);
            return tag;
        }

    }

}
