﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksDB;

namespace TasksBL
{
    public class TasksWorkingDurationsService
    {

        public event EventHandler<TaskWorkingDurationsArgs> OnWorkingDurationAdded;
        public event EventHandler<TaskWorkingDurationsArgs> OnWorkingDurationEdited;
        public event EventHandler<TaskWorkingDurationsArgs> OnWorkingDurationRemove;

        public void Add(TasksDB.TaskWorkingDuration wd)
        {
            DBAccess DB = new DBAccess();
            DB.AddWorkingDuration(wd);

            if (OnWorkingDurationAdded != null)
            {
                TaskWorkingDurationsArgs args = new TaskWorkingDurationsArgs();
                args.WorkingDuration = wd;
                OnWorkingDurationAdded.Invoke(this, args);
                //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("[HH:mm:ss] ") + "OnCustomerAdded");
            }

        }


        public void Edit(TasksDB.TaskWorkingDuration duration)
        {
            DBAccess DB = new DBAccess();
            DB.EditWorkingDuration(duration);
            if (OnWorkingDurationEdited != null)
            {
                TaskWorkingDurationsArgs args = new TaskWorkingDurationsArgs();
                args.WorkingDuration = duration;
                OnWorkingDurationEdited.Invoke(this, args);
            }

        }


        public void Remove(TasksDB.TaskWorkingDuration duration)
        {
            DBAccess DB = new DBAccess();
            DB.RemoveWorkingDuration(duration.Id);
            if (OnWorkingDurationRemove != null)
            {
                TaskWorkingDurationsArgs args = new TaskWorkingDurationsArgs();
                args.WorkingDuration = duration;
                OnWorkingDurationRemove.Invoke(this, args);
            }
        }

        public List<TaskWorkingDuration> GetAllWorkingDurations(int TaskId)
        {
            DBAccess DB = new DBAccess();
            List<TaskWorkingDuration> Durations = new List<TaskWorkingDuration>();
            Durations = DB.GetAllWorkingDurations(TaskId);
            return Durations;
        }

        public TaskWorkingDuration ReturnOneWorkingDuration(int workingdurationId)
        {
            DBAccess DB = new DBAccess();
            TaskWorkingDuration duration = new TaskWorkingDuration();
            duration = DB.ReturnOneWorkingDuration(workingdurationId);
            return duration;
        }
    }
}
