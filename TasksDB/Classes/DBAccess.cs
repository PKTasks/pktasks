﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TasksDB
{
    public class DBAccess
    {

        #region HumanResources DB Functions
        public int AddHumanResource(HumanResource human)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.HumanResources.Add(human);
            DB.SaveChanges();
            return human.Id;
        }

        public bool EditHumanResource(HumanResource human)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource humanresourceTemp = DB.HumanResources.First(x => x.Id == human.Id);
            humanresourceTemp.Name = human.Name;
            humanresourceTemp.SectorId = human.SectorId;
            humanresourceTemp.Username = human.Username;
            humanresourceTemp.Password = human.Password;
            humanresourceTemp.Role = human.Role;
            humanresourceTemp.LastLogin = human.LastLogin;
            DB.SaveChanges();
            return true;
        }


        public bool RemoveHumanResource(int humanId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource humanresourceTemp = DB.HumanResources.First(x => x.Id == humanId);
            DB.HumanResources.Remove(humanresourceTemp);
            DB.SaveChanges();
            return true;
        }

        public List<HumanResource> GetAllHumanResources()
        {

            PKTasksEntities DB = new PKTasksEntities();
            List<HumanResource> HumanResources = new List<HumanResource>();
            HumanResources = DB.HumanResources.ToList();
            return HumanResources;
        }

        public HumanResource GetHumanResourceByUsername(String username)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource human = new HumanResource();
            human = DB.HumanResources.FirstOrDefault(x => x.Username == username);
            return human;
        }

        public HumanResource ReturnOneHumanResource(int hrId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource human = new HumanResource();
            human = DB.HumanResources.FirstOrDefault(x => x.Id == hrId);
            return human;
        }
        public HumanResource ReturnOneHumanResource(string name)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource human = new HumanResource();
            human = DB.HumanResources.FirstOrDefault(x => x.Name == name);
            return human;
        }


        public HumanResource ReturnOneHumanResource(string username,string password)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource human = new HumanResource();
            human = DB.HumanResources.FirstOrDefault(x => x.Username == username && x.Password==password);
            return human;
        }

        public byte GetHumanResourceRole(int userId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResource human = DB.HumanResources.FirstOrDefault(x => x.Id == userId);
            if (human != null)
            {
                return human.Role;
            }
            else
            {

                return 0;
            }
        }
        #endregion


        #region Sectors DB Functions
        public int AddSector(Sector sector)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.Sectors.Add(sector);
            DB.SaveChanges();
            return sector.Id;
        }


        public bool EditSector(Sector sector)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Sector sectorTemp = DB.Sectors.First(x => x.Id == sector.Id);
            sectorTemp.Description = sector.Description;
            DB.SaveChanges();
            return true;
        }


        public bool RemoveSector(int sectorId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Sector sectorTemp = DB.Sectors.First(x => x.Id == sectorId);
            DB.Sectors.Remove(sectorTemp);
            DB.SaveChanges();
            return true;
        }


        public List<Sector> GetAllSectors()
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Sector> Sectors = new List<Sector>();
            Sectors = DB.Sectors.ToList();
            return Sectors;
        }

        public Sector ReturnOneSector(int id)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Sector sector = DB.Sectors.FirstOrDefault(x => x.Id == id);
            return sector;
        }

        #endregion



        #region Projects DB Functions
        public int AddProject(Project project)
        {
            project.EstimatedDurationType = 1;
            PKTasksEntities DB = new PKTasksEntities();
            DB.Projects.Add(project);
            DB.SaveChanges();
            return project.Id;
        }


        public bool EditProject(Project project)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Project projectTemp = DB.Projects.First(x => x.Id == project.Id);
            projectTemp.Name = project.Name;
            projectTemp.Description = project.Description;
            projectTemp.EstimatedStartDate = project.EstimatedStartDate;
            projectTemp.EstimatedEndDate = project.EstimatedEndDate;
            projectTemp.StartDate = project.StartDate;
            projectTemp.EndDate = project.EndDate;
            projectTemp.Deadline = project.Deadline;
            projectTemp.StatusId = project.StatusId;
            projectTemp.EstimatedDuration = project.EstimatedDuration;
            projectTemp.EstimatedDurationType = 1;
            projectTemp.IsDeleted = project.IsDeleted;
            projectTemp.ModifiedDate = DateTime.Now;
            //projectTemp.StatusExplain = project.StatusExplain;
            DB.SaveChanges();
            return true;
        }


        public bool RemoveProject(int projectId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Project projectTemp = DB.Projects.First(x => x.Id == projectId);
            DB.Projects.Remove(projectTemp);
            DB.SaveChanges();
            return true;
        }


        public List<Project> GetAllProjects(Expression<Func<Project, bool>> IsDeletedfilter = null, Expression<Func<Project, bool>> filter1 = null)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Project> Projects = new List<Project>();

            IQueryable<Project> x = DB.Projects;
            if (IsDeletedfilter != null)
            {
                x = x.Where(IsDeletedfilter);
            }
            if(filter1 != null)
            {
                x = x.Where(filter1);
            }

            Projects = x.ToList();
            return Projects;
        }

        public Project ReturnOneProject(int projectId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Project project = new Project();
            project = DB.Projects.FirstOrDefault(x => x.Id == projectId);
            return project;
        }
        #endregion


        #region Status DB Functions
        public List<Status> GetProjectStatus()
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.Status.Where(x => x.CanStatusInProject == true).ToList();
            return Status;
        }

        public List<Status> GetSubProjectStatus()
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.Status.Where(x => x.CanStatusInSubProject == true).ToList();
            return Status;
        }

        public List<Status> GetTasksStatus()
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Status> Status = new List<TasksDB.Status>();
            Status = DB.Status.Where(x => x.CanStatusInTasks == true).ToList();
            return Status;
        }


        public Status ReturnOneStatus(int statusId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Status status = new Status();
            status = DB.Status.FirstOrDefault(x => x.Id == statusId);
            return status;
        }


        public Status ReturnOneStatus(string name)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Status status = new Status();
            status = DB.Status.FirstOrDefault(x => x.Name == name);
            return status;
        }
        #endregion



        #region Tasks DB Functions
        public int AddTask(Task task)
        {
            PKTasksEntities DB = new PKTasksEntities();

            List<int> parentTaskIds = task.ParentTasks.Select(x => x.Id).ToList();
            task.ParentTasks.Clear();

            foreach (int parentTaskId in parentTaskIds)
            {
                task.ParentTasks.Add(DB.Tasks.First(x => x.Id == parentTaskId));
            }

            DB.Tasks.Add(task);
            DB.SaveChanges();            
            return task.Id;
        }

        public bool EditTask(Task task)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Task taskTemp = DB.Tasks.First(x => x.Id == task.Id);
            taskTemp.Name = task.Name;
            taskTemp.Description = task.Description;
            taskTemp.EstimatedStartDate = task.EstimatedStartDate;
            taskTemp.EstimatedEndDate = task.EstimatedEndDate;
            taskTemp.StartDate = task.StartDate;
            taskTemp.EndDate = task.EndDate;
            taskTemp.Deadline = task.Deadline;
            taskTemp.StatusId = task.StatusId;
            //taskTemp.StatusExplain = task.StatusExplain;
            taskTemp.HumanResourcesId = task.HumanResourcesId;
            taskTemp.EstimatedDuration = task.EstimatedDuration;
            taskTemp.EstimatedDurationType = task.EstimatedDurationType;
            if (task.TaskTagsId !=0)
            {
                taskTemp.TaskTagsId = task.TaskTagsId;
            }
            taskTemp.IsDeleted = task.IsDeleted;
            taskTemp.ModifiedDate = DateTime.Now;
            taskTemp.ParentTasks.Clear();
            foreach (Task parentTask in task.ParentTasks)
            {
                taskTemp.ParentTasks.Add(DB.Tasks.First(x => x.Id == parentTask.Id));
            }

            DB.SaveChanges();
            return true;
        }


        public bool RemoveTask(int TaskId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Task taskTemp = DB.Tasks.First(x => x.Id == TaskId);
            DB.Tasks.Remove(taskTemp);
            DB.SaveChanges();
            return true;
        }

        public List<Task> GetSubProjectTasks(int SubProjectId, Expression<Func<Task, bool>> IsDeletedfilter = null, Expression<Func<Task, bool>> Filter = null)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Task> Tasks = new List<Task>();

            IQueryable<Task> x = DB.Tasks;
            if (IsDeletedfilter != null)
            {
                x = x.Where(IsDeletedfilter);
            }
            if (Filter != null)
            {
                x = x.Where(Filter);
            }


            Tasks = x.Where(k => k.SubProjectId == SubProjectId).ToList();
            return Tasks;
        }


        public List<Task> GetHumanResourceTasks(int humanId, Expression<Func<Task, bool>> filter = null)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Task> Tasks = new List<Task>();

            IQueryable<Task> x = DB.Tasks;
            if (filter != null)
            {
                x = x.Where(filter);
            }

            Tasks = x.Where(k => k.HumanResourcesId == humanId).ToList();
            return Tasks;
        }


        public Task ReturnOneTask(int id)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Task task = DB.Tasks.FirstOrDefault(x => x.Id == id);
            return task;
        }


        //public Task ReturnSubProjectsOneTask(int subprojectId, int Id)
        //{
        //    PKTasksEntities DB = new PKTasksEntities();
        //    Task task = DB.Tasks.FirstOrDefault(x => x.SubProjectId == subprojectId && x.Id==Id);
        //    return task;
        //}
        #endregion



        #region SubProjects DB Functions

        public int AddSubProject(SubProject subproject)
        {
            subproject.EstimatedDurationType = 1;
            PKTasksEntities DB = new PKTasksEntities();
            DB.SubProjects.Add(subproject);
            DB.SaveChanges();
            return subproject.Id;
        }

        public bool EditSubProject(SubProject subproject)
        {
            PKTasksEntities DB = new PKTasksEntities();
            SubProject subprojectTemp = DB.SubProjects.First(x => x.Id == subproject.Id);
            subprojectTemp.Name = subproject.Name;
            subprojectTemp.Description = subproject.Description;
            subprojectTemp.EstimatedStartDate = subproject.EstimatedStartDate;
            subprojectTemp.EstimatedEndDate = subproject.EstimatedEndDate;
            subprojectTemp.StartDate = subproject.StartDate;
            subprojectTemp.EndDate = subproject.EndDate;
            subprojectTemp.Deadline = subproject.Deadline;
            subprojectTemp.StatusId = subproject.StatusId;
            subprojectTemp.EstimatedDuration = subproject.EstimatedDuration;
            subprojectTemp.EstimatedDurationType = 1;
            subprojectTemp.IsDeleted = subproject.IsDeleted;
            subprojectTemp.ModifiedDate = DateTime.Now;
            //subprojectTemp.StatusExplain = subproject.StatusExplain;
            DB.SaveChanges();
            return true;
        }


        public bool RemoveSubProject(int subprojectId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            SubProject subprojectTemp = DB.SubProjects.First(x => x.Id == subprojectId);
            DB.SubProjects.Remove(subprojectTemp);
            DB.SaveChanges();
            return true;
        }


        public List<SubProject> GetAllSubProjects(Expression<Func<SubProject, bool>> filter = null)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<SubProject> SubProjects = new List<SubProject>();

            IQueryable<SubProject> x = DB.SubProjects;
            if (filter != null)
            {
                x = x.Where(filter);
            }


            SubProjects = x.ToList();
            return SubProjects;
        }


        public List<SubProject> GetProjectSubProjects(int ProjectId, Expression<Func<SubProject, bool>> filter = null, Expression<Func<SubProject, bool>> filter1 = null)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<SubProject> SubProjects = new List<SubProject>();

            IQueryable<SubProject> x = DB.SubProjects;
            if (filter != null)
            {
                x = x.Where(filter);
            }
            if (filter1 != null)
            {
                x = x.Where(filter1);
            }

            SubProjects = x.Where(k => k.ProjectId == ProjectId).ToList();
            return SubProjects;
        }

        public SubProject ReturnOneSubProject(int subprojectId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            SubProject subproject = new SubProject();
            subproject = DB.SubProjects.FirstOrDefault(x => x.Id == subprojectId);
            return subproject;
        }


        //public List<> GetSubprojectSectors(int subprojectId)
        //{
        //    PKTasksEntities DB = new PKTasksEntities();
        //    SubProject subproject = new SubProject();
        //    subproject.Sectors
        //}


        //public SubProject ReturnProjectsOneSubProject(int projectId)
        //{
        //    PKTasksEntities DB = new PKTasksEntities();
        //    SubProject subproject = new SubProject();
        //    subproject = DB.SubProjects.FirstOrDefault(x => x.ProjectId == projectId);
        //    return subproject;
        //}

        #endregion


        #region TasksWorkingDurations Functions

        public int AddWorkingDuration(TaskWorkingDuration workingduration)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.TaskWorkingDuration.Add(workingduration);
            DB.SaveChanges();
            return workingduration.Id;
        }

        public bool EditWorkingDuration(TaskWorkingDuration workingduration)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskWorkingDuration workingdurationTemp = DB.TaskWorkingDuration.First(x => x.Id == workingduration.Id);
            workingdurationTemp.TaskId = workingduration.TaskId;
            workingdurationTemp.Duration = workingduration.Duration;
            workingdurationTemp.WorkingDate = workingduration.WorkingDate;
            DB.SaveChanges();
            return true;
        }

        public bool RemoveWorkingDuration(int workingdurationId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskWorkingDuration workingdurationTemp = DB.TaskWorkingDuration.First(x => x.Id == workingdurationId);
            DB.TaskWorkingDuration.Remove(workingdurationTemp);
            DB.SaveChanges();
            return true;
        }


        public List<TaskWorkingDuration> GetAllWorkingDurations(int TaskId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<TaskWorkingDuration> Durations = new List<TaskWorkingDuration>();
            Durations = DB.TaskWorkingDuration.Where(x => x.TaskId == TaskId).ToList();
            return Durations;
        }

        public TaskWorkingDuration ReturnOneWorkingDuration(int workingdurationId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskWorkingDuration duration = new TaskWorkingDuration();
            duration = DB.TaskWorkingDuration.FirstOrDefault(x => x.Id == workingdurationId);
            return duration;
        }

        #endregion


        #region HumanResourceCost Functions
        public int AddHumanResourceCost(HumanResourceCost humanresourcecost)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.HumanResourceCosts.Add(humanresourcecost);
            DB.SaveChanges();
            return humanresourcecost.Id;
        }

        public bool EditHumanResourceCost(HumanResourceCost humanresourcecost)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResourceCost humanresourcecostTemp = DB.HumanResourceCosts.First(x => x.Id == humanresourcecost.Id);
            humanresourcecostTemp.HumanResourceId = humanresourcecost.HumanResourceId;
            humanresourcecostTemp.Cost = humanresourcecost.Cost;
            humanresourcecostTemp.FromDate = humanresourcecost.FromDate;
            DB.SaveChanges();
            return true;
        }

        public bool RemoveHumanResourceCost(int humanresourcecostId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResourceCost humanresourcecostTemp = DB.HumanResourceCosts.First(x => x.Id == humanresourcecostId);
            DB.HumanResourceCosts.Remove(humanresourcecostTemp);
            DB.SaveChanges();
            return true;
        }


        public List<HumanResourceCost> GetAllHumanResourceCosts(int humanresourceId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<HumanResourceCost> Costs = new List<HumanResourceCost>();
            Costs = DB.HumanResourceCosts.Where(x => x.HumanResourceId == humanresourceId).ToList();
            return Costs;
        }


        public HumanResourceCost ReturnOneHumanResourceCostbyId(int humanresourceId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResourceCost cost = new HumanResourceCost();
            cost = DB.HumanResourceCosts.FirstOrDefault(x => x.HumanResourceId == humanresourceId);
            return cost;
        }

        public HumanResourceCost ReturnOneHumanResourceCost(DateTime fromdate, int humanresourceId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            HumanResourceCost cost = new HumanResourceCost();
            cost = DB.HumanResourceCosts.OrderByDescending(x => x.FromDate).FirstOrDefault(x => x.HumanResourceId == humanresourceId && x.FromDate <= fromdate);
            return cost;
        }
        #endregion


        #region Notes Functions
        public int AddNote(Note note)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.Notes.Add(note);
            DB.SaveChanges();
            return note.Id;
        }

        public bool EditNote(Note note)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Note noteTemp = DB.Notes.First(x => x.Id == note.Id);
            noteTemp.HumanResourceId = noteTemp.HumanResourceId;
            noteTemp.EntryId = note.EntryId;
            noteTemp.EntryTypeId = note.EntryTypeId;
            noteTemp.PostDate = note.PostDate;
            noteTemp.Message = note.Message;
            noteTemp.ModifiedDate = DateTime.Now;
            DB.SaveChanges();
            return true;
        }

        public bool RemoveNote(int NoteId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Note noteTemp = DB.Notes.First(x => x.Id == NoteId);
            DB.Notes.Remove(noteTemp);
            DB.SaveChanges();
            return true;
        }


        public List<Note> GetAllNotesForHumanResource(int taskId, byte Type, HumanResource human)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Note> notes = new List<Note>();

            notes = DB.Notes.Where(x => x.EntryId == taskId && x.EntryTypeId ==Type && x.HumanResourceId == human.Id).ToList();
            return notes;
        }

        public List<Note> GetAllNotesForAdmin(int taskId, byte Type)
        {
            PKTasksEntities DB = new PKTasksEntities();
            List<Note> notes = new List<Note>();
            notes = DB.Notes.Where(x => x.EntryId == taskId && x.EntryTypeId == Type).ToList();
            return notes;
        }


        public Note ReturnOneNotebyId(int noteId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Note note = new Note();
            note = DB.Notes.FirstOrDefault(x => x.Id == noteId);
            return note;
        }

        public Note ReturnOneNote(DateTime postdate, int EntryId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            Note note = new Note();
            note = DB.Notes.OrderByDescending(x => x.PostDate).FirstOrDefault(x => x.EntryId == EntryId && x.PostDate <= postdate);
            return note;
        }
        #endregion


        #region Tags Functions
        public int AddTaskTag(TaskTags tag)
        {
            PKTasksEntities DB = new PKTasksEntities();
            DB.TaskTags.Add(tag);
            DB.SaveChanges();
            return tag.Id;
        }

        public bool EditTaskTag(TaskTags tag)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskTags tagTemp = DB.TaskTags.First(x => x.Id == tag.Id);
            tagTemp.Name = tag.Name;
            DB.SaveChanges();
            return true;
        }


        public bool RemoveTaskTag(int tagId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskTags TaskTagsTemp = DB.TaskTags.First(x => x.Id == tagId);
            DB.TaskTags.Remove(TaskTagsTemp);
            DB.SaveChanges();
            return true;
        }

        public List<TaskTags> GetAllTags()
        {

            PKTasksEntities DB = new PKTasksEntities();
            List<TaskTags> Tags = new List<TaskTags>();
            Tags = DB.TaskTags.ToList();
            return Tags;
        }


        public TaskTags ReturnOneTag(byte? tagId)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskTags tag = new TaskTags();
            tag = DB.TaskTags.FirstOrDefault(x => x.Id == tagId);
            return tag;
        }
        public TaskTags ReturnOneTag(string name)
        {
            PKTasksEntities DB = new PKTasksEntities();
            TaskTags tag = new TaskTags();
            tag = DB.TaskTags.FirstOrDefault(x => x.Name == name);
            return tag;
        }
        #endregion
    }
}
