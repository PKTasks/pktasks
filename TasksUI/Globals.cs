﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksBL;

namespace TasksUI
{
    public static class Globals
    {
        public static int CurrentHumanId;
        public static byte Role;

        public static void SetCurrentHumanId(int Id)
        {
            CurrentHumanId = Id;
        }
        public static void SetCurrentHumanRole(byte role)
        {
            Role = role;
        }


    }
}
