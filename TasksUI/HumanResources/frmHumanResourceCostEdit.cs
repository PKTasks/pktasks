﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmHumanResourceCostEdit : Form
    {
        public event EventHandler<HumanResourceCostArgs> OnHumanResourceCostAdd;
        public event EventHandler<HumanResourceCostArgs> OnHUmanResourceCostEdit;

        Enumerations.PKTasksState ProjectState;

        HumanResourceCost Costs;
        int HumanResourceId;

        public frmHumanResourceCostEdit(int humanresourceId, HumanResourceCost costs, Enumerations.PKTasksState projectstate)
        {
            InitializeComponent();
            ProjectState = projectstate;
            HumanResourceId = humanresourceId;

            if (projectstate == Enumerations.PKTasksState.Edited)
            {
                Costs = costs;
                nudCost.Value = costs.Cost;
                dtpDate.Value = costs.FromDate;
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (nudCost.Value == 0)
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε διάρκεια");
                return;
            }

            HumanResourceCostsService srvhrc = new HumanResourceCostsService();
            HumanResourceCost humanresourcecost = new HumanResourceCost();

            humanresourcecost.Cost = (int)nudCost.Value;
            humanresourcecost.FromDate = dtpDate.Value;
            humanresourcecost.HumanResourceId = HumanResourceId;
            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    srvhrc.Add(humanresourcecost);

                    if (OnHumanResourceCostAdd != null)
                    {
                        HumanResourceCostArgs args = new HumanResourceCostArgs();
                        args.humanresourcecost = humanresourcecost;
                        OnHumanResourceCostAdd.Invoke(this, args);
                    }
                    break;
                case Enumerations.PKTasksState.Edited:
                    humanresourcecost.Id = Costs.Id;
                    humanresourcecost.HumanResourceId = Costs.HumanResourceId;
                    srvhrc.Edit(humanresourcecost);

                    if (OnHUmanResourceCostEdit != null)
                    {
                        HumanResourceCostArgs args = new HumanResourceCostArgs();
                        args.humanresourcecost = humanresourcecost;
                        OnHUmanResourceCostEdit.Invoke(this, args);
                    }
                    break;
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmHumanResourceCostEdit_Load(object sender, EventArgs e)
        {
            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Εισαγωγή";
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία";
                    break;
            }
        }
    }
}
