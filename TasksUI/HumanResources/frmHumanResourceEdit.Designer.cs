﻿namespace TasksUI
{
    partial class NewHumanResourceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSector = new System.Windows.Forms.ComboBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.cmbHumanResourceRole = new System.Windows.Forms.ComboBox();
            this.lvCosts = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnInsertCost = new System.Windows.Forms.Button();
            this.btnEditCost = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbSector
            // 
            this.cmbSector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSector.FormattingEnabled = true;
            this.cmbSector.Location = new System.Drawing.Point(198, 33);
            this.cmbSector.Name = "cmbSector";
            this.cmbSector.Size = new System.Drawing.Size(194, 21);
            this.cmbSector.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(198, 7);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(194, 20);
            this.txtName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Επωνυμία";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ειδικότητα";
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(43, 217);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(134, 37);
            this.btnAccept.TabIndex = 4;
            this.btnAccept.Text = "Εισαγωγή";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(218, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 37);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Άκυρο";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Όνομα Χρήστη (Username)";
            // 
            // txtUsername
            // 
            this.txtUsername.BackColor = System.Drawing.SystemColors.Window;
            this.txtUsername.Location = new System.Drawing.Point(198, 65);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(194, 20);
            this.txtUsername.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Κωδικός Πρόσβασης (Password)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ιδιότητα";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(198, 98);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(194, 20);
            this.txtPassword.TabIndex = 10;
            // 
            // cmbHumanResourceRole
            // 
            this.cmbHumanResourceRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHumanResourceRole.FormattingEnabled = true;
            this.cmbHumanResourceRole.Location = new System.Drawing.Point(198, 132);
            this.cmbHumanResourceRole.Name = "cmbHumanResourceRole";
            this.cmbHumanResourceRole.Size = new System.Drawing.Size(121, 21);
            this.cmbHumanResourceRole.TabIndex = 11;
            // 
            // lvCosts
            // 
            this.lvCosts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader1,
            this.columnHeader2});
            this.lvCosts.FullRowSelect = true;
            this.lvCosts.GridLines = true;
            this.lvCosts.Location = new System.Drawing.Point(504, 41);
            this.lvCosts.MultiSelect = false;
            this.lvCosts.Name = "lvCosts";
            this.lvCosts.Size = new System.Drawing.Size(235, 196);
            this.lvCosts.TabIndex = 12;
            this.lvCosts.UseCompatibleStateImageBehavior = false;
            this.lvCosts.View = System.Windows.Forms.View.Details;
            this.lvCosts.DoubleClick += new System.EventHandler(this.btnEditCost_Click);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ΑΑ";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Ημερομηνία";
            this.columnHeader1.Width = 99;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Κόστος";
            // 
            // btnInsertCost
            // 
            this.btnInsertCost.Location = new System.Drawing.Point(524, 243);
            this.btnInsertCost.Name = "btnInsertCost";
            this.btnInsertCost.Size = new System.Drawing.Size(88, 30);
            this.btnInsertCost.TabIndex = 13;
            this.btnInsertCost.Text = "Εισαγωγή";
            this.btnInsertCost.UseVisualStyleBackColor = true;
            this.btnInsertCost.Click += new System.EventHandler(this.btnInsertCost_Click);
            // 
            // btnEditCost
            // 
            this.btnEditCost.Location = new System.Drawing.Point(618, 243);
            this.btnEditCost.Name = "btnEditCost";
            this.btnEditCost.Size = new System.Drawing.Size(88, 30);
            this.btnEditCost.TabIndex = 14;
            this.btnEditCost.Text = "Επεξεργασία";
            this.btnEditCost.UseVisualStyleBackColor = true;
            this.btnEditCost.Click += new System.EventHandler(this.btnEditCost_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(545, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Κόστος Εργασίας ανά Ώρα";
            // 
            // NewHumanResourceForm
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(901, 422);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnEditCost);
            this.Controls.Add(this.btnInsertCost);
            this.Controls.Add(this.lvCosts);
            this.Controls.Add(this.cmbHumanResourceRole);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.cmbSector);
            this.Name = "NewHumanResourceForm";
            this.Text = "NewHumanResourceForm";
            this.Load += new System.EventHandler(this.NewHumanResourceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSector;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.ComboBox cmbHumanResourceRole;
        private System.Windows.Forms.ListView lvCosts;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnInsertCost;
        private System.Windows.Forms.Button btnEditCost;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label6;
    }
}