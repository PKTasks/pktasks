﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class NewHumanResourceForm : Form
    {
        public event EventHandler<HumanResourcesEventArgs> OnHumanResourceInserted;
        public event EventHandler<HumanResourcesEventArgs> OnHumanResourceUpdated;

        Enumerations.PKTasksState Humanresourcestate;
        TasksDB.HumanResource humanresourceTemp;

        public NewHumanResourceForm(TasksDB.HumanResource humanresource, Enumerations.PKTasksState humanresourcestate)
        {
            InitializeComponent();
            ComboBox_AutoComplete();
            

            Humanresourcestate = humanresourcestate;

            if (humanresource != null)
            {
                humanresourceTemp = humanresource;
                txtName.Text = humanresource.Name;
                SetComboSelectedItemById(humanresource.Sectors.Id);
                SetHumanResourceRoleCombo(humanresource.Role);
                txtUsername.Text = humanresource.Username;
                txtPassword.Text = humanresource.Password;
                ListViewCostAutoComplete();
            }
        }

        private void SetComboSelectedItemById(int id)
        {
            for (int i =0; i< cmbSector.Items.Count;i++)
            {
                if (((TasksDB.Sector)cmbSector.Items[i]).Id == id)
                {
                    cmbSector.SelectedIndex = i;
                    break;
                }
            }
        }


        private void SetHumanResourceRoleCombo(byte role)
        {
            for (int i = 0; i < cmbHumanResourceRole.Items.Count; i++)
            {
                if ((Enumerations.PKTasksUserRole)cmbHumanResourceRole.Items[i] == (Enumerations.PKTasksUserRole)role)
                {
                    cmbHumanResourceRole.SelectedIndex = i;
                    break;
                }
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewHumanResourceForm_Load(object sender, EventArgs e)
        {
           
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "" || cmbSector.Text == "")
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε όλα τα πεδία");
                return;
            }

            TasksDB.HumanResource humanresource = new TasksDB.HumanResource();
            TasksBL.HumanResourceService hr = new TasksBL.HumanResourceService();

            humanresource.Name = txtName.Text;
            humanresource.SectorId = ((TasksDB.Sector)cmbSector.SelectedValue).Id;
            humanresource.Username = txtUsername.Text;
            humanresource.Password = txtPassword.Text;
            
            humanresource.Role = (byte)((Enumerations.PKTasksUserRole)cmbHumanResourceRole.SelectedValue);

            switch (Humanresourcestate)
            {
                case Enumerations.PKTasksState.Added:
                    hr.Add(humanresource);
                    if(OnHumanResourceInserted != null)
                    {
                        HumanResourcesEventArgs args = new HumanResourcesEventArgs();
                        args.HumanResource = humanresource;
                        OnHumanResourceInserted.Invoke(this, args);
                    }
                    break;

                case Enumerations.PKTasksState.Edited:
                    humanresource.Id = humanresourceTemp.Id;
                    hr.Edit(humanresource);
                    if (OnHumanResourceUpdated != null)
                    {
                        HumanResourcesEventArgs args = new HumanResourcesEventArgs();
                        args.HumanResource = humanresource;
                        OnHumanResourceUpdated.Invoke(this, args);
                    }
                    break;
            }
            this.Close();

            //humanresource.AddHumanResource()
        }


        private void ComboBox_AutoComplete()
        {
            SectorService srvsector = new SectorService();
            cmbSector.DataSource = null;

            cmbSector.DisplayMember = "Description";
            cmbSector.DataSource = srvsector.GetAllSectors();
            cmbSector.SelectedIndex = -1;

            cmbHumanResourceRole.DataSource = null;


            cmbHumanResourceRole.DataSource = Enum.GetValues(typeof(Enumerations.PKTasksUserRole));
            cmbHumanResourceRole.SelectedIndex = 1;

            //cmbDescription.Items.Clear();
        }

        private void btnInsertCost_Click(object sender, EventArgs e)
        {
            frmHumanResourceCostEdit humanresourcecosts = new frmHumanResourceCostEdit(humanresourceTemp.Id, null, Enumerations.PKTasksState.Added);
            humanresourcecosts.OnHumanResourceCostAdd += Humanresourcecosts_OnHumanResourceCostAdd;
            humanresourcecosts.ShowDialog();
        }

        private void Humanresourcecosts_OnHumanResourceCostAdd(object sender, HumanResourceCostArgs e)
        {
            NewHumanResourceCostAdd(e.humanresourcecost);
        }

        private void btnEditCost_Click(object sender, EventArgs e)
        {
            int j = 0;
            int.TryParse(lvCosts.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            HumanResourceCost humanresourcecost = DB.ReturnOneHumanResourceCostbyId(j);

            frmHumanResourceCostEdit humanresourcecosts = new frmHumanResourceCostEdit(humanresourceTemp.Id, humanresourcecost, Enumerations.PKTasksState.Edited);
            humanresourcecosts.OnHUmanResourceCostEdit += Humanresourcecosts_OnHUmanResourceCostEdit;
            humanresourcecosts.ShowDialog();
        }

        private void Humanresourcecosts_OnHUmanResourceCostEdit(object sender, HumanResourceCostArgs e)
        {
            UpdateHumanResourceCost(e.humanresourcecost);
        }


        private void NewHumanResourceCostAdd(TasksDB.HumanResourceCost cost)
        {
            ListViewItem lvi = new ListViewItem(cost.Id.ToString());

            lvCosts.Items.Add(lvi);
            lvi.SubItems.Add(cost.FromDate.ToString("dd/MM/yyyy"));
            lvi.SubItems.Add(cost.Cost.ToString());

        }

        private void UpdateHumanResourceCost(TasksDB.HumanResourceCost cost)
        {
            ListViewItem item = lvCosts.SelectedItems[0];
            item.SubItems[0].Text = cost.Id.ToString();
            item.SubItems[1].Text = cost.FromDate.ToString("dd/MM/yyyy");
            item.SubItems[2].Text = cost.Cost.ToString();


        }


        private void ListViewCostAutoComplete()
        {
            List<TasksDB.HumanResourceCost> Costs;
            HumanResourceCostsService srvcost = new HumanResourceCostsService();
            Costs = srvcost.GetAllHumanResourceCosts(humanresourceTemp.Id);

            int j;
            for (j = 0; j < Costs.Count; j++)
            {
                ListViewItem lvi2 = new ListViewItem(Costs[j].Id.ToString());
                lvCosts.Items.Add(lvi2);
                lvi2.SubItems.Add(Costs[j].FromDate.Date.ToString("dd/MM/yyyy"));
                lvi2.SubItems.Add(Costs[j].Cost.ToString());
            }

            lvCosts.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }
    }
}
