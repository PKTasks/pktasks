﻿namespace TasksUI
{
    partial class HumanResourcesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewHumanResorce = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnNewEditResorce = new System.Windows.Forms.Button();
            this.btnNewRemoveResorce = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNewHumanResorce
            // 
            this.btnNewHumanResorce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewHumanResorce.Location = new System.Drawing.Point(736, 12);
            this.btnNewHumanResorce.Name = "btnNewHumanResorce";
            this.btnNewHumanResorce.Size = new System.Drawing.Size(185, 59);
            this.btnNewHumanResorce.TabIndex = 1;
            this.btnNewHumanResorce.Text = "Εισαγωγή";
            this.btnNewHumanResorce.UseVisualStyleBackColor = true;
            this.btnNewHumanResorce.Click += new System.EventHandler(this.btnNewHumanResorce_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 14);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(684, 397);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.DoubleClick += new System.EventHandler(this.btnNewEditResorce_Click);
            this.listView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listView1_KeyDown);
            this.listView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.listView1_KeyPress);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ΑΑ";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Επωνυμία";
            this.columnHeader1.Width = 276;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ειδικότητα";
            this.columnHeader2.Width = 292;
            // 
            // btnNewEditResorce
            // 
            this.btnNewEditResorce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewEditResorce.Enabled = false;
            this.btnNewEditResorce.Location = new System.Drawing.Point(736, 77);
            this.btnNewEditResorce.Name = "btnNewEditResorce";
            this.btnNewEditResorce.Size = new System.Drawing.Size(185, 59);
            this.btnNewEditResorce.TabIndex = 3;
            this.btnNewEditResorce.Text = "Επεξεργασία";
            this.btnNewEditResorce.UseVisualStyleBackColor = true;
            this.btnNewEditResorce.Click += new System.EventHandler(this.btnNewEditResorce_Click);
            // 
            // btnNewRemoveResorce
            // 
            this.btnNewRemoveResorce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewRemoveResorce.Enabled = false;
            this.btnNewRemoveResorce.Location = new System.Drawing.Point(736, 142);
            this.btnNewRemoveResorce.Name = "btnNewRemoveResorce";
            this.btnNewRemoveResorce.Size = new System.Drawing.Size(185, 59);
            this.btnNewRemoveResorce.TabIndex = 4;
            this.btnNewRemoveResorce.Text = "Διαγραφή";
            this.btnNewRemoveResorce.UseVisualStyleBackColor = true;
            this.btnNewRemoveResorce.Click += new System.EventHandler(this.btnNewRemoveResource_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(736, 325);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 59);
            this.button1.TabIndex = 5;
            this.button1.Text = "Έξοδος";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(736, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 59);
            this.button2.TabIndex = 6;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HumanResourcesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 423);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnNewRemoveResorce);
            this.Controls.Add(this.btnNewEditResorce);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.btnNewHumanResorce);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.Name = "HumanResourcesForm";
            this.Text = "Διαχείρηση Ανθρώπινου Δυναμικού";
            this.Load += new System.EventHandler(this.HumanResourcesForm_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNewHumanResorce;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnNewEditResorce;
        private System.Windows.Forms.Button btnNewRemoveResorce;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button button2;
    }
}