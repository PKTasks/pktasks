﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class HumanResourcesForm : Form
    {

        public HumanResourcesForm()
        {
            InitializeComponent();

        }

        private void HumanResourcesForm_Load(object sender, EventArgs e)
        {
            RefreshElementsAll();
        }

        private void btnNewHumanResorce_Click(object sender, EventArgs e)
        {
            NewHumanResourceForm humanresourceformNew = new NewHumanResourceForm(null, Enumerations.PKTasksState.Added);
            //humanresourceformNew.MdiParent = this.MdiParent;

            humanresourceformNew.OnHumanResourceInserted += HumanresourceformNew_OnHumanResourceInserted;
            humanresourceformNew.ShowDialog();
        }

        private void HumanresourceformNew_OnHumanResourceInserted(object sender, HumanResourcesEventArgs e)
        {
            NewHumanResourceAdd(e.HumanResource);
        }

        private void btnNewEditResorce_Click(object sender, EventArgs e)
        {

            int j = 0;
            int.TryParse(listView1.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TasksDB.HumanResource humanresource = DB.ReturnOneHumanResource(j);

            NewHumanResourceForm humanresourceformEdit = new NewHumanResourceForm(humanresource, Enumerations.PKTasksState.Edited);
            //humanresourceformEdit.MdiParent = this.MdiParent;

            humanresourceformEdit.OnHumanResourceUpdated += HumanresourceformEdit_OnHumanResourceUpdated;

            humanresourceformEdit.ShowDialog();
        }

        private void HumanresourceformEdit_OnHumanResourceUpdated(object sender, HumanResourcesEventArgs e)
        {
            UpdateEditHumanResource(e.HumanResource);
        }

        private void btnNewRemoveResource_Click(object sender, EventArgs e)
        {
            string tmp = "Θέλετε να διαγραψετε την επιλεγμένη εγγραφή;\r\n\tΕπωνυμία : " + listView1.SelectedItems[0].SubItems[1].Text + "\r\n\tΕιδικότητα : " + listView1.SelectedItems[0].SubItems[2].Text;

            if (MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }

            int j = 0;
            int.TryParse(listView1.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TasksDB.HumanResource humanresource = DB.ReturnOneHumanResource(j);
            TasksBL.HumanResourceService hr = new TasksBL.HumanResourceService();
            hr.Remove(humanresource);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool test = (this.listView1.SelectedItems.Count > 0);
            this.btnNewEditResorce.Enabled = test;
            this.btnNewRemoveResorce.Enabled = test;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void RefreshElementsAll()
        {
            listView1.Items.Clear();

            List<TasksDB.HumanResource> HumanResources = new List<TasksDB.HumanResource>();
            HumanResourceService srvhr = new HumanResourceService();
            HumanResources = srvhr.GetAllHumanResources();

            int i;
            for (i = 0; i < HumanResources.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(HumanResources[i].Id.ToString());

                listView1.Items.Add(lvi);

                lvi.SubItems.Add(HumanResources[i].Name);
                //lvi.SubItems.Add(HumanResources[HumanResources[i].Id - 1].Sectors.Description);
                lvi.SubItems.Add(HumanResources[i].Sectors.Description);
            }
        }


        private void NewHumanResourceAdd(TasksDB.HumanResource humanresource)
        {
            SectorService srvsector = new SectorService();
            TasksDB.Sector sector = srvsector.ReturnOneSector(humanresource.SectorId);
            ListViewItem lvi = new ListViewItem(humanresource.Id.ToString());

            listView1.Items.Add(lvi);
            lvi.SubItems.Add(humanresource.Name);
            lvi.SubItems.Add(sector.Description);

        }
        private void UpdateEditHumanResource(TasksDB.HumanResource humanresource)
        {
            SectorService srvsector = new SectorService();
            TasksDB.Sector sector = srvsector.ReturnOneSector(humanresource.SectorId);
            ListViewItem item = listView1.SelectedItems[0];
            item.SubItems[0].Text = humanresource.Id.ToString();
            item.SubItems[1].Text = humanresource.Name;
            item.SubItems[2].Text = sector.Description;

        }

        private void listView1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Delete)
            {
                btnNewRemoveResource_Click(null, null);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            RefreshElementsAll();
        }
    }
}
