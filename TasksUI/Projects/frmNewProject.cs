﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class NewProject : Form
    {
        Enumerations.PKTasksState ProjectState;
        TasksDB.Project ProjectTemp;

        public event EventHandler<ProjectsEventArgs> OnNewProjectAdded;
        public event EventHandler<ProjectsEventArgs> OnNewProjectEdited;

        public NewProject(TasksDB.Project project, Enumerations.PKTasksState projectstate)
        {
            InitializeComponent();
            ComboBox_AutoComplete();

            ProjectState = projectstate;
            if(projectstate == Enumerations.PKTasksState.Edited)
            {
                ucNotes1.Initialize(project.Id, Enumerations.PKTasksEntryType.Project);
            }
            else
            {
                ucNotes1.Visible = false;
            }
            

            if (project != null)
            {
                ProjectTemp = project;
                txtName.Text = project.Name;
                txtDescription.Text = project.Description;

                if (project.EstimatedStartDate.HasValue)
                {
                    dtpEstimatedStartDate.Checked = true;
                    dtpEstimatedStartDate.Value = project.EstimatedStartDate.Value;
                }
                if (project.EstimatedEndDate.HasValue)
                {
                    dtpEstimatedEndDate.Checked = true;
                    dtpEstimatedEndDate.Value = project.EstimatedEndDate.Value;
                }
                if (project.StartDate.HasValue)
                {
                    dtpStartDate.Checked = true;
                    dtpStartDate.Value = project.StartDate.Value;
                }
                if (project.EndDate.HasValue)
                {
                    dtpEndDate.Checked = true;
                    dtpEndDate.Value = project.EndDate.Value;
                }
                if (project.Deadline.HasValue)
                {
                    dtpDeadline.Checked = true;
                    dtpDeadline.Value = project.Deadline.Value;
                }

                SetComboSelectedItemById(project.StatusId);

                //txtStatusExplain.Text = project.StatusExplain;

                //this.subProject1.ProjectId = ProjectTemp.Id;

            }
        }



        private void SetComboSelectedItemById(byte id)
        {
            for (int i = 0; i < cmbStatus.Items.Count; i++)
            {
                if (((TasksDB.Status)cmbStatus.Items[i]).Id == id)
                {
                    cmbStatus.SelectedIndex = i;
                    break;
                }
            }
        }



        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void ComboBox_AutoComplete()
        {
            StatusService srvst = new StatusService();
            cmbStatus.DataSource = null;

            cmbStatus.DisplayMember = "Name";
            cmbStatus.DataSource = srvst.GetProjectStatus();
            if (cmbStatus.Items.Count > 0)
            {
                cmbStatus.SelectedIndex = 0;
            }

            //cmbDescription.Items.Clear();
        }

        private void NewProject_Load(object sender, EventArgs e)
        {
            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Νέο Έργο";
                    //btnInsertGeneral.Text = "Εισαγωγή";
                    //tabControl1.TabPages.Remove(tpSubProjects);
                    
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία Έργου";
                    //btnInsertGeneral.Text = "Επεξεργασία";
                    break;
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInsertGeneral_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε ένα όνομα");
                return;
            }

            TasksDB.Project project = new TasksDB.Project();
            TasksBL.ProjectService pr = new TasksBL.ProjectService();

            project.Name = txtName.Text;

            if (!string.IsNullOrEmpty(txtDescription.Text))
            {
                project.Description = txtDescription.Text;
            }
            else
            {
                project.Description = null;
            }

            if (dtpEstimatedStartDate.Checked)
            {
                project.EstimatedStartDate = dtpEstimatedStartDate.Value.Date;
            }
            else
            {
                project.EstimatedStartDate = null;
            }


            if (dtpEstimatedEndDate.Checked)
            {
                project.EstimatedEndDate = dtpEstimatedEndDate.Value.Date;
            }
            else
            {
                project.EstimatedEndDate = null;
            }

            if (dtpStartDate.Checked)
            {
                project.StartDate = dtpStartDate.Value.Date;
            }
            else
            {
                project.StartDate = null;
            }

            if (dtpEndDate.Checked)
            {
                project.EndDate = dtpEndDate.Value.Date;
            }
            else
            {
                project.EndDate = null;
            }

            if (dtpDeadline.Checked)
            {
                project.Deadline = dtpDeadline.Value.Date;
            }
            else
            {
                project.Deadline = null;
            }

            project.StatusId = ((TasksDB.Status)cmbStatus.SelectedValue).Id;

            //if (!string.IsNullOrEmpty(txtStatusExplain.Text))
            //{
            //    project.StatusExplain = txtStatusExplain.Text;
            //}
            //else
            //{
            //    project.StatusExplain = null;
            //}

            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    pr.Add(project);
                    if (OnNewProjectAdded != null)
                    {
                        TasksDB.DBAccess DB = new DBAccess();
                        TasksDB.Project t = DB.ReturnOneProject(project.Id);

                        ProjectsEventArgs args = new ProjectsEventArgs();
                        args.Project = t;
                        OnNewProjectAdded.Invoke(this, args);
                    }
                    break;
                case Enumerations.PKTasksState.Edited:
                    project.Id = ProjectTemp.Id;
                    pr.Edit(project);
                    if (OnNewProjectEdited != null)
                    {
                        TasksDB.DBAccess DB = new DBAccess();
                        TasksDB.Project t = DB.ReturnOneProject(project.Id);

                        ProjectsEventArgs args = new ProjectsEventArgs();
                        args.Project = t;
                        OnNewProjectEdited.Invoke(this, args);
                    }
                    break;
            }

            this.Close();
        }



        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }





        private void tpSubProjects_Click(object sender, EventArgs e)
        {
            
        }

        private void subProject1_Load(object sender, EventArgs e)
        {
            
        }

        private void subProject1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
