﻿namespace TasksUI
{
    partial class frmProjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnNewRemoveProject = new System.Windows.Forms.Button();
            this.btnNewEditProject = new System.Windows.Forms.Button();
            this.lvProjects = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnNewProject = new System.Windows.Forms.Button();
            this.lvSubProjects = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button3 = new System.Windows.Forms.Button();
            this.btnNewRemoveSubProject = new System.Windows.Forms.Button();
            this.btnNewEditSubProject = new System.Windows.Forms.Button();
            this.btnNewSubProject = new System.Windows.Forms.Button();
            this.lvTasks = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button7 = new System.Windows.Forms.Button();
            this.btnNewRemoveTasks = new System.Windows.Forms.Button();
            this.btnNewEditTasks = new System.Windows.Forms.Button();
            this.btnNewTasks = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSelectAllTasks = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 362);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 26);
            this.button2.TabIndex = 12;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(491, 480);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 26);
            this.button1.TabIndex = 11;
            this.button1.Text = "Έξοδος";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnNewRemoveProject
            // 
            this.btnNewRemoveProject.Enabled = false;
            this.btnNewRemoveProject.Location = new System.Drawing.Point(98, 362);
            this.btnNewRemoveProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewRemoveProject.Name = "btnNewRemoveProject";
            this.btnNewRemoveProject.Size = new System.Drawing.Size(109, 26);
            this.btnNewRemoveProject.TabIndex = 10;
            this.btnNewRemoveProject.Text = "Διαγραφή";
            this.btnNewRemoveProject.UseVisualStyleBackColor = true;
            this.btnNewRemoveProject.Click += new System.EventHandler(this.btnNewRemoveProject_Click);
            // 
            // btnNewEditProject
            // 
            this.btnNewEditProject.Enabled = false;
            this.btnNewEditProject.Location = new System.Drawing.Point(98, 330);
            this.btnNewEditProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewEditProject.Name = "btnNewEditProject";
            this.btnNewEditProject.Size = new System.Drawing.Size(109, 26);
            this.btnNewEditProject.TabIndex = 9;
            this.btnNewEditProject.Text = "Επεξεργασία";
            this.btnNewEditProject.UseVisualStyleBackColor = true;
            this.btnNewEditProject.Click += new System.EventHandler(this.btnNewEditProject_Click);
            // 
            // lvProjects
            // 
            this.lvProjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader9});
            this.lvProjects.FullRowSelect = true;
            this.lvProjects.GridLines = true;
            this.lvProjects.HideSelection = false;
            this.lvProjects.Location = new System.Drawing.Point(13, 29);
            this.lvProjects.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvProjects.MultiSelect = false;
            this.lvProjects.Name = "lvProjects";
            this.lvProjects.Size = new System.Drawing.Size(277, 276);
            this.lvProjects.TabIndex = 8;
            this.lvProjects.UseCompatibleStateImageBehavior = false;
            this.lvProjects.View = System.Windows.Forms.View.Details;
            this.lvProjects.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.lvProjects.DoubleClick += new System.EventHandler(this.btnNewEditProject_Click);
            this.lvProjects.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listView1_KeyDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ΑΑ";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Όνομα";
            this.columnHeader2.Width = 96;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Κατάσταση";
            this.columnHeader9.Width = 111;
            // 
            // btnNewProject
            // 
            this.btnNewProject.Location = new System.Drawing.Point(13, 330);
            this.btnNewProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewProject.Name = "btnNewProject";
            this.btnNewProject.Size = new System.Drawing.Size(77, 26);
            this.btnNewProject.TabIndex = 7;
            this.btnNewProject.Text = "Εισαγωγή";
            this.btnNewProject.UseVisualStyleBackColor = true;
            this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // lvSubProjects
            // 
            this.lvSubProjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvSubProjects.FullRowSelect = true;
            this.lvSubProjects.GridLines = true;
            this.lvSubProjects.HideSelection = false;
            this.lvSubProjects.Location = new System.Drawing.Point(374, 26);
            this.lvSubProjects.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvSubProjects.MultiSelect = false;
            this.lvSubProjects.Name = "lvSubProjects";
            this.lvSubProjects.Size = new System.Drawing.Size(277, 276);
            this.lvSubProjects.TabIndex = 13;
            this.lvSubProjects.UseCompatibleStateImageBehavior = false;
            this.lvSubProjects.View = System.Windows.Forms.View.Details;
            this.lvSubProjects.SelectedIndexChanged += new System.EventHandler(this.lvSubProjects_SelectedIndexChanged);
            this.lvSubProjects.DoubleClick += new System.EventHandler(this.btnNewEditSubProject_Click);
            this.lvSubProjects.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvSubProjects_KeyDown);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ΑΑ";
            this.columnHeader3.Width = 56;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Όνομα";
            this.columnHeader4.Width = 91;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Κατάσταση";
            this.columnHeader5.Width = 111;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(374, 362);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 26);
            this.button3.TabIndex = 17;
            this.button3.Text = "Refresh";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnNewRemoveSubProject
            // 
            this.btnNewRemoveSubProject.Enabled = false;
            this.btnNewRemoveSubProject.Location = new System.Drawing.Point(459, 362);
            this.btnNewRemoveSubProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewRemoveSubProject.Name = "btnNewRemoveSubProject";
            this.btnNewRemoveSubProject.Size = new System.Drawing.Size(109, 26);
            this.btnNewRemoveSubProject.TabIndex = 16;
            this.btnNewRemoveSubProject.Text = "Διαγραφή";
            this.btnNewRemoveSubProject.UseVisualStyleBackColor = true;
            this.btnNewRemoveSubProject.Click += new System.EventHandler(this.btnNewRemoveSubProject_Click);
            // 
            // btnNewEditSubProject
            // 
            this.btnNewEditSubProject.Enabled = false;
            this.btnNewEditSubProject.Location = new System.Drawing.Point(459, 330);
            this.btnNewEditSubProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewEditSubProject.Name = "btnNewEditSubProject";
            this.btnNewEditSubProject.Size = new System.Drawing.Size(109, 26);
            this.btnNewEditSubProject.TabIndex = 15;
            this.btnNewEditSubProject.Text = "Επεξεργασία";
            this.btnNewEditSubProject.UseVisualStyleBackColor = true;
            this.btnNewEditSubProject.Click += new System.EventHandler(this.btnNewEditSubProject_Click);
            // 
            // btnNewSubProject
            // 
            this.btnNewSubProject.Location = new System.Drawing.Point(374, 330);
            this.btnNewSubProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewSubProject.Name = "btnNewSubProject";
            this.btnNewSubProject.Size = new System.Drawing.Size(77, 26);
            this.btnNewSubProject.TabIndex = 14;
            this.btnNewSubProject.Text = "Εισαγωγή";
            this.btnNewSubProject.UseVisualStyleBackColor = true;
            this.btnNewSubProject.Click += new System.EventHandler(this.btnNewSubProject_Click);
            // 
            // lvTasks
            // 
            this.lvTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTasks.CheckBoxes = true;
            this.lvTasks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lvTasks.FullRowSelect = true;
            this.lvTasks.GridLines = true;
            this.lvTasks.HideSelection = false;
            this.lvTasks.Location = new System.Drawing.Point(735, 26);
            this.lvTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvTasks.MultiSelect = false;
            this.lvTasks.Name = "lvTasks";
            this.lvTasks.Size = new System.Drawing.Size(277, 276);
            this.lvTasks.TabIndex = 18;
            this.lvTasks.UseCompatibleStateImageBehavior = false;
            this.lvTasks.View = System.Windows.Forms.View.Details;
            this.lvTasks.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvTasks_ItemChecked);
            this.lvTasks.SelectedIndexChanged += new System.EventHandler(this.lvTasks_SelectedIndexChanged);
            this.lvTasks.DoubleClick += new System.EventHandler(this.btnNewEditTasks_Click);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ΑΑ";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Όνομα";
            this.columnHeader7.Width = 96;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Κατάσταση";
            this.columnHeader8.Width = 111;
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Location = new System.Drawing.Point(736, 362);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(77, 26);
            this.button7.TabIndex = 22;
            this.button7.Text = "Refresh";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnNewRemoveTasks
            // 
            this.btnNewRemoveTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewRemoveTasks.Enabled = false;
            this.btnNewRemoveTasks.Location = new System.Drawing.Point(821, 362);
            this.btnNewRemoveTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewRemoveTasks.Name = "btnNewRemoveTasks";
            this.btnNewRemoveTasks.Size = new System.Drawing.Size(109, 26);
            this.btnNewRemoveTasks.TabIndex = 21;
            this.btnNewRemoveTasks.Text = "Διαγραφή";
            this.btnNewRemoveTasks.UseVisualStyleBackColor = true;
            this.btnNewRemoveTasks.Click += new System.EventHandler(this.btnNewRemoveTasks_Click);
            // 
            // btnNewEditTasks
            // 
            this.btnNewEditTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewEditTasks.Enabled = false;
            this.btnNewEditTasks.Location = new System.Drawing.Point(821, 330);
            this.btnNewEditTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewEditTasks.Name = "btnNewEditTasks";
            this.btnNewEditTasks.Size = new System.Drawing.Size(109, 26);
            this.btnNewEditTasks.TabIndex = 15;
            this.btnNewEditTasks.Text = "Επεξεργασία";
            this.btnNewEditTasks.UseVisualStyleBackColor = true;
            this.btnNewEditTasks.Click += new System.EventHandler(this.btnNewEditTasks_Click);
            // 
            // btnNewTasks
            // 
            this.btnNewTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewTasks.Location = new System.Drawing.Point(736, 330);
            this.btnNewTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewTasks.Name = "btnNewTasks";
            this.btnNewTasks.Size = new System.Drawing.Size(77, 26);
            this.btnNewTasks.TabIndex = 19;
            this.btnNewTasks.Text = "Εισαγωγή";
            this.btnNewTasks.UseVisualStyleBackColor = true;
            this.btnNewTasks.Click += new System.EventHandler(this.btnNewTasks_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 14);
            this.label1.TabIndex = 23;
            this.label1.Text = "Έργα";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(456, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 14);
            this.label2.TabIndex = 24;
            this.label2.Text = "Υποέργα";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(798, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 14);
            this.label3.TabIndex = 25;
            this.label3.Text = "Εργασίες";
            // 
            // chkSelectAllTasks
            // 
            this.chkSelectAllTasks.AutoSize = true;
            this.chkSelectAllTasks.Location = new System.Drawing.Point(736, 306);
            this.chkSelectAllTasks.Name = "chkSelectAllTasks";
            this.chkSelectAllTasks.Size = new System.Drawing.Size(115, 18);
            this.chkSelectAllTasks.TabIndex = 26;
            this.chkSelectAllTasks.Text = "Επιλογή Όλων";
            this.chkSelectAllTasks.UseVisualStyleBackColor = true;
            this.chkSelectAllTasks.CheckedChanged += new System.EventHandler(this.chkSelectAllTasks_CheckedChanged);
            // 
            // frmProjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 518);
            this.Controls.Add(this.chkSelectAllTasks);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.btnNewRemoveTasks);
            this.Controls.Add(this.btnNewEditTasks);
            this.Controls.Add(this.btnNewTasks);
            this.Controls.Add(this.lvTasks);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnNewRemoveSubProject);
            this.Controls.Add(this.btnNewEditSubProject);
            this.Controls.Add(this.btnNewSubProject);
            this.Controls.Add(this.lvSubProjects);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnNewRemoveProject);
            this.Controls.Add(this.btnNewEditProject);
            this.Controls.Add(this.lvProjects);
            this.Controls.Add(this.btnNewProject);
            this.Font = new System.Drawing.Font("Verdana", 9F);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frmProjects";
            this.Text = "Διαχείρηση Έργων";
            this.Load += new System.EventHandler(this.frmProjects_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNewRemoveProject;
        private System.Windows.Forms.Button btnNewEditProject;
        private System.Windows.Forms.ListView lvProjects;
        private System.Windows.Forms.Button btnNewProject;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ListView lvSubProjects;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnNewRemoveSubProject;
        private System.Windows.Forms.Button btnNewEditSubProject;
        private System.Windows.Forms.Button btnNewSubProject;
        private System.Windows.Forms.ListView lvTasks;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnNewRemoveTasks;
        private System.Windows.Forms.Button btnNewEditTasks;
        private System.Windows.Forms.Button btnNewTasks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkSelectAllTasks;
    }
}