﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmProjects : Form
    {

        #region frmProjects Functions
        public frmProjects()
        {
            InitializeComponent();
            
        }

        private void frmProjects_Load(object sender, EventArgs e)
        {
            btnNewSubProject.Enabled = false;
            btnNewTasks.Enabled = false;

            RefreshAllElementsProjects();
            
        }

        #endregion

        #region Project Functions
        private void btnNewProject_Click(object sender, EventArgs e)
        {
            NewProject newproject = new NewProject(null, Enumerations.PKTasksState.Added);
            newproject.MdiParent = this.MdiParent;

            newproject.OnNewProjectAdded += Newproject_OnNewProjectAdded;

            newproject.Show();
        }

        private void Newproject_OnNewProjectAdded(object sender, ProjectsEventArgs e)
        {
            NewProjectAdd(e.Project);

        }


        private void btnNewEditProject_Click(object sender, EventArgs e)
        {
            int j = 0;
            int.TryParse(lvProjects.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TasksDB.Project project = DB.ReturnOneProject(j);

            NewProject projectformEdit = new NewProject(project, Enumerations.PKTasksState.Edited);
            projectformEdit.OnNewProjectEdited += ProjectformEdit_OnNewProjectEdited;
            projectformEdit.ShowDialog();

        }

        private void ProjectformEdit_OnNewProjectEdited(object sender, ProjectsEventArgs e)
        {
            UpdateEditProject(e.Project);
        }


        private void UpdateEditProject(TasksDB.Project Project)
        {
            ListViewItem item = lvProjects.SelectedItems[0];
            item.SubItems[0].Text = Project.Id.ToString();
            item.SubItems[1].Text = Project.Name;
            item.SubItems[2].Text = Project.Status.Name.ToString();

        }

        
        private void btnNewRemoveProject_Click(object sender, EventArgs e)
        {
            string tmp = "Θέλετε να διαγραψετε το επιλεγμένο έργο και όλα τα περιεχόμενα του;\r\n\tΌνομα : " + lvProjects.SelectedItems[0].SubItems[1].Text;

            if (MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                return;
            }

            int j = 0;
            int.TryParse(lvProjects.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TasksDB.Project project = DB.ReturnOneProject(j);
            TasksBL.ProjectService pr = new TasksBL.ProjectService();
            pr.Remove(project);
            RemoveItemProject();
        }


        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool test = (this.lvProjects.SelectedItems.Count > 0);
            this.btnNewRemoveProject.Enabled = test;
            this.btnNewEditProject.Enabled = test;
            this.btnNewTasks.Enabled = test;
            this.btnNewSubProject.Enabled = test;
            if (test == true)
            {
                RefreshAllElementsSubProjects();
            }
            else
            {
                lvSubProjects.Items.Clear();
                this.btnNewSubProject.Enabled = test;
                this.btnNewRemoveSubProject.Enabled = test;
                this.btnNewEditSubProject.Enabled = test;

                lvTasks.Items.Clear();
                this.btnNewTasks.Enabled = test;
                this.btnNewRemoveTasks.Enabled = test;
                this.btnNewEditTasks.Enabled = test;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                btnNewRemoveProject_Click(null, null);
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnNewEditProject_Click(null, null);
            }
            if (e.KeyCode == Keys.Insert)
            {
                btnNewProject_Click(null, null);
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            RefreshAllElementsProjects();
        }


        private void RemoveItemProject()
        {
            lvProjects.Items.RemoveAt(lvProjects.SelectedIndices[0]);
        }


        private void NewProjectAdd(TasksDB.Project Project)
        {
            ListViewItem lvi = new ListViewItem(Project.Id.ToString());

            lvProjects.Items.Add(lvi);

            lvi.SubItems.Add(Project.Name);
            lvi.SubItems.Add(Project.Status.Name);

        }
        #endregion

        #region SubProject Functions
        private void btnNewSubProject_Click(object sender, EventArgs e)
        {
            frmNewSubProject subproject = new frmNewSubProject(Int32.Parse(lvProjects.SelectedItems[0].Text), null, Enumerations.PKTasksState.Added);
            subproject.OnNewSubProjectAdded += Subproject_OnNewSubProjectAdded;
            subproject.ShowDialog(this);
        }

        private void Subproject_OnNewSubProjectAdded(object sender, SubProjectsEventArgs e)
        {
            NewSubProjectAdd(e.SubProject);
        }

        private void btnNewEditSubProject_Click(object sender, EventArgs e)
        {
            int j = 0;
            int.TryParse(lvSubProjects.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TasksDB.SubProject Subproject = DB.ReturnOneSubProject(j);

            frmNewSubProject subproject = new frmNewSubProject(Int32.Parse(lvProjects.SelectedItems[0].Text),Subproject, Enumerations.PKTasksState.Edited);
            subproject.OnNewSubProjectEdited += Subproject_OnNewSubProjectEdited1;
            subproject.ShowDialog(this);

        }

        private void Subproject_OnNewSubProjectEdited1(object sender, SubProjectsEventArgs e)
        {
            UpdateEditSubProject(e.SubProject);
        }



        private void UpdateEditSubProject(TasksDB.SubProject SubProject)
        {
            ListViewItem item = lvSubProjects.SelectedItems[0];
            item.SubItems[0].Text = SubProject.Id.ToString();
            item.SubItems[1].Text = SubProject.Name;
            item.SubItems[2].Text = SubProject.Status.Name.ToString();

        }


        private void NewSubProjectAdd(TasksDB.SubProject SubProject)
        {
            ListViewItem lvi = new ListViewItem(SubProject.Id.ToString());

            lvSubProjects.Items.Add(lvi);
            lvi.SubItems.Add(SubProject.Name);
            lvi.SubItems.Add(SubProject.Status.Name);

        }


        private void button3_Click(object sender, EventArgs e)
        {
            RefreshAllElementsSubProjects();
        }

        private void btnNewRemoveSubProject_Click(object sender, EventArgs e)
        {
            if (lvTasks.Items.Count == 0)
            {
                string tmp = "Θέλετε να διαγραψετε το επιλεγμένο υποέργο;\r\n\tΌνομα : " + lvSubProjects.SelectedItems[0].SubItems[1].Text;

                if (MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) != DialogResult.Yes)
                {
                    return;
                }

                int j = 0;
                int.TryParse(lvSubProjects.SelectedItems[0].Text, out j);

                DBAccess DB = new DBAccess();
                TasksDB.SubProject subproject = DB.ReturnOneSubProject(j);
                TasksBL.SubProjectService pr = new TasksBL.SubProjectService();
                pr.Remove(subproject);
                RemoveItemSubProject();
            }
            else {
                MessageBox.Show("Διαγράψτε όλες τις εργασίες");
            }
        }


        private void lvSubProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool test = (this.lvSubProjects.SelectedItems.Count > 0);
            this.btnNewRemoveSubProject.Enabled = test;
            this.btnNewEditSubProject.Enabled = test;
            this.btnNewTasks.Enabled = test;
            if (test == true)
            {
                RefreshAllElementsTasks();
            }
            else
            {
                lvTasks.Items.Clear();
                this.btnNewTasks.Enabled = test;
                this.btnNewRemoveTasks.Enabled = test;
                this.btnNewEditTasks.Enabled = test;
            }
        }

        private void lvSubProjects_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                btnNewRemoveSubProject_Click(null, null);
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnNewEditSubProject_Click(null, null);
            }
            if (e.KeyCode == Keys.Insert)
            {
                btnNewSubProject_Click(null, null);
            }
        }


        private void RemoveItemSubProject()
        {
            lvSubProjects.Items.RemoveAt(lvSubProjects.SelectedIndices[0]);
        }


        #endregion

        #region Tasks Functions
        private void btnNewEditTasks_Click(object sender, EventArgs e)
        {
            if (lvTasks.SelectedItems.Count > 0)
            {
                int taskId = 0;
                int.TryParse(lvTasks.SelectedItems[0].Text, out taskId);

                frmNewTask task = new frmNewTask(Int32.Parse(lvSubProjects.SelectedItems[0].Text), taskId, Enumerations.PKTasksState.Edited);
                task.OnNewTaskEdited += Task_OnNewTaskEdited;
                task.ShowDialog(this);
            }
        }

        private void Task_OnNewTaskEdited(object sender, TasksEventArgs e)
        {
            UpdateEditTask(e.Task);
        }

        private void btnNewTasks_Click(object sender, EventArgs e)
        {
            frmNewTask task = new frmNewTask(Int32.Parse(lvSubProjects.SelectedItems[0].Text), -1, Enumerations.PKTasksState.Added);
            task.OnNewTaskAdded += Task_OnNewTaskAdded;
            task.ShowDialog(this);
        }

        private void Task_OnNewTaskAdded(object sender, TasksEventArgs e)
        {
            NewTaskAdd(e.Task);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            RefreshAllElementsTasks();
        }

        private void btnNewRemoveTasks_Click(object sender, EventArgs e)
        {
            if (chkSelectAllTasks.Checked == true)
            {
                string tmp = "Θέλετε να διαγραψετε όλες τις εργασίες;";
                if (MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) != DialogResult.Yes)
                {
                    return;
                }
                int i;
                for (i = 0; i < lvTasks.Items.Count; i++)
                {
                    int j = 0;
                    int.TryParse(lvTasks.Items[i].Text, out j);


                    TasksBL.TaskService pr = new TasksBL.TaskService();
                    TasksDB.Task task = pr.ReturnOneTask(j);
                    pr.Remove(task);
                    
                }
                lvTasks.Items.Clear();
                
            }
            else
            {


                string tmp = "Θέλετε να διαγραψετε την επιλεγμένη εργασία;\r\n\tΌνομα : " + lvTasks.SelectedItems[0].SubItems[1].Text;

                if (MessageBox.Show(tmp, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) != DialogResult.Yes)
                {
                    return;
                }

                int j = 0;
                int.TryParse(lvTasks.SelectedItems[0].Text, out j);

                TasksBL.TaskService pr = new TasksBL.TaskService();
                TasksDB.Task task = pr.ReturnOneTask(j);
                
                pr.Remove(task);
                RemoveItemTask();
            }
            chkSelectAllTasks.Checked = false;
        }


        private void NewTaskAdd(TasksDB.Task Task)
        {
            ListViewItem lvi = new ListViewItem(Task.Id.ToString());

            lvTasks.Items.Add(lvi);
            lvi.SubItems.Add(Task.Name);
            lvi.SubItems.Add(Task.Status.Name);

        }
        private void UpdateEditTask(TasksDB.Task Task)
        {
            ListViewItem item = lvTasks.SelectedItems[0];
            item.SubItems[0].Text = Task.Id.ToString();
            item.SubItems[1].Text = Task.Name;
            item.SubItems[2].Text = Task.Status.Name.ToString();

        }


        private void lvTasks_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToggleDeleteButton();
            ToggleEditButton();

        }

        private void chkSelectAllTasks_CheckedChanged(object sender, EventArgs e)
        {
            CheckAllTheCheckboxes();
            ToggleDeleteButton();
        }

        private void CheckAllTheCheckboxes()
        {
            bool IsChecked = this.chkSelectAllTasks.Checked;
            for (int y = 0; y < lvTasks.Items.Count; y++)
            {
                lvTasks.Items[y].Checked = IsChecked;
            }
        }


        private void ToggleDeleteButton()
        {
            
            this.btnNewRemoveTasks.Enabled = (this.lvTasks.SelectedItems.Count > 0 || this.lvTasks.CheckedItems.Count >0);
        }

        private void ToggleEditButton()
        {
           
            this.btnNewEditTasks.Enabled = (this.lvTasks.SelectedItems.Count > 0);
        }

        private void RemoveItemTask()
        {
            lvTasks.Items.RemoveAt(lvTasks.SelectedIndices[0]);
        }

        #endregion

        #region RefreshAllElements
        private void RefreshAllElementsProjects()
        {
            lvProjects.Items.Clear();

            List<TasksDB.Project> Projects = new List<TasksDB.Project>();
            ProjectService srvpr = new ProjectService();
            Projects = srvpr.GetAllProjects();

            int i;
            for (i = 0; i < Projects.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(Projects[i].Id.ToString());

                lvProjects.Items.Add(lvi);

                lvi.SubItems.Add(Projects[i].Name);
                lvi.SubItems.Add(Projects[i].Status.Name);

            }

        }



        private void RefreshAllElementsSubProjects()
        {
            lvSubProjects.Items.Clear();

            List<TasksDB.SubProject> SubProjects = new List<TasksDB.SubProject>();
            SubProjectService srvsbpr = new SubProjectService();
            SubProjects = srvsbpr.GetProjectSubProjects(Int32.Parse(lvProjects.SelectedItems[0].Text));

            int i;
            for (i = 0; i < SubProjects.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(SubProjects[i].Id.ToString());

                lvSubProjects.Items.Add(lvi);
                lvi.SubItems.Add(SubProjects[i].Name);
                lvi.SubItems.Add(SubProjects[i].Status.Name);

            }
            if (this.lvSubProjects.Items.Count > 0)
            {
                this.lvSubProjects.Items[0].Selected = true;
            }
            else
            {
                this.lvSubProjects_SelectedIndexChanged(null, null);    
            }

        }


        private void RefreshAllElementsTasks()
        {
            lvTasks.Items.Clear();

            List<TasksDB.Task> Tasks = new List<TasksDB.Task>();
            TaskService srvtk = new TaskService();
            Tasks = srvtk.GetSubProjectTasks(Int32.Parse(lvSubProjects.SelectedItems[0].Text));

            int i;
            for (i = 0; i < Tasks.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(Tasks[i].Id.ToString());

                lvTasks.Items.Add(lvi);
                lvi.SubItems.Add(Tasks[i].Name);
                lvi.SubItems.Add(Tasks[i].Status.Name);

            }
            if (this.lvTasks.Items.Count > 0)
            {
                this.lvTasks.Items[0].Selected = true;
            }
            else
            {
                //this.lvSubProjects_SelectedIndexChanged(null, null);
            }
        }

        #endregion

        private void lvTasks_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            ToggleDeleteButton();
        }
    }
}
