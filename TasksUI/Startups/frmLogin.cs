﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HumanResourceService srvhr = new HumanResourceService();
            bool x;
            x = srvhr.ValidateHumanRecourceByCredentials(txtUsername.Text, txtPassword.Text);

            if (x == true)
            {
                HumanResource human = srvhr.GetHumanResource(txtUsername.Text);
                Globals.SetCurrentHumanId(human.Id);
                MDIMain main = new MDIMain();
                

                main.MdiParent = this.MdiParent;

                main.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Παρακαλώ εισάγετε σωστά στοιχεία");
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
