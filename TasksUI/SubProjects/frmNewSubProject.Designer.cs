﻿namespace TasksUI
{
    partial class frmNewSubProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblEstimatedStartDate = new System.Windows.Forms.Label();
            this.dtpDeadline = new System.Windows.Forms.DateTimePicker();
            this.lblEstimatedEndDate = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpEstimatedEndDate = new System.Windows.Forms.DateTimePicker();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.dtpEstimatedStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDeadline = new System.Windows.Forms.Label();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ucNotes1 = new TasksUI.UserControls.ucNotes();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(39, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "Όνομα";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(39, 65);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(62, 13);
            this.lblDescription.TabIndex = 20;
            this.lblDescription.Text = "Περιγραφή";
            // 
            // lblEstimatedStartDate
            // 
            this.lblEstimatedStartDate.AutoSize = true;
            this.lblEstimatedStartDate.Location = new System.Drawing.Point(39, 149);
            this.lblEstimatedStartDate.Name = "lblEstimatedStartDate";
            this.lblEstimatedStartDate.Size = new System.Drawing.Size(180, 13);
            this.lblEstimatedStartDate.TabIndex = 22;
            this.lblEstimatedStartDate.Text = "Εκτιμώμενη Ημερομηνία Εκκίνησης";
            // 
            // dtpDeadline
            // 
            this.dtpDeadline.Checked = false;
            this.dtpDeadline.Location = new System.Drawing.Point(361, 341);
            this.dtpDeadline.Name = "dtpDeadline";
            this.dtpDeadline.ShowCheckBox = true;
            this.dtpDeadline.Size = new System.Drawing.Size(217, 20);
            this.dtpDeadline.TabIndex = 31;
            // 
            // lblEstimatedEndDate
            // 
            this.lblEstimatedEndDate.AutoSize = true;
            this.lblEstimatedEndDate.Location = new System.Drawing.Point(39, 201);
            this.lblEstimatedEndDate.Name = "lblEstimatedEndDate";
            this.lblEstimatedEndDate.Size = new System.Drawing.Size(197, 13);
            this.lblEstimatedEndDate.TabIndex = 24;
            this.lblEstimatedEndDate.Text = "Εκτιμώμενη Ημερομηνία Ολοκλήρωσης";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            this.dtpEndDate.Location = new System.Drawing.Point(361, 294);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEndDate.TabIndex = 29;
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(39, 252);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(119, 13);
            this.lblStartDate.TabIndex = 26;
            this.lblStartDate.Text = "Ημερομηνία Εκκίνησης";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            this.dtpStartDate.Location = new System.Drawing.Point(361, 246);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.ShowCheckBox = true;
            this.dtpStartDate.Size = new System.Drawing.Size(217, 20);
            this.dtpStartDate.TabIndex = 27;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(39, 300);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(136, 13);
            this.lblEndDate.TabIndex = 28;
            this.lblEndDate.Text = "Ημερομηνία Ολοκλήρωσης";
            // 
            // dtpEstimatedEndDate
            // 
            this.dtpEstimatedEndDate.Checked = false;
            this.dtpEstimatedEndDate.Location = new System.Drawing.Point(361, 195);
            this.dtpEstimatedEndDate.Name = "dtpEstimatedEndDate";
            this.dtpEstimatedEndDate.ShowCheckBox = true;
            this.dtpEstimatedEndDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEstimatedEndDate.TabIndex = 25;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(457, 380);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(121, 21);
            this.cmbStatus.TabIndex = 33;
            this.cmbStatus.SelectedIndexChanged += new System.EventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // dtpEstimatedStartDate
            // 
            this.dtpEstimatedStartDate.Checked = false;
            this.dtpEstimatedStartDate.Location = new System.Drawing.Point(361, 149);
            this.dtpEstimatedStartDate.Name = "dtpEstimatedStartDate";
            this.dtpEstimatedStartDate.ShowCheckBox = true;
            this.dtpEstimatedStartDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEstimatedStartDate.TabIndex = 23;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(39, 388);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(66, 13);
            this.lblStatus.TabIndex = 32;
            this.lblStatus.Text = "Κατάσταση";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(378, 29);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(200, 20);
            this.txtName.TabIndex = 19;
            // 
            // txtDescription
            // 
            this.txtDescription.AcceptsReturn = true;
            this.txtDescription.Location = new System.Drawing.Point(291, 65);
            this.txtDescription.MaxLength = 500;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDescription.Size = new System.Drawing.Size(287, 70);
            this.txtDescription.TabIndex = 21;
            // 
            // lblDeadline
            // 
            this.lblDeadline.AutoSize = true;
            this.lblDeadline.Location = new System.Drawing.Point(39, 347);
            this.lblDeadline.Name = "lblDeadline";
            this.lblDeadline.Size = new System.Drawing.Size(98, 13);
            this.lblDeadline.TabIndex = 30;
            this.lblDeadline.Text = "Οριακή Προθεσμία";
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(647, 446);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(166, 61);
            this.btnInsert.TabIndex = 37;
            this.btnInsert.Text = "ΟΚ";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(858, 446);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(166, 61);
            this.btnCancel.TabIndex = 38;
            this.btnCancel.Text = "Ακύρωση";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ucNotes1
            // 
            this.ucNotes1.Location = new System.Drawing.Point(623, 53);
            this.ucNotes1.Name = "ucNotes1";
            this.ucNotes1.Size = new System.Drawing.Size(503, 348);
            this.ucNotes1.TabIndex = 39;
            // 
            // frmNewSubProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 564);
            this.Controls.Add(this.ucNotes1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblEstimatedStartDate);
            this.Controls.Add(this.dtpDeadline);
            this.Controls.Add(this.lblEstimatedEndDate);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.dtpEstimatedEndDate);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.dtpEstimatedStartDate);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDeadline);
            this.Name = "frmNewSubProject";
            this.Text = "frmNewSubProject";
            this.Load += new System.EventHandler(this.frmNewSubProject_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblEstimatedStartDate;
        private System.Windows.Forms.DateTimePicker dtpDeadline;
        private System.Windows.Forms.Label lblEstimatedEndDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpEstimatedEndDate;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.DateTimePicker dtpEstimatedStartDate;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDeadline;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnCancel;
        private UserControls.ucNotes ucNotes1;
    }
}