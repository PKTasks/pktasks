﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmNewSubProject : Form
    {
        Enumerations.PKTasksState SubProjectState;
        TasksDB.SubProject SubProjectTemp;
        int ProjectId_sub;

        public event EventHandler<SubProjectsEventArgs> OnNewSubProjectAdded;
        public event EventHandler<SubProjectsEventArgs> OnNewSubProjectEdited;
        
        public frmNewSubProject(int projectId_sub,TasksDB.SubProject subproject, Enumerations.PKTasksState projectstate)
        {
            InitializeComponent();
            ComboBox_AutoComplete();

            SubProjectState = projectstate;
            ProjectId_sub = projectId_sub;

            if (projectstate == Enumerations.PKTasksState.Edited)
            {
                ucNotes1.Initialize(subproject.Id, Enumerations.PKTasksEntryType.SubProject);
            }
            else
            {
                ucNotes1.Visible = false;
            }
            

            if (subproject != null)
            {
                SubProjectTemp = subproject;
                txtName.Text = subproject.Name;
                txtDescription.Text = subproject.Description;

                if (subproject.EstimatedStartDate.HasValue)
                {
                    dtpEstimatedStartDate.Checked = true;
                    dtpEstimatedStartDate.Value = subproject.EstimatedStartDate.Value;
                }
                if (subproject.EstimatedEndDate.HasValue)
                {
                    dtpEstimatedEndDate.Checked = true;
                    dtpEstimatedEndDate.Value = subproject.EstimatedEndDate.Value;
                }
                if (subproject.StartDate.HasValue)
                {
                    dtpStartDate.Checked = true;
                    dtpStartDate.Value = subproject.StartDate.Value;
                }
                if (subproject.EndDate.HasValue)
                {
                    dtpEndDate.Checked = true;
                    dtpEndDate.Value = subproject.EndDate.Value;
                }
                if (subproject.Deadline.HasValue)
                {
                    dtpDeadline.Checked = true;
                    dtpDeadline.Value = subproject.Deadline.Value;
                }

                SetComboSelectedItemById(subproject.StatusId);

                //txtStatusExplain.Text = subproject.StatusExplain;

                //this.subproject.ProjectId = SubProjectTemp.Id;
            }
        }



        private void SetComboSelectedItemById(byte id)
        {
            for (int i = 0; i < cmbStatus.Items.Count; i++)
            {
                if (((TasksDB.Status)cmbStatus.Items[i]).Id == id)
                {
                    cmbStatus.SelectedIndex = i;
                    break;
                }
            }
        }


        private void frmNewSubProject_Load(object sender, EventArgs e)
        {
            switch (SubProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Νέο Υποέργο";
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία Υποέργου";
                    break;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void ComboBox_AutoComplete()
        {
            StatusService srvst = new StatusService(); ;
            cmbStatus.DataSource = null;

            cmbStatus.DisplayMember = "Name";
            cmbStatus.DataSource = srvst.GetSubProjectStatus();
            if (cmbStatus.Items.Count > 0)
            {
                cmbStatus.SelectedIndex = 0;
            }

            //cmbDescription.Items.Clear();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {

            if (txtName.Text == "")
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε ένα όνομα");
                return;
            }

            TasksDB.SubProject subproject = new TasksDB.SubProject();
            TasksBL.SubProjectService sbpr = new TasksBL.SubProjectService();

            subproject.Name = txtName.Text;

            if (!string.IsNullOrEmpty(txtDescription.Text))
            {
                subproject.Description = txtDescription.Text;
            }
            else
            {
                subproject.Description = null;
            }

            if (dtpEstimatedStartDate.Checked)
            {
                subproject.EstimatedStartDate = dtpEstimatedStartDate.Value.Date;
            }
            else
            {
                subproject.EstimatedStartDate = null;
            }


            if (dtpEstimatedEndDate.Checked)
            {
                subproject.EstimatedEndDate = dtpEstimatedEndDate.Value.Date;
            }
            else
            {
                subproject.EstimatedEndDate = null;
            }

            if (dtpStartDate.Checked)
            {
                subproject.StartDate = dtpStartDate.Value.Date;
            }
            else
            {
                subproject.StartDate = null;
            }

            if (dtpEndDate.Checked)
            {
                subproject.EndDate = dtpEndDate.Value.Date;
            }
            else
            {
                subproject.EndDate = null;
            }

            if (dtpDeadline.Checked)
            {
                subproject.Deadline = dtpDeadline.Value.Date;
            }
            else
            {
                subproject.Deadline = null;
            }

            subproject.StatusId = ((TasksDB.Status)cmbStatus.SelectedValue).Id;

            //if (!string.IsNullOrEmpty(txtStatusExplain.Text))
            //{
            //    subproject.StatusExplain = txtStatusExplain.Text;
            //}
            //else
            //{
            //    subproject.StatusExplain = null;
            //}

            switch (SubProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    subproject.ProjectId = ProjectId_sub;
                    sbpr.Add(subproject);
                    if (OnNewSubProjectAdded != null)
                    {
                        TasksDB.DBAccess p = new DBAccess();
                        TasksDB.SubProject t = p.ReturnOneSubProject(subproject.Id);

                        SubProjectsEventArgs args = new SubProjectsEventArgs();
                        args.SubProject = t;
                        OnNewSubProjectAdded.Invoke(this, args);
                    }
                    break;
                case Enumerations.PKTasksState.Edited:
                    subproject.ProjectId = SubProjectTemp.ProjectId;
                    subproject.Id = SubProjectTemp.Id;
                    sbpr.Edit(subproject);
                    if (OnNewSubProjectEdited != null)
                    {
                        TasksDB.DBAccess p = new DBAccess();
                        TasksDB.SubProject t = p.ReturnOneSubProject(subproject.Id);

                        SubProjectsEventArgs args = new SubProjectsEventArgs();
                        args.SubProject = t;
                        OnNewSubProjectEdited.Invoke(this, args);
                    }
                    break;
            }

            this.Close();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
