﻿namespace TasksUI
{
    partial class frmNewTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblEstimatedStartDate = new System.Windows.Forms.Label();
            this.dtpDeadline = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpEstimatedEndDate = new System.Windows.Forms.DateTimePicker();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.dtpEstimatedStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblStatusExplain = new System.Windows.Forms.Label();
            this.txtStatusExplain = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDeadline = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHumanResource = new System.Windows.Forms.Label();
            this.cmbEmployee = new System.Windows.Forms.ComboBox();
            this.lblEstimatedDuration = new System.Windows.Forms.Label();
            this.nudEstimatedDuration = new System.Windows.Forms.NumericUpDown();
            this.rbDay = new System.Windows.Forms.RadioButton();
            this.rbHour = new System.Windows.Forms.RadioButton();
            this.tbTasks = new System.Windows.Forms.TabControl();
            this.tbpRelatedTasks = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.lvSelectedTasks = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnexportFromList = new System.Windows.Forms.Button();
            this.lvUnSelectedTasks = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnInsertToList = new System.Windows.Forms.Button();
            this.lblPreviousTask = new System.Windows.Forms.Label();
            this.lblNotPreviousTask = new System.Windows.Forms.Label();
            this.tbpTasksDuration = new System.Windows.Forms.TabPage();
            this.lvTaskDurations = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucNotes1 = new TasksUI.UserControls.ucNotes();
            ((System.ComponentModel.ISupportInitialize)(this.nudEstimatedDuration)).BeginInit();
            this.tbTasks.SuspendLayout();
            this.tbpRelatedTasks.SuspendLayout();
            this.tbpTasksDuration.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(863, 453);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(166, 61);
            this.btnCancel.TabIndex = 58;
            this.btnCancel.Text = "Ακύρωση";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(663, 453);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(166, 61);
            this.btnInsert.TabIndex = 57;
            this.btnInsert.Text = "ΟΚ";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(28, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 39;
            this.lblName.Text = "Όνομα";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(28, 66);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(62, 13);
            this.lblDescription.TabIndex = 41;
            this.lblDescription.Text = "Περιγραφή";
            // 
            // lblEstimatedStartDate
            // 
            this.lblEstimatedStartDate.AutoSize = true;
            this.lblEstimatedStartDate.Location = new System.Drawing.Point(28, 150);
            this.lblEstimatedStartDate.Name = "lblEstimatedStartDate";
            this.lblEstimatedStartDate.Size = new System.Drawing.Size(180, 13);
            this.lblEstimatedStartDate.TabIndex = 43;
            this.lblEstimatedStartDate.Text = "Εκτιμώμενη Ημερομηνία Εκκίνησης";
            // 
            // dtpDeadline
            // 
            this.dtpDeadline.Checked = false;
            this.dtpDeadline.Location = new System.Drawing.Point(357, 293);
            this.dtpDeadline.Name = "dtpDeadline";
            this.dtpDeadline.ShowCheckBox = true;
            this.dtpDeadline.Size = new System.Drawing.Size(217, 20);
            this.dtpDeadline.TabIndex = 52;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            this.dtpEndDate.Location = new System.Drawing.Point(357, 267);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEndDate.TabIndex = 50;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Checked = false;
            this.dtpStartDate.Location = new System.Drawing.Point(357, 241);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.ShowCheckBox = true;
            this.dtpStartDate.Size = new System.Drawing.Size(217, 20);
            this.dtpStartDate.TabIndex = 48;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(28, 267);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(136, 13);
            this.lblEndDate.TabIndex = 49;
            this.lblEndDate.Text = "Ημερομηνία Ολοκλήρωσης";
            // 
            // dtpEstimatedEndDate
            // 
            this.dtpEstimatedEndDate.Checked = false;
            this.dtpEstimatedEndDate.Location = new System.Drawing.Point(357, 215);
            this.dtpEstimatedEndDate.Name = "dtpEstimatedEndDate";
            this.dtpEstimatedEndDate.ShowCheckBox = true;
            this.dtpEstimatedEndDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEstimatedEndDate.TabIndex = 46;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(453, 319);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(121, 21);
            this.cmbStatus.TabIndex = 54;
            this.cmbStatus.SelectedIndexChanged += new System.EventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // dtpEstimatedStartDate
            // 
            this.dtpEstimatedStartDate.Checked = false;
            this.dtpEstimatedStartDate.Location = new System.Drawing.Point(357, 150);
            this.dtpEstimatedStartDate.Name = "dtpEstimatedStartDate";
            this.dtpEstimatedStartDate.ShowCheckBox = true;
            this.dtpEstimatedStartDate.Size = new System.Drawing.Size(217, 20);
            this.dtpEstimatedStartDate.TabIndex = 44;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(28, 319);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(66, 13);
            this.lblStatus.TabIndex = 53;
            this.lblStatus.Text = "Κατάσταση";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(374, 30);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(200, 20);
            this.txtName.TabIndex = 40;
            // 
            // lblStatusExplain
            // 
            this.lblStatusExplain.AutoSize = true;
            this.lblStatusExplain.Location = new System.Drawing.Point(28, 346);
            this.lblStatusExplain.Name = "lblStatusExplain";
            this.lblStatusExplain.Size = new System.Drawing.Size(129, 13);
            this.lblStatusExplain.TabIndex = 55;
            this.lblStatusExplain.Text = "Επεξήγηση Κατάστασης";
            // 
            // txtStatusExplain
            // 
            this.txtStatusExplain.AcceptsReturn = true;
            this.txtStatusExplain.Location = new System.Drawing.Point(310, 346);
            this.txtStatusExplain.MaxLength = 500;
            this.txtStatusExplain.Multiline = true;
            this.txtStatusExplain.Name = "txtStatusExplain";
            this.txtStatusExplain.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStatusExplain.Size = new System.Drawing.Size(264, 55);
            this.txtStatusExplain.TabIndex = 56;
            // 
            // txtDescription
            // 
            this.txtDescription.AcceptsReturn = true;
            this.txtDescription.Location = new System.Drawing.Point(287, 66);
            this.txtDescription.MaxLength = 500;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDescription.Size = new System.Drawing.Size(287, 70);
            this.txtDescription.TabIndex = 42;
            // 
            // lblDeadline
            // 
            this.lblDeadline.AutoSize = true;
            this.lblDeadline.Location = new System.Drawing.Point(28, 293);
            this.lblDeadline.Name = "lblDeadline";
            this.lblDeadline.Size = new System.Drawing.Size(98, 13);
            this.lblDeadline.TabIndex = 51;
            this.lblDeadline.Text = "Οριακή Προθεσμία";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Εκτιμώμενη Ημερομηνία Ολοκλήρωσης";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 62;
            this.label2.Text = "Ημερομηνία Εκκίνησης";
            // 
            // lblHumanResource
            // 
            this.lblHumanResource.AutoSize = true;
            this.lblHumanResource.Location = new System.Drawing.Point(28, 407);
            this.lblHumanResource.Name = "lblHumanResource";
            this.lblHumanResource.Size = new System.Drawing.Size(173, 13);
            this.lblHumanResource.TabIndex = 63;
            this.lblHumanResource.Text = "Όνομα Υπαλλήλου προς Ανάθεση";
            // 
            // cmbEmployee
            // 
            this.cmbEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmployee.FormattingEnabled = true;
            this.cmbEmployee.Location = new System.Drawing.Point(453, 407);
            this.cmbEmployee.Name = "cmbEmployee";
            this.cmbEmployee.Size = new System.Drawing.Size(121, 21);
            this.cmbEmployee.TabIndex = 54;
            // 
            // lblEstimatedDuration
            // 
            this.lblEstimatedDuration.AutoSize = true;
            this.lblEstimatedDuration.Location = new System.Drawing.Point(28, 186);
            this.lblEstimatedDuration.Name = "lblEstimatedDuration";
            this.lblEstimatedDuration.Size = new System.Drawing.Size(112, 13);
            this.lblEstimatedDuration.TabIndex = 72;
            this.lblEstimatedDuration.Text = "Εκτιμώμενη Διάρκεια";
            // 
            // nudEstimatedDuration
            // 
            this.nudEstimatedDuration.Location = new System.Drawing.Point(357, 186);
            this.nudEstimatedDuration.Name = "nudEstimatedDuration";
            this.nudEstimatedDuration.Size = new System.Drawing.Size(73, 20);
            this.nudEstimatedDuration.TabIndex = 73;
            // 
            // rbDay
            // 
            this.rbDay.AutoSize = true;
            this.rbDay.Checked = true;
            this.rbDay.Location = new System.Drawing.Point(436, 186);
            this.rbDay.Name = "rbDay";
            this.rbDay.Size = new System.Drawing.Size(62, 17);
            this.rbDay.TabIndex = 74;
            this.rbDay.TabStop = true;
            this.rbDay.Text = "Ημέρες";
            this.rbDay.UseVisualStyleBackColor = true;
            // 
            // rbHour
            // 
            this.rbHour.AutoSize = true;
            this.rbHour.Location = new System.Drawing.Point(499, 186);
            this.rbHour.Name = "rbHour";
            this.rbHour.Size = new System.Drawing.Size(53, 17);
            this.rbHour.TabIndex = 75;
            this.rbHour.TabStop = true;
            this.rbHour.Text = "Ώρες";
            this.rbHour.UseVisualStyleBackColor = true;
            // 
            // tbTasks
            // 
            this.tbTasks.Controls.Add(this.tbpRelatedTasks);
            this.tbTasks.Controls.Add(this.tbpTasksDuration);
            this.tbTasks.Controls.Add(this.tabPage1);
            this.tbTasks.Location = new System.Drawing.Point(580, 12);
            this.tbTasks.Name = "tbTasks";
            this.tbTasks.SelectedIndex = 0;
            this.tbTasks.Size = new System.Drawing.Size(654, 408);
            this.tbTasks.TabIndex = 76;
            // 
            // tbpRelatedTasks
            // 
            this.tbpRelatedTasks.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbpRelatedTasks.Controls.Add(this.label3);
            this.tbpRelatedTasks.Controls.Add(this.lvSelectedTasks);
            this.tbpRelatedTasks.Controls.Add(this.btnexportFromList);
            this.tbpRelatedTasks.Controls.Add(this.lvUnSelectedTasks);
            this.tbpRelatedTasks.Controls.Add(this.btnInsertToList);
            this.tbpRelatedTasks.Controls.Add(this.lblPreviousTask);
            this.tbpRelatedTasks.Controls.Add(this.lblNotPreviousTask);
            this.tbpRelatedTasks.Location = new System.Drawing.Point(4, 22);
            this.tbpRelatedTasks.Name = "tbpRelatedTasks";
            this.tbpRelatedTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRelatedTasks.Size = new System.Drawing.Size(646, 382);
            this.tbpRelatedTasks.TabIndex = 0;
            this.tbpRelatedTasks.Text = "Εξαρτώμενες Εργασίες";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(135, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(367, 13);
            this.label3.TabIndex = 78;
            this.label3.Text = "Εργασίες από τις οποίες η συγκεκριμένη εργασία εξαρτάται";
            // 
            // lvSelectedTasks
            // 
            this.lvSelectedTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lvSelectedTasks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lvSelectedTasks.FullRowSelect = true;
            this.lvSelectedTasks.GridLines = true;
            this.lvSelectedTasks.HideSelection = false;
            this.lvSelectedTasks.Location = new System.Drawing.Point(365, 61);
            this.lvSelectedTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvSelectedTasks.Name = "lvSelectedTasks";
            this.lvSelectedTasks.Size = new System.Drawing.Size(277, 276);
            this.lvSelectedTasks.TabIndex = 72;
            this.lvSelectedTasks.UseCompatibleStateImageBehavior = false;
            this.lvSelectedTasks.View = System.Windows.Forms.View.Details;
            this.lvSelectedTasks.DoubleClick += new System.EventHandler(this.btnexportFromList_Click);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ΑΑ";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Όνομα";
            this.columnHeader7.Width = 96;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Κατάσταση";
            this.columnHeader8.Width = 111;
            // 
            // btnexportFromList
            // 
            this.btnexportFromList.Location = new System.Drawing.Point(291, 193);
            this.btnexportFromList.Name = "btnexportFromList";
            this.btnexportFromList.Size = new System.Drawing.Size(67, 41);
            this.btnexportFromList.TabIndex = 73;
            this.btnexportFromList.Text = "<--";
            this.btnexportFromList.UseVisualStyleBackColor = true;
            this.btnexportFromList.Click += new System.EventHandler(this.btnexportFromList_Click);
            // 
            // lvUnSelectedTasks
            // 
            this.lvUnSelectedTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lvUnSelectedTasks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvUnSelectedTasks.FullRowSelect = true;
            this.lvUnSelectedTasks.GridLines = true;
            this.lvUnSelectedTasks.HideSelection = false;
            this.lvUnSelectedTasks.Location = new System.Drawing.Point(7, 61);
            this.lvUnSelectedTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvUnSelectedTasks.Name = "lvUnSelectedTasks";
            this.lvUnSelectedTasks.Size = new System.Drawing.Size(277, 276);
            this.lvUnSelectedTasks.TabIndex = 76;
            this.lvUnSelectedTasks.UseCompatibleStateImageBehavior = false;
            this.lvUnSelectedTasks.View = System.Windows.Forms.View.Details;
            this.lvUnSelectedTasks.SelectedIndexChanged += new System.EventHandler(this.lvUnSelectedTasks_SelectedIndexChanged_1);
            this.lvUnSelectedTasks.DoubleClick += new System.EventHandler(this.btnInsertToList_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ΑΑ";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Όνομα";
            this.columnHeader2.Width = 96;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Κατάσταση";
            this.columnHeader3.Width = 111;
            // 
            // btnInsertToList
            // 
            this.btnInsertToList.Location = new System.Drawing.Point(291, 146);
            this.btnInsertToList.Name = "btnInsertToList";
            this.btnInsertToList.Size = new System.Drawing.Size(66, 41);
            this.btnInsertToList.TabIndex = 77;
            this.btnInsertToList.Text = "-->";
            this.btnInsertToList.UseVisualStyleBackColor = true;
            this.btnInsertToList.Click += new System.EventHandler(this.btnInsertToList_Click);
            // 
            // lblPreviousTask
            // 
            this.lblPreviousTask.AutoSize = true;
            this.lblPreviousTask.Location = new System.Drawing.Point(428, 45);
            this.lblPreviousTask.Name = "lblPreviousTask";
            this.lblPreviousTask.Size = new System.Drawing.Size(119, 13);
            this.lblPreviousTask.TabIndex = 75;
            this.lblPreviousTask.Text = "Επιλεγμένες Εργασίες";
            this.lblPreviousTask.Click += new System.EventHandler(this.lblPreviousTask_Click);
            // 
            // lblNotPreviousTask
            // 
            this.lblNotPreviousTask.AutoSize = true;
            this.lblNotPreviousTask.Location = new System.Drawing.Point(76, 45);
            this.lblNotPreviousTask.Name = "lblNotPreviousTask";
            this.lblNotPreviousTask.Size = new System.Drawing.Size(114, 13);
            this.lblNotPreviousTask.TabIndex = 74;
            this.lblNotPreviousTask.Text = "Διαθέσιμες Εργασίες";
            // 
            // tbpTasksDuration
            // 
            this.tbpTasksDuration.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbpTasksDuration.Controls.Add(this.lvTaskDurations);
            this.tbpTasksDuration.Location = new System.Drawing.Point(4, 22);
            this.tbpTasksDuration.Name = "tbpTasksDuration";
            this.tbpTasksDuration.Padding = new System.Windows.Forms.Padding(3);
            this.tbpTasksDuration.Size = new System.Drawing.Size(646, 382);
            this.tbpTasksDuration.TabIndex = 1;
            this.tbpTasksDuration.Text = "Διάρκειες Εργασίας";
            // 
            // lvTaskDurations
            // 
            this.lvTaskDurations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5});
            this.lvTaskDurations.GridLines = true;
            this.lvTaskDurations.Location = new System.Drawing.Point(6, 6);
            this.lvTaskDurations.MultiSelect = false;
            this.lvTaskDurations.Name = "lvTaskDurations";
            this.lvTaskDurations.Size = new System.Drawing.Size(499, 188);
            this.lvTaskDurations.TabIndex = 0;
            this.lvTaskDurations.UseCompatibleStateImageBehavior = false;
            this.lvTaskDurations.View = System.Windows.Forms.View.Details;
            this.lvTaskDurations.SelectedIndexChanged += new System.EventHandler(this.lvTaskDurations_SelectedIndexChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Ημερομηνία";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Διάρκεια (σε ώρες)";
            this.columnHeader5.Width = 224;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.ucNotes1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(646, 382);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Σημειώσεις";
            // 
            // ucNotes1
            // 
            this.ucNotes1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNotes1.Location = new System.Drawing.Point(3, 3);
            this.ucNotes1.Name = "ucNotes1";
            this.ucNotes1.Size = new System.Drawing.Size(640, 376);
            this.ucNotes1.TabIndex = 0;
            // 
            // frmNewTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 564);
            this.Controls.Add(this.tbTasks);
            this.Controls.Add(this.rbHour);
            this.Controls.Add(this.rbDay);
            this.Controls.Add(this.nudEstimatedDuration);
            this.Controls.Add(this.lblEstimatedDuration);
            this.Controls.Add(this.cmbEmployee);
            this.Controls.Add(this.lblHumanResource);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblEstimatedStartDate);
            this.Controls.Add(this.dtpDeadline);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.dtpEstimatedEndDate);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.dtpEstimatedStartDate);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblStatusExplain);
            this.Controls.Add(this.txtStatusExplain);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDeadline);
            this.Name = "frmNewTask";
            this.Text = "frmNewTask";
            this.Load += new System.EventHandler(this.frmNewTask_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudEstimatedDuration)).EndInit();
            this.tbTasks.ResumeLayout(false);
            this.tbpRelatedTasks.ResumeLayout(false);
            this.tbpRelatedTasks.PerformLayout();
            this.tbpTasksDuration.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblEstimatedStartDate;
        private System.Windows.Forms.DateTimePicker dtpDeadline;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpEstimatedEndDate;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.DateTimePicker dtpEstimatedStartDate;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblStatusExplain;
        private System.Windows.Forms.TextBox txtStatusExplain;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDeadline;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblHumanResource;
        private System.Windows.Forms.ComboBox cmbEmployee;
        private System.Windows.Forms.Label lblEstimatedDuration;
        private System.Windows.Forms.NumericUpDown nudEstimatedDuration;
        private System.Windows.Forms.RadioButton rbDay;
        private System.Windows.Forms.RadioButton rbHour;
        private System.Windows.Forms.TabControl tbTasks;
        private System.Windows.Forms.TabPage tbpRelatedTasks;
        private System.Windows.Forms.ListView lvSelectedTasks;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button btnexportFromList;
        private System.Windows.Forms.ListView lvUnSelectedTasks;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnInsertToList;
        private System.Windows.Forms.Label lblPreviousTask;
        private System.Windows.Forms.Label lblNotPreviousTask;
        private System.Windows.Forms.TabPage tbpTasksDuration;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvTaskDurations;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TabPage tabPage1;
        private UserControls.ucNotes ucNotes1;
    }
}