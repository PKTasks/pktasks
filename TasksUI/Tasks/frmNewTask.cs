﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmNewTask : Form
    {

        Enumerations.PKTasksState SubProjectState;
        TasksDB.Task CurrentTask = new TasksDB.Task();
        int SubProjectId_Task;

        public event EventHandler<TasksEventArgs> OnNewTaskAdded;
        public event EventHandler<TasksEventArgs> OnNewTaskEdited;

        public frmNewTask(int subprojectId_task, int taskId, Enumerations.PKTasksState projectstate)
        {
            InitializeComponent();
            ComboBox_AutoComplete();
            
            SubProjectId_Task = subprojectId_task;

            SubProjectState = projectstate;     /****************************************************/

            

            if(projectstate == Enumerations.PKTasksState.Added)
            {
                RefreshAllElementsTasks();
            }

            if (projectstate == Enumerations.PKTasksState.Edited)
            {
                TaskService DB = new TaskService();
                TasksDB.Task Task = DB.ReturnOneTask(taskId);
                CurrentTask = Task;

                UpdateFormFromCurrentTask();
            }

            
            if (projectstate == Enumerations.PKTasksState.Edited)
            {
                ucNotes1.Initialize(taskId, Enumerations.PKTasksEntryType.Task);
            }
            else
            {
                ucNotes1.Visible = false;
            }
        }


        private void UpdateFormFromCurrentTask()
        {
            RefreshAllElementsTasks();

            txtName.Text = CurrentTask.Name;
            txtDescription.Text = CurrentTask.Description;

            if (CurrentTask.EstimatedStartDate.HasValue)
            {
                dtpEstimatedStartDate.Checked = true;
                dtpEstimatedStartDate.Value = CurrentTask.EstimatedStartDate.Value;
            }
            if (CurrentTask.EstimatedEndDate.HasValue)
            {
                dtpEstimatedEndDate.Checked = true;
                dtpEstimatedEndDate.Value = CurrentTask.EstimatedEndDate.Value;
            }
            if (CurrentTask.StartDate.HasValue)
            {
                dtpStartDate.Checked = true;
                dtpStartDate.Value = CurrentTask.StartDate.Value;
            }
            if (CurrentTask.EndDate.HasValue)
            {
                dtpEndDate.Checked = true;
                dtpEndDate.Value = CurrentTask.EndDate.Value;
            }
            if (CurrentTask.Deadline.HasValue)
            {
                dtpDeadline.Checked = true;
                dtpDeadline.Value = CurrentTask.Deadline.Value;
            }

            SetComboSelectedItemById(CurrentTask.StatusId, CurrentTask.HumanResourcesId);

            //txtStatusExplain.Text = CurrentTask.StatusExplain;

            if (CurrentTask.EstimatedDuration.HasValue)
            {
                nudEstimatedDuration.Value = CurrentTask.EstimatedDuration.Value;

                if (CurrentTask.EstimatedDurationType == (Byte)Enumerations.PKTasksDurationType.Day)
                {
                    rbDay.Checked = true;
                }
                else
                {
                    rbHour.Checked = true;
                }
            }
        }

        private void SetComboSelectedItemById(byte statusid, int employeeid)
        {
            for (int i = 0; i < cmbStatus.Items.Count; i++)
            {
                if (((TasksDB.Status)cmbStatus.Items[i]).Id == statusid)
                {
                    cmbStatus.SelectedIndex = i;
                    break;
                }
            }

            for (int i = 0; i < cmbEmployee.Items.Count; i++)
            {
                if (((TasksDB.HumanResource)cmbEmployee.Items[i]).Id == employeeid)
                {
                    cmbEmployee.SelectedIndex = i;
                    break;
                }
            }

        }

        private void frmNewTask_Load(object sender, EventArgs e)
        {
            switch (SubProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Νέα Εργασία";
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία Εργασίας";
                    break;
            }
        }


        private void ComboBox_AutoComplete()
        {
            HumanResourceService srvhr = new HumanResourceService();
            StatusService srvst = new StatusService();

            cmbStatus.DataSource = null;

            cmbStatus.DisplayMember = "Name";
            cmbStatus.DataSource = srvst.GetTasksStatus();
            if (cmbStatus.Items.Count > 0)
            {
                cmbStatus.SelectedIndex = 0;
            }

            cmbEmployee.DataSource = null;
            cmbEmployee.DisplayMember = "Name";
            cmbEmployee.DataSource = srvhr.GetAllHumanResources();
            if (cmbEmployee.Items.Count > 0)
            {
                cmbEmployee.SelectedIndex = 0;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε ένα όνομα");
                return;
            }

            TasksDB.Task task = new TasksDB.Task();
            TasksBL.TaskService sbpr = new TasksBL.TaskService();
            sbpr.OnTaskAdded += Sbpr_OnTaskAdded;

            task.Id = CurrentTask.Id;
            task.Name = txtName.Text;

            if (nudEstimatedDuration.Value >0)
            {
                task.EstimatedDuration = (int)nudEstimatedDuration.Value;
            }

            if (rbDay.Checked)
            {
                task.EstimatedDurationType = (byte)Enumerations.PKTasksDurationType.Day;
            }
            else
            {
                task.EstimatedDurationType = (byte)Enumerations.PKTasksDurationType.Hour;
            }

            if (!string.IsNullOrEmpty(txtDescription.Text))
            {
                task.Description = txtDescription.Text;
            }
            else
            {
                task.Description = null;
            }

            if (dtpEstimatedStartDate.Checked)
            {
                task.EstimatedStartDate = dtpEstimatedStartDate.Value.Date;
            }
            else
            {
                task.EstimatedStartDate = null;
            }


            if (dtpEstimatedEndDate.Checked)
            {
                task.EstimatedEndDate = dtpEstimatedEndDate.Value.Date;
            }
            else
            {
                task.EstimatedEndDate = null;
            }

            if (dtpStartDate.Checked)
            {
                task.StartDate = dtpStartDate.Value.Date;
            }
            else
            {
                task.StartDate = null;
            }

            if (dtpEndDate.Checked)
            {
                task.EndDate = dtpEndDate.Value.Date;
            }
            else
            {
                task.EndDate = null;
            }

            if (dtpDeadline.Checked)
            {
                task.Deadline = dtpDeadline.Value.Date;
            }
            else
            {
                task.Deadline = null;
            }

            task.StatusId = ((TasksDB.Status)cmbStatus.SelectedValue).Id;

            //if (!string.IsNullOrEmpty(txtStatusExplain.Text))
            //{
            //    task.StatusExplain = txtStatusExplain.Text;
            //}
            //else
            //{
            //    task.StatusExplain = null;
            //}

            task.HumanResourcesId = ((TasksDB.HumanResource)cmbEmployee.SelectedValue).Id;


            foreach (ListViewItem item in lvSelectedTasks.Items)
            {
                task.ParentTasks.Add(new TasksDB.Task() { Id = Int32.Parse(item.Text) });
            }

            
            task.SubProjectId = SubProjectId_Task;

            switch (SubProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    sbpr.Add(task);

                    break;
                case Enumerations.PKTasksState.Edited:

                    
                    sbpr.Edit(task);
                    if (OnNewTaskEdited != null)
                    {
                        TaskService DB = new TaskService();
                        TasksDB.Task t = DB.ReturnOneTask(task.Id);

                        TasksEventArgs args = new TasksEventArgs();
                        args.Task = t;
                        OnNewTaskEdited.Invoke(this, args);
                    }
                    break;
            }
            this.Close();
        }

        private void Sbpr_OnTaskAdded(object sender, TasksEventArgs e)
        {
            if (OnNewTaskAdded != null)
            {
                TaskService DB = new TaskService();
                TasksDB.Task t = DB.ReturnOneTask(e.Task.Id);

                TasksEventArgs args = new TasksEventArgs();
                args.Task = t;
                OnNewTaskAdded.Invoke(this, args);
            }
        }


        private void RefreshAllElementsTasks()
        {
            lvUnSelectedTasks.Items.Clear();

            List<TasksDB.TaskWorkingDuration> taskDurations;
            List<TasksDB.Task> tasks;

            TasksWorkingDurationsService srvwdur = new TasksWorkingDurationsService();

            TaskService srvtk = new TaskService();
            taskDurations = srvwdur.GetAllWorkingDurations(CurrentTask.Id);

            tasks = srvtk.GetSubProjectTasks(SubProjectId_Task);
            int i;
            for (i = 0; i < tasks.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(tasks[i].Id.ToString());
                ListViewItem lvi1 = new ListViewItem(tasks[i].Id.ToString());

                if (CurrentTask.ParentTasks.FirstOrDefault(x => x.Id == tasks[i].Id) == null)
                {
                    if (tasks[i].ParentTasks.FirstOrDefault(x => x.Id == CurrentTask.Id) == null)
                    {

                        if (CurrentTask.Id != tasks[i].Id)
                        {
                            lvUnSelectedTasks.Items.Add(lvi);
                            lvi.SubItems.Add(tasks[i].Name);
                            lvi.SubItems.Add(tasks[i].Status.Name);
                        }
                    }
                }
                else
                {
                    lvSelectedTasks.Items.Add(lvi1);
                    lvi1.SubItems.Add(tasks[i].Name);
                    lvi1.SubItems.Add(tasks[i].Status.Name);
                }
                

            }

            int j;
            for(j=0;j< taskDurations.Count; j++)
            {
                ListViewItem lvi2 = new ListViewItem(taskDurations[j].WorkingDate.Date.ToString("dd/MM/yyyy"));
                lvTaskDurations.Items.Add(lvi2);
                lvi2.SubItems.Add(taskDurations[j].Duration.ToString());
            }

            lvTaskDurations.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            if (this.lvUnSelectedTasks.Items.Count > 0)
            {
                this.lvUnSelectedTasks.Items[0].Selected = true;
            }
            else
            {
                //this.lvSubProjects_SelectedIndexChanged(null, null);
            }
        }

        private void btnInsertToList_Click(object sender, EventArgs e)
        {
            //TasksDB.Task task = new TasksDB.Task();
            //task.Id = Int32.Parse(lvUnSelectedTasks.SelectedItems[0].Text);

            AddRelatedTask();
        }


        private void AddRelatedTask()
        {

            foreach (ListViewItem item in lvUnSelectedTasks.SelectedItems)
            {
                ListViewItem lvi = new ListViewItem(lvUnSelectedTasks.SelectedItems[0].Text);
                lvSelectedTasks.Items.Add(lvi);
                lvi.SubItems.Add(lvUnSelectedTasks.SelectedItems[0].SubItems[1].Text);
                lvi.SubItems.Add(lvUnSelectedTasks.SelectedItems[0].SubItems[2].Text);

                lvUnSelectedTasks.SelectedItems[0].Remove();
            }

        }


        private void RemoveRelatedTask()
        {
            foreach (ListViewItem item in lvSelectedTasks.SelectedItems)
            {
                ListViewItem lvi = new ListViewItem(lvSelectedTasks.SelectedItems[0].Text);
                lvUnSelectedTasks.Items.Add(lvi);
                lvi.SubItems.Add(lvSelectedTasks.SelectedItems[0].SubItems[1].Text);
                lvi.SubItems.Add(lvSelectedTasks.SelectedItems[0].SubItems[2].Text);

                lvSelectedTasks.SelectedItems[0].Remove();
            }

        }

        private void btnexportFromList_Click(object sender, EventArgs e)
        {

            RemoveRelatedTask();
        }

        private void lblPreviousTask_Click(object sender, EventArgs e)
        {

        }

        private void lvUnSelectedTasks_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void lvTaskDurations_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
