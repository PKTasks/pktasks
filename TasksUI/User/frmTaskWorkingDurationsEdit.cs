﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmTaskWorkingDurationsEdit : Form
    {
        public event EventHandler<TaskWorkingDurationsArgs> OnTaskWorkingDurationAdd;
        public event EventHandler<TaskWorkingDurationsArgs> OnTaskWorkingDurationEdit;

        TaskWorkingDuration WorkingDurations;
        int TaskId;
        Enumerations.PKTasksState ProjectState;
        int HumanResourceId;
        
        public frmTaskWorkingDurationsEdit(int humanId,int taskId,TaskWorkingDuration workingdurations, Enumerations.PKTasksState projectstate)
        {
            InitializeComponent();
            ProjectState = projectstate;
            TaskId = taskId;
            HumanResourceId = humanId;
            if (projectstate == Enumerations.PKTasksState.Edited)
            {
                WorkingDurations = workingdurations;
                nudDuration.Value = workingdurations.Duration;
                dtpDate.Value = workingdurations.WorkingDate;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if(nudDuration.Value== 0)
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε διάρκεια");
                return;
            }

            TasksWorkingDurationsService wd = new TasksWorkingDurationsService();
            TaskWorkingDuration workingduration = new TaskWorkingDuration();

            workingduration.Duration = (int)nudDuration.Value;
            workingduration.WorkingDate = dtpDate.Value;
            workingduration.TaskId = TaskId;
            workingduration.HumanResourceId = HumanResourceId;


            HumanResourceCost humanresourcecost;
            HumanResourceCostsService srvhrc = new HumanResourceCostsService();
            humanresourcecost = srvhrc.ReturnOneHumanResourceCost(dtpDate.Value, HumanResourceId);
            workingduration.HumanResourceCostsId = humanresourcecost.Id;

            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    wd.Add(workingduration);

                    if(OnTaskWorkingDurationAdd != null)
                    {
                        TaskWorkingDurationsArgs args = new TaskWorkingDurationsArgs();
                        args.WorkingDuration = workingduration;
                        OnTaskWorkingDurationAdd.Invoke(this, args);
                    }
                    break;
                case Enumerations.PKTasksState.Edited:
                    workingduration.Id = WorkingDurations.Id;
                    workingduration.TaskId = WorkingDurations.TaskId;
                    wd.Edit(workingduration);

                    if (OnTaskWorkingDurationEdit != null)
                    {
                        TaskWorkingDurationsArgs args = new TaskWorkingDurationsArgs();
                        args.WorkingDuration = workingduration;
                        OnTaskWorkingDurationEdit.Invoke(this, args);
                    }
                    break;
            }
            this.Close();
        }

        private void frmTaskWorkingDurationsEdit_Load(object sender, EventArgs e)
        {
            switch (ProjectState)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Εισαγωγή";
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία";
                    break;
            }
        }
    }
}
