﻿namespace TasksUI
{
    partial class frmUserAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblViewName = new System.Windows.Forms.Label();
            this.lblViewSector = new System.Windows.Forms.Label();
            this.lblViewUserName = new System.Windows.Forms.Label();
            this.lblViewRole = new System.Windows.Forms.Label();
            this.btnViewTasks = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Ιδιότητα";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Όνομα Χρήστη (Username)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ειδικότητα";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Επωνυμία";
            // 
            // lblViewName
            // 
            this.lblViewName.AutoSize = true;
            this.lblViewName.Location = new System.Drawing.Point(317, 24);
            this.lblViewName.Name = "lblViewName";
            this.lblViewName.Size = new System.Drawing.Size(35, 13);
            this.lblViewName.TabIndex = 15;
            this.lblViewName.Text = "label6";
            // 
            // lblViewSector
            // 
            this.lblViewSector.AutoSize = true;
            this.lblViewSector.Location = new System.Drawing.Point(317, 53);
            this.lblViewSector.Name = "lblViewSector";
            this.lblViewSector.Size = new System.Drawing.Size(35, 13);
            this.lblViewSector.TabIndex = 16;
            this.lblViewSector.Text = "label7";
            // 
            // lblViewUserName
            // 
            this.lblViewUserName.AutoSize = true;
            this.lblViewUserName.Location = new System.Drawing.Point(317, 84);
            this.lblViewUserName.Name = "lblViewUserName";
            this.lblViewUserName.Size = new System.Drawing.Size(35, 13);
            this.lblViewUserName.TabIndex = 17;
            this.lblViewUserName.Text = "label8";
            // 
            // lblViewRole
            // 
            this.lblViewRole.AutoSize = true;
            this.lblViewRole.Location = new System.Drawing.Point(317, 116);
            this.lblViewRole.Name = "lblViewRole";
            this.lblViewRole.Size = new System.Drawing.Size(41, 13);
            this.lblViewRole.TabIndex = 19;
            this.lblViewRole.Text = "label10";
            // 
            // btnViewTasks
            // 
            this.btnViewTasks.Location = new System.Drawing.Point(320, 157);
            this.btnViewTasks.Name = "btnViewTasks";
            this.btnViewTasks.Size = new System.Drawing.Size(103, 47);
            this.btnViewTasks.TabIndex = 20;
            this.btnViewTasks.Text = "Οι Εργασίες Μου";
            this.btnViewTasks.UseVisualStyleBackColor = true;
            this.btnViewTasks.Click += new System.EventHandler(this.btnViewTasks_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(166, 251);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 47);
            this.btnExit.TabIndex = 21;
            this.btnExit.Text = "Έξοδος";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmUserAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 310);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnViewTasks);
            this.Controls.Add(this.lblViewRole);
            this.Controls.Add(this.lblViewUserName);
            this.Controls.Add(this.lblViewSector);
            this.Controls.Add(this.lblViewName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmUserAccount";
            this.Text = "Λογαριασμός Χρήστη";
            this.Load += new System.EventHandler(this.frmUserAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblViewName;
        private System.Windows.Forms.Label lblViewSector;
        private System.Windows.Forms.Label lblViewUserName;
        private System.Windows.Forms.Label lblViewRole;
        private System.Windows.Forms.Button btnViewTasks;
        private System.Windows.Forms.Button btnExit;
    }
}