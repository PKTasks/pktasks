﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmUserAccount : Form
    {
        int HumanId;
        public frmUserAccount()
        {
            InitializeComponent();
            HumanId = Globals.CurrentHumanId;
            FormComplete(HumanId);
            
        }

        private void frmUserAccount_Load(object sender, EventArgs e)
        {

        }

        private void FormComplete(int humanId)
        {
            HumanResourceService srvhr = new HumanResourceService();
            HumanResource human = srvhr.ReturnOneHumanResource(humanId);

            SectorService srvst = new SectorService();
            Sector sector = new Sector();

            sector = srvst.ReturnOneSector(human.SectorId);

            lblViewName.Text = human.Name;
            lblViewRole.Text = ((Enumerations.PKTasksUserRole)human.Role).ToString();
            lblViewSector.Text = sector.Description;
            lblViewUserName.Text = human.Username;
            

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewTasks_Click(object sender, EventArgs e)
        {
            frmUserTasks usertasks = new frmUserTasks();
            usertasks.MdiParent = this.MdiParent;
            usertasks.Show();
        }
    }
}
