﻿namespace TasksUI
{
    partial class frmUserTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEstimatedDuration = new System.Windows.Forms.Label();
            this.lblHumanResource = new System.Windows.Forms.Label();
            this.lblEstimatedEndDate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblEstimatedStartDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblDeadline = new System.Windows.Forms.Label();
            this.lblViewEstimatedDuration = new System.Windows.Forms.Label();
            this.lblViewHumanResource = new System.Windows.Forms.Label();
            this.lblViewEstimatedEndDate = new System.Windows.Forms.Label();
            this.lblViewStartDate = new System.Windows.Forms.Label();
            this.lblViewName = new System.Windows.Forms.Label();
            this.lblViewEstimatedStartDate = new System.Windows.Forms.Label();
            this.lblViewEndDate = new System.Windows.Forms.Label();
            this.lblViewStatus = new System.Windows.Forms.Label();
            this.lblViewDeadline = new System.Windows.Forms.Label();
            this.lvTaskDurations = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label14 = new System.Windows.Forms.Label();
            this.txtViewDescription = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblCount = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ucNotes2 = new TasksUI.UserControls.ucNotes();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEstimatedDuration
            // 
            this.lblEstimatedDuration.AutoSize = true;
            this.lblEstimatedDuration.Location = new System.Drawing.Point(29, 185);
            this.lblEstimatedDuration.Name = "lblEstimatedDuration";
            this.lblEstimatedDuration.Size = new System.Drawing.Size(112, 13);
            this.lblEstimatedDuration.TabIndex = 96;
            this.lblEstimatedDuration.Text = "Εκτιμώμενη Διάρκεια";
            // 
            // lblHumanResource
            // 
            this.lblHumanResource.AutoSize = true;
            this.lblHumanResource.Location = new System.Drawing.Point(29, 85);
            this.lblHumanResource.Name = "lblHumanResource";
            this.lblHumanResource.Size = new System.Drawing.Size(173, 13);
            this.lblHumanResource.TabIndex = 95;
            this.lblHumanResource.Text = "Όνομα Υπαλλήλου προς Ανάθεση";
            this.lblHumanResource.Click += new System.EventHandler(this.lblHumanResource_Click);
            // 
            // lblEstimatedEndDate
            // 
            this.lblEstimatedEndDate.AutoSize = true;
            this.lblEstimatedEndDate.Location = new System.Drawing.Point(29, 214);
            this.lblEstimatedEndDate.Name = "lblEstimatedEndDate";
            this.lblEstimatedEndDate.Size = new System.Drawing.Size(197, 13);
            this.lblEstimatedEndDate.TabIndex = 93;
            this.lblEstimatedEndDate.Text = "Εκτιμώμενη Ημερομηνία Ολοκλήρωσης";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(29, 245);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(119, 13);
            this.lblStartDate.TabIndex = 94;
            this.lblStartDate.Text = "Ημερομηνία Εκκίνησης";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(29, 40);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 76;
            this.lblName.Text = "Όνομα";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(29, 382);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(62, 13);
            this.lblDescription.TabIndex = 78;
            this.lblDescription.Text = "Περιγραφή";
            // 
            // lblEstimatedStartDate
            // 
            this.lblEstimatedStartDate.AutoSize = true;
            this.lblEstimatedStartDate.Location = new System.Drawing.Point(29, 148);
            this.lblEstimatedStartDate.Name = "lblEstimatedStartDate";
            this.lblEstimatedStartDate.Size = new System.Drawing.Size(180, 13);
            this.lblEstimatedStartDate.TabIndex = 80;
            this.lblEstimatedStartDate.Text = "Εκτιμώμενη Ημερομηνία Εκκίνησης";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(29, 271);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(136, 13);
            this.lblEndDate.TabIndex = 84;
            this.lblEndDate.Text = "Ημερομηνία Ολοκλήρωσης";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(29, 57);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(66, 13);
            this.lblStatus.TabIndex = 88;
            this.lblStatus.Text = "Κατάσταση";
            // 
            // lblDeadline
            // 
            this.lblDeadline.AutoSize = true;
            this.lblDeadline.Location = new System.Drawing.Point(29, 297);
            this.lblDeadline.Name = "lblDeadline";
            this.lblDeadline.Size = new System.Drawing.Size(98, 13);
            this.lblDeadline.TabIndex = 86;
            this.lblDeadline.Text = "Οριακή Προθεσμία";
            // 
            // lblViewEstimatedDuration
            // 
            this.lblViewEstimatedDuration.AutoSize = true;
            this.lblViewEstimatedDuration.Location = new System.Drawing.Point(449, 185);
            this.lblViewEstimatedDuration.Name = "lblViewEstimatedDuration";
            this.lblViewEstimatedDuration.Size = new System.Drawing.Size(14, 13);
            this.lblViewEstimatedDuration.TabIndex = 108;
            this.lblViewEstimatedDuration.Text = "α";
            // 
            // lblViewHumanResource
            // 
            this.lblViewHumanResource.AutoSize = true;
            this.lblViewHumanResource.Location = new System.Drawing.Point(449, 85);
            this.lblViewHumanResource.Name = "lblViewHumanResource";
            this.lblViewHumanResource.Size = new System.Drawing.Size(14, 13);
            this.lblViewHumanResource.TabIndex = 107;
            this.lblViewHumanResource.Text = "α";
            // 
            // lblViewEstimatedEndDate
            // 
            this.lblViewEstimatedEndDate.AutoSize = true;
            this.lblViewEstimatedEndDate.Location = new System.Drawing.Point(449, 214);
            this.lblViewEstimatedEndDate.Name = "lblViewEstimatedEndDate";
            this.lblViewEstimatedEndDate.Size = new System.Drawing.Size(14, 13);
            this.lblViewEstimatedEndDate.TabIndex = 105;
            this.lblViewEstimatedEndDate.Text = "α";
            // 
            // lblViewStartDate
            // 
            this.lblViewStartDate.AutoSize = true;
            this.lblViewStartDate.Location = new System.Drawing.Point(449, 245);
            this.lblViewStartDate.Name = "lblViewStartDate";
            this.lblViewStartDate.Size = new System.Drawing.Size(14, 13);
            this.lblViewStartDate.TabIndex = 106;
            this.lblViewStartDate.Text = "α";
            // 
            // lblViewName
            // 
            this.lblViewName.AutoSize = true;
            this.lblViewName.Location = new System.Drawing.Point(449, 40);
            this.lblViewName.Name = "lblViewName";
            this.lblViewName.Size = new System.Drawing.Size(14, 13);
            this.lblViewName.TabIndex = 98;
            this.lblViewName.Text = "α";
            // 
            // lblViewEstimatedStartDate
            // 
            this.lblViewEstimatedStartDate.AutoSize = true;
            this.lblViewEstimatedStartDate.Location = new System.Drawing.Point(449, 148);
            this.lblViewEstimatedStartDate.Name = "lblViewEstimatedStartDate";
            this.lblViewEstimatedStartDate.Size = new System.Drawing.Size(14, 13);
            this.lblViewEstimatedStartDate.TabIndex = 100;
            this.lblViewEstimatedStartDate.Text = "α";
            // 
            // lblViewEndDate
            // 
            this.lblViewEndDate.AutoSize = true;
            this.lblViewEndDate.Location = new System.Drawing.Point(449, 271);
            this.lblViewEndDate.Name = "lblViewEndDate";
            this.lblViewEndDate.Size = new System.Drawing.Size(14, 13);
            this.lblViewEndDate.TabIndex = 101;
            this.lblViewEndDate.Text = "α";
            this.lblViewEndDate.Click += new System.EventHandler(this.label10_Click);
            // 
            // lblViewStatus
            // 
            this.lblViewStatus.AutoSize = true;
            this.lblViewStatus.Location = new System.Drawing.Point(449, 57);
            this.lblViewStatus.Name = "lblViewStatus";
            this.lblViewStatus.Size = new System.Drawing.Size(14, 13);
            this.lblViewStatus.TabIndex = 103;
            this.lblViewStatus.Text = "α";
            // 
            // lblViewDeadline
            // 
            this.lblViewDeadline.AutoSize = true;
            this.lblViewDeadline.Location = new System.Drawing.Point(449, 297);
            this.lblViewDeadline.Name = "lblViewDeadline";
            this.lblViewDeadline.Size = new System.Drawing.Size(14, 13);
            this.lblViewDeadline.TabIndex = 102;
            this.lblViewDeadline.Text = "α";
            // 
            // lvTaskDurations
            // 
            this.lvTaskDurations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader4,
            this.columnHeader5});
            this.lvTaskDurations.FullRowSelect = true;
            this.lvTaskDurations.GridLines = true;
            this.lvTaskDurations.Location = new System.Drawing.Point(33, 59);
            this.lvTaskDurations.MultiSelect = false;
            this.lvTaskDurations.Name = "lvTaskDurations";
            this.lvTaskDurations.Size = new System.Drawing.Size(499, 216);
            this.lvTaskDurations.TabIndex = 109;
            this.lvTaskDurations.UseCompatibleStateImageBehavior = false;
            this.lvTaskDurations.View = System.Windows.Forms.View.Details;
            this.lvTaskDurations.SelectedIndexChanged += new System.EventHandler(this.lvTaskDurations_SelectedIndexChanged);
            this.lvTaskDurations.DoubleClick += new System.EventHandler(this.btnEdit_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "AA";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Ημερομηνία";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Διάρκεια (σε ώρες)";
            this.columnHeader5.Width = 224;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label14.Location = new System.Drawing.Point(122, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(298, 13);
            this.label14.TabIndex = 110;
            this.label14.Text = "Αναλυτικά οι Ώρες Ενασχόλησης με την Εργασία";
            // 
            // txtViewDescription
            // 
            this.txtViewDescription.Location = new System.Drawing.Point(352, 382);
            this.txtViewDescription.Multiline = true;
            this.txtViewDescription.Name = "txtViewDescription";
            this.txtViewDescription.ReadOnly = true;
            this.txtViewDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtViewDescription.Size = new System.Drawing.Size(206, 65);
            this.txtViewDescription.TabIndex = 111;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(675, 422);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(145, 54);
            this.btnOk.TabIndex = 113;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(198, 281);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(85, 35);
            this.btnInsert.TabIndex = 114;
            this.btnInsert.Text = "Νέο";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(289, 281);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(85, 35);
            this.btnEdit.TabIndex = 115;
            this.btnEdit.Text = "Επεξεργασία";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(936, 422);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(35, 13);
            this.lblCount.TabIndex = 116;
            this.lblCount.Text = "label1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(563, 21);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(577, 374);
            this.tabControl1.TabIndex = 117;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.lvTaskDurations);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.btnEdit);
            this.tabPage1.Controls.Add(this.btnInsert);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(569, 348);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ώρες Εργασίας";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.ucNotes2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(569, 348);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Σημειώσεις";
            // 
            // ucNotes2
            // 
            this.ucNotes2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucNotes2.Location = new System.Drawing.Point(3, 3);
            this.ucNotes2.Name = "ucNotes2";
            this.ucNotes2.Size = new System.Drawing.Size(563, 342);
            this.ucNotes2.TabIndex = 120;
            this.ucNotes2.Load += new System.EventHandler(this.ucNotes2_Load);
            // 
            // frmUserTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 499);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtViewDescription);
            this.Controls.Add(this.lblViewEstimatedDuration);
            this.Controls.Add(this.lblViewHumanResource);
            this.Controls.Add(this.lblViewEstimatedEndDate);
            this.Controls.Add(this.lblViewStartDate);
            this.Controls.Add(this.lblViewName);
            this.Controls.Add(this.lblViewEstimatedStartDate);
            this.Controls.Add(this.lblViewEndDate);
            this.Controls.Add(this.lblViewStatus);
            this.Controls.Add(this.lblViewDeadline);
            this.Controls.Add(this.lblEstimatedDuration);
            this.Controls.Add(this.lblHumanResource);
            this.Controls.Add(this.lblEstimatedEndDate);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblEstimatedStartDate);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblDeadline);
            this.Name = "frmUserTask";
            this.Text = "Εργασία";
            this.Load += new System.EventHandler(this.frmUserAccount_Load);
            this.DoubleClick += new System.EventHandler(this.frmUserTask_DoubleClick);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEstimatedDuration;
        private System.Windows.Forms.Label lblHumanResource;
        private System.Windows.Forms.Label lblEstimatedEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblEstimatedStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblDeadline;
        private System.Windows.Forms.Label lblViewEstimatedDuration;
        private System.Windows.Forms.Label lblViewHumanResource;
        private System.Windows.Forms.Label lblViewEstimatedEndDate;
        private System.Windows.Forms.Label lblViewStartDate;
        private System.Windows.Forms.Label lblViewName;
        private System.Windows.Forms.Label lblViewEstimatedStartDate;
        private System.Windows.Forms.Label lblViewEndDate;
        private System.Windows.Forms.Label lblViewStatus;
        private System.Windows.Forms.Label lblViewDeadline;
        private System.Windows.Forms.ListView lvTaskDurations;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtViewDescription;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private UserControls.ucNotes ucNotes2;
    }
}