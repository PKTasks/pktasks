﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmUserTask : Form
    {
        HumanResource human;
        Task Task;
        //HumanResourceCost humanresourcecost;
        public frmUserTask(int taskId)
        {
            InitializeComponent();
            FormComplete(taskId);
            ListViewDurationAutoComplete();
            //ListViewNotesAutoComplete();
            ucNotes2.Initialize(taskId, Enumerations.PKTasksEntryType.Task);
        }

        private void lblHumanResource_Click(object sender, EventArgs e)
        {

        }

        private void frmUserAccount_Load(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void FormComplete(int taskId)
        {
            TaskService DB = new TaskService();

            HumanResourceService srvhr = new HumanResourceService();
            Task = DB.ReturnOneTask(taskId);
            human = srvhr.ReturnOneHumanResource(Task.HumanResourcesId);

            StatusService srvst = new StatusService();
            Status status = srvst.ReturnOneStatus(Task.StatusId);
            
            lblViewName.Text = Task.Name;
            lblViewStatus.Text = status.Name;
            lblViewHumanResource.Text = human.Name;
            lblViewEstimatedStartDate.Text = Task.EstimatedStartDate.ToString();
            lblViewEstimatedEndDate.Text = Task.EstimatedEndDate.ToString();
            lblViewStartDate.Text = Task.StartDate.ToString();
            lblViewEndDate.Text = Task.EndDate.ToString();
            lblViewEstimatedDuration.Text = Task.EstimatedDuration.ToString();
            lblViewDeadline.Text = Task.Deadline.ToString();
            txtViewDescription.Text = Task.Description;
            //txtViewStatusExplain.Text = Task.StatusExplain;

            lblcomplete();

        }


        private void lblcomplete()
        {
            List<TaskWorkingDuration> workingdurations = new List<TaskWorkingDuration>();
            TasksWorkingDurationsService srvwd = new TasksWorkingDurationsService();
            workingdurations = srvwd.GetAllWorkingDurations(Task.Id);
            int count = workingdurations.Sum(x => x.Duration);

            lblCount.Text = count.ToString() + " ώρες" ;
        }

        private void ListViewDurationAutoComplete()
        {
            List<TasksDB.TaskWorkingDuration> taskDurations;
            TasksWorkingDurationsService srvwdur = new TasksWorkingDurationsService();
            taskDurations = srvwdur.GetAllWorkingDurations(Task.Id);

            int j;
            for (j = 0; j < taskDurations.Count; j++)
            {
                ListViewItem lvi2 = new ListViewItem(taskDurations[j].Id.ToString());
                lvTaskDurations.Items.Add(lvi2);
                lvi2.SubItems.Add(taskDurations[j].WorkingDate.Date.ToString("dd/MM/yyyy"));
                lvi2.SubItems.Add(taskDurations[j].Duration.ToString());
            }

            lvTaskDurations.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lvTaskDurations_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            frmTaskWorkingDurationsEdit workingdurations = new frmTaskWorkingDurationsEdit(human.Id,Task.Id,null,Enumerations.PKTasksState.Added);
            workingdurations.OnTaskWorkingDurationAdd += Workingdurations_OnTaskWorkingDurationAdd;
            workingdurations.ShowDialog();
        }

        private void Workingdurations_OnTaskWorkingDurationAdd(object sender, TaskWorkingDurationsArgs e)
        {
            NewTaskWorkingDurationAdd(e.WorkingDuration);
            lblcomplete();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int j = 0;
            int.TryParse(lvTaskDurations.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            TaskWorkingDuration workingduration = DB.ReturnOneWorkingDuration(j);

            frmTaskWorkingDurationsEdit workingdurations = new frmTaskWorkingDurationsEdit(human.Id,Task.Id,workingduration, Enumerations.PKTasksState.Edited);
            workingdurations.OnTaskWorkingDurationEdit += Workingdurations_OnTaskWorkingDurationEdit;
            workingdurations.ShowDialog();
        }

        private void Workingdurations_OnTaskWorkingDurationEdit(object sender, TaskWorkingDurationsArgs e)
        {
            UpdateTaskWorkingDuration(e.WorkingDuration);
            lblcomplete();
        }

        private void NewTaskWorkingDurationAdd(TasksDB.TaskWorkingDuration duration)
        {
            ListViewItem lvi = new ListViewItem(duration.Id.ToString());

            lvTaskDurations.Items.Add(lvi);
            lvi.SubItems.Add(duration.WorkingDate.ToString("dd/MM/yyyy"));
            lvi.SubItems.Add(duration.Duration.ToString());

        }

        private void UpdateTaskWorkingDuration(TasksDB.TaskWorkingDuration duration)
        {
            ListViewItem item = lvTaskDurations.SelectedItems[0];
            item.SubItems[0].Text = duration.Id.ToString();
            item.SubItems[1].Text = duration.WorkingDate.ToString("dd/MM/yyyy");
            item.SubItems[2].Text = duration.Duration.ToString();
        }

        private void frmUserTask_DoubleClick(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void ucNotes2_Load(object sender, EventArgs e)
        {

        }

    }
}
