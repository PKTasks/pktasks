﻿namespace TasksUI
{
    partial class frmUserTasks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.lvUserTasks = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnViewTask = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(132, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Εργασίες";
            // 
            // lvUserTasks
            // 
            this.lvUserTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lvUserTasks.CheckBoxes = true;
            this.lvUserTasks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lvUserTasks.FullRowSelect = true;
            this.lvUserTasks.GridLines = true;
            this.lvUserTasks.HideSelection = false;
            this.lvUserTasks.Location = new System.Drawing.Point(13, 22);
            this.lvUserTasks.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lvUserTasks.MultiSelect = false;
            this.lvUserTasks.Name = "lvUserTasks";
            this.lvUserTasks.Size = new System.Drawing.Size(312, 444);
            this.lvUserTasks.TabIndex = 26;
            this.lvUserTasks.UseCompatibleStateImageBehavior = false;
            this.lvUserTasks.View = System.Windows.Forms.View.Details;
            this.lvUserTasks.SelectedIndexChanged += new System.EventHandler(this.lvTasks_SelectedIndexChanged);
            this.lvUserTasks.DoubleClick += new System.EventHandler(this.btnViewTask_Click);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ΑΑ";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Όνομα";
            this.columnHeader7.Width = 96;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Κατάσταση";
            this.columnHeader8.Width = 111;
            // 
            // btnViewTask
            // 
            this.btnViewTask.Location = new System.Drawing.Point(371, 375);
            this.btnViewTask.Name = "btnViewTask";
            this.btnViewTask.Size = new System.Drawing.Size(87, 43);
            this.btnViewTask.TabIndex = 28;
            this.btnViewTask.Text = "Προβολή";
            this.btnViewTask.UseVisualStyleBackColor = true;
            this.btnViewTask.Click += new System.EventHandler(this.btnViewTask_Click);
            // 
            // frmUserTasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 478);
            this.Controls.Add(this.btnViewTask);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvUserTasks);
            this.Name = "frmUserTasks";
            this.Text = "frmUserTasks";
            this.Load += new System.EventHandler(this.frmUserTasks_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvUserTasks;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button btnViewTask;
    }
}