﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmUserTasks : Form
    {
        public frmUserTasks()
        {
            InitializeComponent();
            RefreshAllElementsTasks(Globals.CurrentHumanId);
        }

        private void lvTasks_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmUserTasks_Load(object sender, EventArgs e)
        {
            
        }

        private void RefreshAllElementsTasks(int humanId)
        {
            lvUserTasks.Items.Clear();

            List<TasksDB.Task> Tasks = new List<TasksDB.Task>();
            TaskService srvtk = new TaskService();

            Tasks = srvtk.GetHumanResourceTasks(humanId);
            int i;
            for (i = 0; i < Tasks.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(Tasks[i].Id.ToString());

                lvUserTasks.Items.Add(lvi);
                lvi.SubItems.Add(Tasks[i].Name);
                lvi.SubItems.Add(Tasks[i].Status.Name);

            }
            if (this.lvUserTasks.Items.Count > 0)
            {
                this.lvUserTasks.Items[0].Selected = true;
            }
            else
            {
                //this.lvSubProjects_SelectedIndexChanged(null, null);
            }
        }

        private void btnViewTask_Click(object sender, EventArgs e)
        {
            frmUserTask usertask = new frmUserTask(Int32.Parse(lvUserTasks.SelectedItems[0].Text));
            usertask.Show();
        }
    }
}
