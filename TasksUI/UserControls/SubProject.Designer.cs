﻿namespace TasksUI.UserControls
{
    partial class ucSubProject
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenProject = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnNewRemoveProject = new System.Windows.Forms.Button();
            this.btnNewEditProject = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnNewProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenProject
            // 
            this.btnOpenProject.Location = new System.Drawing.Point(722, 13);
            this.btnOpenProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnOpenProject.Name = "btnOpenProject";
            this.btnOpenProject.Size = new System.Drawing.Size(166, 53);
            this.btnOpenProject.TabIndex = 20;
            this.btnOpenProject.Text = "Άνοιγμα";
            this.btnOpenProject.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(545, 13);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(166, 53);
            this.button2.TabIndex = 19;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnNewRemoveProject
            // 
            this.btnNewRemoveProject.Enabled = false;
            this.btnNewRemoveProject.Location = new System.Drawing.Point(371, 13);
            this.btnNewRemoveProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewRemoveProject.Name = "btnNewRemoveProject";
            this.btnNewRemoveProject.Size = new System.Drawing.Size(166, 53);
            this.btnNewRemoveProject.TabIndex = 17;
            this.btnNewRemoveProject.Text = "Διαγραφή";
            this.btnNewRemoveProject.UseVisualStyleBackColor = true;
            // 
            // btnNewEditProject
            // 
            this.btnNewEditProject.Enabled = false;
            this.btnNewEditProject.Location = new System.Drawing.Point(197, 13);
            this.btnNewEditProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewEditProject.Name = "btnNewEditProject";
            this.btnNewEditProject.Size = new System.Drawing.Size(166, 53);
            this.btnNewEditProject.TabIndex = 16;
            this.btnNewEditProject.Text = "Επεξεργασία";
            this.btnNewEditProject.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(23, 72);
            this.listView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(864, 414);
            this.listView1.TabIndex = 15;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ΑΑ";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Όνομα";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Εκτιμώμενη Ημερομηνία Εκκίνησης";
            this.columnHeader4.Width = 109;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Εκτιμώμενη Ημερομηνία Ολοκλήρωσης";
            this.columnHeader5.Width = 128;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Ημερομηνία Εκκίνησης";
            this.columnHeader6.Width = 142;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Ημερομηνία Ολοκλήρωσης";
            this.columnHeader7.Width = 118;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Οριακή Προθεσμία";
            this.columnHeader8.Width = 122;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Κατάσταση";
            this.columnHeader9.Width = 111;
            // 
            // btnNewProject
            // 
            this.btnNewProject.Location = new System.Drawing.Point(23, 13);
            this.btnNewProject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnNewProject.Name = "btnNewProject";
            this.btnNewProject.Size = new System.Drawing.Size(166, 53);
            this.btnNewProject.TabIndex = 14;
            this.btnNewProject.Text = "Εισαγωγή";
            this.btnNewProject.UseVisualStyleBackColor = true;
            this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // ucSubProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnNewProject);
            this.Controls.Add(this.btnOpenProject);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnNewRemoveProject);
            this.Controls.Add(this.btnNewEditProject);
            this.Controls.Add(this.listView1);
            this.Name = "ucSubProject";
            this.Size = new System.Drawing.Size(906, 501);
            this.Load += new System.EventHandler(this.SubProject_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenProject;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnNewRemoveProject;
        private System.Windows.Forms.Button btnNewEditProject;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.Button btnNewProject;
    }
}
