﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI.UserControls
{
    public partial class ucSubProject : UserControl
    {

        /// <summary>
        /// Το project Id για το οποίο γίνεται ο χείρσιμςο των subProjects
        /// </summary>
        public int ProjectId { get; set; }

        public ucSubProject()
        {
            InitializeComponent();

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnNewProject_Click(object sender, EventArgs e)
        {
            //frmNewSubProject subproject = new frmNewSubProject(,null, Enumerations.PKTasksState.Added);
            //subproject.ShowDialog(this);
        }


        private void Update(TasksDB.SubProject SubProject)
        {
            ListViewItem item = listView1.SelectedItems[0];
            item.SubItems[0].Text = SubProject.Id.ToString();
            item.SubItems[1].Text = SubProject.Name;
            item.SubItems[2].Text = Utilities.GetFormatedDateFromNullable(SubProject.EstimatedStartDate);
            item.SubItems[3].Text = Utilities.GetFormatedDateFromNullable(SubProject.EstimatedEndDate);
            item.SubItems[4].Text = Utilities.GetFormatedDateFromNullable(SubProject.StartDate);
            item.SubItems[5].Text = Utilities.GetFormatedDateFromNullable(SubProject.EndDate);
            item.SubItems[6].Text = Utilities.GetFormatedDateFromNullable(SubProject.Deadline);
            item.SubItems[7].Text = SubProject.Status.Name.ToString();

        }

        private void New(TasksDB.SubProject SubProject)
        {
            ListViewItem lvi = new ListViewItem(SubProject.Id.ToString());

            listView1.Items.Add(lvi);

            lvi.SubItems.Add(SubProject.Name);
            lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProject.EstimatedStartDate));
            lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProject.EstimatedEndDate));
            lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProject.StartDate));
            lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProject.EndDate));
            lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProject.Deadline));
            lvi.SubItems.Add(SubProject.Status.Name);

        }


        private void RefreshElementsAll()
        {
            listView1.Items.Clear();

            List<TasksDB.SubProject> SubProjects = new List<TasksDB.SubProject>();
            SubProjectService srvsbpr = new SubProjectService();
            SubProjects = srvsbpr.GetProjectSubProjects(ProjectId);

            int i;
            for (i = 0; i < SubProjects.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(SubProjects[i].Id.ToString());

                listView1.Items.Add(lvi);

                lvi.SubItems.Add(SubProjects[i].Name);
                lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProjects[i].EstimatedStartDate));
                lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProjects[i].EstimatedEndDate));
                lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProjects[i].StartDate));
                lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProjects[i].EndDate));
                lvi.SubItems.Add(Utilities.GetFormatedDateFromNullable(SubProjects[i].Deadline));
                lvi.SubItems.Add(SubProjects[i].Status.Name);

            }
            this.listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            ////this.listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

        }

        private void SubProject_Load(object sender, EventArgs e)
        {
            if (Site != null && Site.DesignMode) return;
            RefreshElementsAll();
        }
    }
}
