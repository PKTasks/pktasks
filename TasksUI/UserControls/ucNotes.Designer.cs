﻿namespace TasksUI.UserControls
{
    partial class ucNotes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvNotes = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEditNote = new System.Windows.Forms.Button();
            this.btnInsertNote = new System.Windows.Forms.Button();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvNotes
            // 
            this.lvNotes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader6,
            this.columnHeader1});
            this.lvNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvNotes.FullRowSelect = true;
            this.lvNotes.GridLines = true;
            this.lvNotes.Location = new System.Drawing.Point(0, 0);
            this.lvNotes.MultiSelect = false;
            this.lvNotes.Name = "lvNotes";
            this.lvNotes.Size = new System.Drawing.Size(503, 348);
            this.lvNotes.TabIndex = 118;
            this.lvNotes.UseCompatibleStateImageBehavior = false;
            this.lvNotes.View = System.Windows.Forms.View.Details;
            this.lvNotes.SelectedIndexChanged += new System.EventHandler(this.lvNotes_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "AA";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Ημερομηνία";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Σημείωση";
            this.columnHeader6.Width = 224;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnEditNote);
            this.panel1.Controls.Add(this.btnInsertNote);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 282);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(503, 66);
            this.panel1.TabIndex = 119;
            // 
            // btnEditNote
            // 
            this.btnEditNote.Location = new System.Drawing.Point(247, 16);
            this.btnEditNote.Name = "btnEditNote";
            this.btnEditNote.Size = new System.Drawing.Size(85, 35);
            this.btnEditNote.TabIndex = 121;
            this.btnEditNote.Text = "Επεξεργασία";
            this.btnEditNote.UseVisualStyleBackColor = true;
            this.btnEditNote.Click += new System.EventHandler(this.btnEditNote_Click);
            // 
            // btnInsertNote
            // 
            this.btnInsertNote.Location = new System.Drawing.Point(156, 16);
            this.btnInsertNote.Name = "btnInsertNote";
            this.btnInsertNote.Size = new System.Drawing.Size(85, 35);
            this.btnInsertNote.TabIndex = 120;
            this.btnInsertNote.Text = "Νέο";
            this.btnInsertNote.UseVisualStyleBackColor = true;
            this.btnInsertNote.Click += new System.EventHandler(this.btnInsertNote_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Συγγραφέας";
            this.columnHeader1.Width = 93;
            // 
            // ucNotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lvNotes);
            this.Name = "ucNotes";
            this.Size = new System.Drawing.Size(503, 348);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvNotes;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEditNote;
        private System.Windows.Forms.Button btnInsertNote;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}
