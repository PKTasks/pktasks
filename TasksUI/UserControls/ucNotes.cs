﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI.UserControls
{
    public partial class ucNotes : UserControl
    {
        private int EntryId;
        private Enumerations.PKTasksEntryType EntryType;
        HumanResource HumanResource;
        HumanResourceService srvhr = new HumanResourceService();
        public ucNotes()
        {
            InitializeComponent();
        }

        public void Initialize(int entryId,  Enumerations.PKTasksEntryType entryType)
        {
            EntryId = entryId;
            EntryType = entryType;

            
            HumanResource = srvhr.ReturnOneHumanResource(Globals.CurrentHumanId);
            
            ListViewNotesAutoComplete();
            
        }



        private void ListViewNotesAutoComplete()
        {
            List<TasksDB.Note> notes;
            NotesService srvnt = new NotesService();

            notes = srvnt.GetAllNotes(EntryId, (byte)EntryType);

            int j;
            for (j = 0; j < notes.Count; j++)
            {
                ListViewItem lvi2 = new ListViewItem(notes[j].Id.ToString());
                lvNotes.Items.Add(lvi2);
                lvi2.SubItems.Add(notes[j].PostDate.ToString("dd/MM/yyyy HH:mm"));
                if (notes[j].Message.Length > 50)
                {
                    lvi2.SubItems.Add(notes[j].Message.Substring(0, 50));
                }
                else
                {
                    lvi2.SubItems.Add(notes[j].Message);
                }
                lvi2.SubItems.Add(srvhr.ReturnOneHumanResource(notes[j].HumanResourceId).Name);
            }

            lvNotes.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void btnInsertNote_Click(object sender, EventArgs e)
        {
            frmNoteEdit notes = new frmNoteEdit(HumanResource.Id, EntryId, null, EntryType, Enumerations.PKTasksState.Added);
            notes.OnNoteAdd += Notes_OnNoteAdd;
            notes.ShowDialog();
        }


        private void Notes_OnNoteAdd(object sender, NoteArgs e)
        {
            NewNoteAdd(e.Note);
        }

        private void btnEditNote_Click(object sender, EventArgs e)
        {
            int j = 0;
            int.TryParse(lvNotes.SelectedItems[0].Text, out j);

            DBAccess DB = new DBAccess();
            Note note = DB.ReturnOneNotebyId(j);

            frmNoteEdit notes = new frmNoteEdit(HumanResource.Id, EntryId, note, EntryType, Enumerations.PKTasksState.Edited);
            notes.OnNoteEdit += Notes_OnNoteEdit;
            notes.ShowDialog();
        }


        private void Notes_OnNoteEdit(object sender, NoteArgs e)
        {
            UpdateNote(e.Note);
        }



        private void NewNoteAdd(TasksDB.Note note)
        {
            ListViewItem lvi = new ListViewItem(note.Id.ToString());

            lvNotes.Items.Add(lvi);
            lvi.SubItems.Add(note.PostDate.ToString());
            lvi.SubItems.Add(note.Message);
            lvi.SubItems.Add(srvhr.ReturnOneHumanResource(note.HumanResourceId).Name);

        }

        private void UpdateNote(TasksDB.Note note)
        {
            ListViewItem item = lvNotes.SelectedItems[0];
            item.SubItems[0].Text = note.Id.ToString();
            item.SubItems[1].Text = note.PostDate.ToString();
            item.SubItems[2].Text = note.Message;
            item.SubItems[3].Text = srvhr.ReturnOneHumanResource(note.HumanResourceId).Name;
        }

        private void lvNotes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
