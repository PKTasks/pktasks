﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksUI
{
    public class Utilities
    {
        public static string GetFormatedDateFromNullable(DateTime? dt)
        {
            if (dt.HasValue)
            {
                return dt.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
