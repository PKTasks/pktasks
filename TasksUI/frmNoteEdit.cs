﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TasksBL;
using TasksDB;

namespace TasksUI
{
    public partial class frmNoteEdit : Form
    {
        public event EventHandler<NoteArgs> OnNoteAdd;
        public event EventHandler<NoteArgs> OnNoteEdit;

        Enumerations.PKTasksEntryType EntryType;
        Enumerations.PKTasksState State;
        Note Note;
        int HumanResourceId;
        int EntryId;
        public frmNoteEdit(int humanId, int entryId, Note note, Enumerations.PKTasksEntryType entrytype, Enumerations.PKTasksState state)
        {
            InitializeComponent();
            EntryType = entrytype;
            State = state;
            HumanResourceId = humanId;
            EntryId = entryId;

            if (state == Enumerations.PKTasksState.Edited)
            {
                Note = note;
                txtNote.Text = Note.Message;
            }
        }

        private void frmNoteEdit_Load(object sender, EventArgs e)
        {
            switch (State)
            {
                case Enumerations.PKTasksState.Added:
                    this.Text = "Εισαγωγή";
                    break;
                case Enumerations.PKTasksState.Edited:
                    this.Text = "Επεξεργασία";
                    break;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (txtNote.Text == null)
            {
                MessageBox.Show("Παρακαλώ συμπληρώστε το κείμενο");
                return;
            }

            NotesService srvnt = new NotesService();
            Note note = new Note();

            note.Message = txtNote.Text;
            note.EntryId = EntryId;
            note.HumanResourceId = HumanResourceId;
            note.EntryTypeId = (byte)EntryType;


            switch (State)
            {
                case Enumerations.PKTasksState.Added:
                    note.PostDate = DateTime.Now;
                    srvnt.Add(note);

                    if (OnNoteAdd != null)
                    {
                        NoteArgs args = new NoteArgs();
                        args.Note = note;
                        OnNoteAdd.Invoke(this, args);
                    }
                    break;
                case Enumerations.PKTasksState.Edited:
                    note.Id = Note.Id;
                    note.PostDate = Note.PostDate;
                    srvnt.Edit(note);

                    if (OnNoteEdit != null)
                    {
                        NoteArgs args = new NoteArgs();
                        args.Note = note;
                        OnNoteEdit.Invoke(this, args);
                    }
                    break;
            }
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
